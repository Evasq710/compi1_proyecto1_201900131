from tkinter import *
from tkinter import filedialog, messagebox
import ply.yacc as yacc
from compiler.scanner import Scanner
from compiler.parser import *
import compiler.parser as prs
import traceback, os

analysis_done = False
entorno_global = None
errores_lexicos_global = []
errores_sintacticos_global = []
errores_semanticos_global = []

class Interfaz:
    def __init__(self, window):
        self.window = window
        self.window.title('Little Code')        
        self.window.state('zoomed')

        self.frame = Frame(self.window, bg="#555252")
        self.frame.place(x=0, y=0, relwidth=1, relheight=1)

        imagen = PhotoImage(file = "images/logo.png")
        logo = Label(self.frame, image = imagen, bg="#555252")
        logo.photo = imagen
        logo.place(x=460, y=15, width=200, height=100)

        title = Label(self.frame, text="Little Code", font=("Consolas", 50, "bold"), bg="#555252", fg="white")
        title.place(x=660, y=15)

        self.txtbox_code = Text(self.frame, font=("Consolas", 13), bg="#362f2e", fg="white")
        self.txtbox_code.place(x=330, y=120, width=1170, height=320)

        self.txtbox_numLineas = Text(self.frame, width=3, font=("Consolas", 13), bg="#362f2e", fg="#D3DCD3")
        self.txtbox_numLineas.place(x=290, y=120, width=35, height=320)
        self.txtbox_numLineas.tag_configure('line', justify='right')
        self.txtbox_numLineas.config(state='disabled')

        self.scroll_two_txtbox= Scrollbar(self.txtbox_code)
        self.scroll_two_txtbox.pack(side=RIGHT, fill=Y)
        self.scroll_two_txtbox.config(command=self.scroll_txtbox)
        self.txtbox_code.config(yscrollcommand=self.update_scroll)
        self.txtbox_numLineas.config(yscrollcommand=self.update_scroll)

        self.txtbox_code.bind("<Key>", self.numLineas)

        lb_txt2 = Label(self.frame, text="=================================================================== TERMINAL ===================================================================",
                        font=("Consolas", 14), bg="#555252", fg="AntiqueWhite")
        lb_txt2.place(x=50, y=455)

        self.txtbox_console = Text(self.frame, font=("Consolas", 13), bg="black", fg="#D3DCD3", wrap="none")
        self.txtbox_console.tag_configure("izquierda", justify='left')
        self.txtbox_console.insert("1.0", "")
        self.txtbox_console.tag_add("izquierda", "1.0")
        self.txtbox_console.config(width=57, height=25, state='disabled')
        self.txtbox_console.place(x=50, y=480, width=1300, height=300) 

        scroll_x = Scrollbar(self.txtbox_console, orient='horizontal')
        scroll_x.pack(side=BOTTOM, fill=X)
        self.txtbox_console.config(xscrollcommand=scroll_x.set)
        scroll_x.config(command=self.txtbox_console.xview)

        self.txtbox_console.tag_config('Fin', font=("Consolas", 13), foreground="#0A8C0A")

        frame_btn_clear = Frame(self.frame, bg="#000000")
        frame_btn_clear.place(x=1350, y=480, width=150, height=300)

        img1 = PhotoImage(file = "images/clean.png")
        btn_clear = Button(self.frame, image=img1, bg="#000000", command = lambda:[self.txtbox_console.config(state='normal'),
                            self.txtbox_console.delete(1.0, END), self.txtbox_console.config(state='disabled')])
        btn_clear.photo = img1
        btn_clear.place(x=1375, y=575)

        frame_btn = Frame(self.frame, bg="#555252")
        frame_btn.place(x=30, y=120)

        abrir_btn = Button(frame_btn, text="Abrir Archivo", font=("Ebrima", 13), bg="steel blue", command = lambda:[self.abrirArchivo()])
        abrir_btn.grid(row=0, column=0)

        sep1 = Label(frame_btn, text="", bg="#555252")
        sep1.grid(row=1, column=0, pady=5)

        analizar_btn = Button(frame_btn, text="Analizar Código", font=("Ebrima", 13), bg="steel blue", command = lambda:[self.obtener_codigo()])
        analizar_btn.grid(row=2, column=0)

        sep2 = Label(frame_btn, text="", bg="#555252")
        sep2.grid(row=3, column=0, pady=10)

        lb_txt3 = Label(frame_btn, text="Sección de reportes", font=("Ebrima bold", 15), bg="#555252", fg="white")
        lb_txt3.grid(row=4, column=0, pady=5)

        list_reportes = [("Tabla de símbolos global", 1), ("Reporte de Errores", 2), ("Árbol sintactico concreto (CST)", 3)]
        reporte = StringVar()
        reporte.set("1")

        row_num = 5
        for (tipo_reporte, valor) in list_reportes:
            Radiobutton(frame_btn, text=tipo_reporte, variable=reporte, value=valor, font=("Ebrima bold", 13), bg="#555252").grid(row=row_num, column=0)
            row_num += 1

        reportes_btn = Button(frame_btn, text="Generar Reporte", font=("Ebrima", 13), bg="steel blue", command = lambda:[self.reporte(reporte.get())])
        reportes_btn.grid(row=row_num, column=0)

    def scroll_txtbox(self, action, position, type=None):
        self.txtbox_code.yview_moveto(position)
        self.txtbox_numLineas.yview_moveto(position)

    def update_scroll(self, first, last, type=None):
        self.txtbox_code.yview_moveto(first)
        self.txtbox_numLineas.yview_moveto(first)
        self.scroll_two_txtbox.set(first, last)
    
    def numLineas(self, event):
        line, column = self.txtbox_code.index(END).split('.')
        self.txtbox_numLineas.config(state='normal')
        self.txtbox_numLineas.delete(1.0, END)
        lineas = ""
        for i in range(int(line)-1):
            lineas += str(i + 1)
            if i + 1 != int(line) - 1:
                lineas += "\n"
        self.txtbox_numLineas.insert(END, lineas)
        self.txtbox_numLineas.config(state='disabled')
        self.txtbox_code.update()

    def abrirArchivo(self):
        global texto_epk
        name_file = filedialog.askopenfilename(
            title = "Seleccionar archivo EPK",
            initialdir = "./",
            filetypes = {
                ("Archivos EPK", "*.epk"),
                ("Todos los archivos", "*.*")
            }
        )
        try:
            archivo = open(name_file, encoding="utf8")
            texto = ""
            texto = archivo.read()
            texto_epk = ""
            texto_epk = texto
            print("->Archivo leído con éxito")            
            self.txtbox_code.insert(END, texto_epk)
            archivo.close()      
            self.numLineas(None)      
        except Exception:
            traceback.print_exc()
            print("->No se seleccionó un archivo")
    
    def obtener_codigo(self):
        global analysis_done
        global entorno_global
        global errores_lexicos_global
        global errores_sintacticos_global
        global errores_semanticos_global

        code = ""
        code = self.txtbox_code.get(1.0, END)
        if code != "\n":
            errores_lexicos_global = []
            errores_sintacticos_global = []
            errores_semanticos_global = []

            scan = Scanner()
            scan.build_scanner()
            scan.errores_lexicos = []
            prs.errores_sintacticos = []
            prs.nodos = []
            lexer = scan.scanner
            tokens = scan.tokens
            parser = yacc.yacc()
            prs.input_string = code
            prs.txtbox_console = self.txtbox_console
            entorno_global = Entorno(None)
            scan.input_data = code
            acciones = parser.parse(code)

            errores_lexicos_global = scan.errores_lexicos
            errores_sintacticos_global = prs.errores_sintacticos

            # for error in scan.errores_lexicos:
            #     print(error.reporte_str_error())
            # for error in prs.errores_sintacticos:
            #     print(error.reporte_str_error())

            self.txtbox_console.config(state='normal')
            if acciones is not None:
                for accion in acciones:
                    try:
                        if accion is not None and not isinstance(accion, Error):
                            respuesta = accion.ejecutar(entorno_global)
                            if isinstance(respuesta, Error):
                                errores_semanticos_global.append(respuesta)
                            elif isinstance(respuesta, list):
                                # Lista de errores que devolvió una instrucción (While, for, ...)
                                errores_semanticos_global.extend(respuesta)
                            elif isinstance(respuesta, dict):
                                # Si viene dict, se detectaron errores semánticos y un break fuera de un bucle, o un return fuera de una función
                                errores_semanticos_global.extend(respuesta.get("errores"))
                                if respuesta.get("break"):
                                    errores_semanticos_global.append(Error("Error semántico", "Se detectó un break fuera de un bucle.", respuesta.get("break").fila, respuesta.get("break").columna))
                                elif respuesta.get("return"):
                                    errores_semanticos_global.append(Error("Error semántico", "Se detectó un return fuera de una función.", respuesta.get("return").fila, respuesta.get("return").columna))
                                print("Error que no debería de pasar, se detectó un dict, sin que viniera break o return")
                            elif isinstance(respuesta, Break):
                                errores_semanticos_global.append(Error("Error semántico", "Se detectó un break fuera de un bucle.", respuesta.fila, respuesta.columna))
                            elif isinstance(respuesta, Return):
                                errores_semanticos_global.append(Error("Error semántico", "Se detectó un return fuera de una función.", respuesta.fila, respuesta.columna))
                    except Exception as e:
                        print(e)
                        continue

            # for error in errores_semanticos_global:
            #     print(error.reporte_str_error())

            self.txtbox_console.insert(END, "\n> Fin de análisis de código.\n", "Fin")
            self.txtbox_console.config(state='disabled')
            analysis_done = True
        else:
            entorno_global = None
            self.txtbox_console.config(state='normal')
            self.txtbox_console.insert(END, " Fin de análisis de código.\n", "Fin")
            self.txtbox_console.config(state='disabled')
    
    def reporte(self, value):
        global analysis_done
        if value == "1":
            print("Tabla de símbolos global")
            if analysis_done:
                if entorno_global is not None:
                    html_generado = self.reporte_tabla_simbolos_global()
                    if html_generado:
                        messagebox.showinfo("Reporte Tabla de símbolos", "Tabla de símbolos generada exitosamente.\nUbicación: Tablas de simbolos.")
                    else:
                        messagebox.showerror("Reporte Tabla de símbolos", "Ocurrió un error en la generación de la tabla :( (Ver consola)")
                else:
                    messagebox.showerror("Reporte Tabla de símbolos", "No se ha generado el entorno global (debes ejecutar código con variables y funciones).")
            else:
                messagebox.showerror("Reporte Tabla de símbolos", "Debes ejecutar código para poder generar su tabla de símbolos.")
        elif value == "2":
            print("Reporte de errores")
            if analysis_done:
                generado = self.reporte_errores()
                if generado:
                    messagebox.showinfo("Reporte de errores", "Reporte de errores generado exitosamente.\nUbicación: Errores.")
                else:
                    messagebox.showerror("Reporte de errores", "Ocurrió un error en la generación del reporte de errores :( (Ver consola)")
            else:
                messagebox.showerror("Reporte de errores", "Debes ejecutar código para poder generar el reporte de errores.")
        elif value == "3":
            print("Reporte de CST")
            if analysis_done:
                tree_done = self.arbol_sintactico_concreto()
                if tree_done is not None:
                    if tree_done:
                        messagebox.showinfo("Reporte árbol CST", "Árbol CST generado exitosamente.\nUbicación: Concrete Syntax Tree.")
                    else:
                        messagebox.showerror("Reporte árbol CST", "Ocurrió un error en la generación del árbol CST :( (Ver consola)")
                else:
                    messagebox.showerror("Reporte árbol CST", "Ocurrió un error, no se generó el árbol CST en el parser :(  (Ver consola)")
            else:
                messagebox.showerror("Reporte árbol CST", "Debes ejecutar código para poder generar su árbol CST.")
        else:
            print("Error: VALUE de reporte no reconocido.")
    
    def arbol_sintactico_concreto(self):
        if prs.arbol_CST is not None:
            dot_file_arbol = prs.arbol_CST.str_graphviz
            
            try:
                ruta = 'Concrete Syntax Tree\\arbol cst.dot'
                archivo_dot = open(ruta, 'w')
                archivo_dot.write(dot_file_arbol)
                archivo_dot.close()
            except Exception as e:
                print(e)
                print("> Ocurrió un error en la creación del archivo .dot :(")
                return False
            print("> Archivo .dot creado exitosamente. Ver: Concrete Syntax Tree")
            try:
                os.chdir('Concrete Syntax Tree')
                ruta_dot = "arbol cst.dot"
                ruta_pdf = "arbol cst.pdf"
                comando = f'dot.exe -Tpdf "{ruta_dot}" -o "{ruta_pdf}"'
                os.system(comando)
                os.chdir('..')
                print("> Archivo .pdf creado exitosamente. Ver: Concrete Syntax Tree")
                return True
            except Exception as e:
                print(e)
                print("> Ocurrió un error en la creación del archivo .pdf :(")
                return False
        return None
    
    def reporte_tabla_simbolos_global(self):
        global entorno_global
        tabla = Tabla_Simbolos(0, 0, nombre_reporte_global="Tabla de símbolos global")
        html_generado = tabla.ejecutar(entorno_global)
        return html_generado
    
    def reporte_errores(self):
        global errores_lexicos_global
        global errores_sintacticos_global
        global errores_semanticos_global

        html = '''<!DOCTYPE html>
        <html lang="es">
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
            <link rel="preconnect" href="https://fonts.googleapis.com">
            <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
            <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&display=swap" rel="stylesheet">
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
            <link rel="stylesheet" href="errores.css" type="text/css" />
            <title>Reporte Errores</title>
        </head>
        <body>
            <li style="float: left; padding-left: 25%; padding-right: 20px;"><span class="material-icons md-light md-100">error</span></li>
            <h1>Reporte de Errores</h1>
        <div class="datos-reporte">
            <h2 style="color: black;">Errores Léxicos</h2>
            <div class="tabla-errores">
                <table class="table table-striped table-hover">
                    <thead style="background-color: black; color: white;">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Descripcion</th>
                        <th scope="col">Fila</th>
                        <th scope="col">Columna</th>
                        </tr>
                    </thead>
                    <tbody>'''
        error_agregado = 0
        id_fila = ""
        try:
            for error in errores_lexicos_global:
                error_agregado += 1
                id_fila = "uno" if error_agregado % 2 == 1 else "dos"
                html += f'''\n<tr id="{id_fila}">
                <th scope="row">{error_agregado}</th>
                <td>{error.descripcion}</td>
                <td>{error.fila}</td>
                <td>{error.columna}</td>
                </tr>'''
        except:
            traceback.print_exc()
            return False
        html += '''\n</tbody>
                    </table>
                </div>
            </div>

        <div class="datos-reporte">
            <h2 style="color: black;">Errores Sintácticos</h2>
            <div class="tabla-errores">
                <table class="table table-striped table-hover">
                    <thead style="background-color: black; color: white;">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Descripcion</th>
                        <th scope="col">Fila</th>
                        <th scope="col">Columna</th>
                        </tr>
                    </thead>
                    <tbody>'''
        error_agregado = 0
        id_fila = ""
        try:
            for error in errores_sintacticos_global:
                error_agregado += 1
                id_fila = "uno" if error_agregado % 2 == 1 else "dos"
                html += f'''\n<tr id="{id_fila}">
                <th scope="row">{error_agregado}</th>\n'''
                html += f'<td>{error.descripcion}</td>\n'
                html += f'''<td>{error.fila}</td>
                <td>{error.columna}</td>
                </tr>'''
        except:
            traceback.print_exc()
            return False
        html += '''\n</tbody>
                    </table>
                </div>
            </div>

        <div class="datos-reporte">
            <h2 style="color: black;">Errores Semánticos</h2>
            <div class="tabla-errores">
                <table class="table table-striped table-hover">
                    <thead style="background-color: black; color: white;">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Descripcion</th>
                        <th scope="col">Fila</th>
                        <th scope="col">Columna</th>
                        </tr>
                    </thead>
                    <tbody>'''
        error_agregado = 0
        id_fila = ""
        try:
            for error in errores_semanticos_global:
                error_agregado += 1
                id_fila = "uno" if error_agregado % 2 == 1 else "dos"
                html += f'''\n<tr id="{id_fila}">
                <th scope="row">{error_agregado}</th>\n'''
                html += f'<td>{error.descripcion}</td>\n'
                html += f'''<td>{error.fila}</td>
                <td>{error.columna}</td>
                </tr>'''
        except:
            traceback.print_exc()
            return False
        html += '''\n</tbody>
                    </table>
                </div>
            </div>

            <footer>
                <p>Elías Abraham Vasquez Soto - 201900131</p>
                <p>Proyecto 2 - Laboratorio Lenguajes Formales y de Programación B-</p>        
                <img src="images/logo_usac.png" width="220" height="60"/>
            </footer>

            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        </body>
        </html>'''
        
        css = '''html {
            min-height: 100%;
            position: relative;
        }

        body {
            background-color:rgb(54, 1, 1);
            padding-top: 20px;
            margin-bottom: 150px;
        }

        /* ===== Iconos de Google ===== */
        /* Rules for sizing the icon. */
        .material-icons.md-24 { font-size: 24px; }
        .material-icons.md-30 { font-size: 30px; }
        .material-icons.md-100 { font-size: 100px; }
        /* Rules for using icons as black on a light background. */
        .material-icons.md-dark { color: rgba(0, 0, 0, 0.54); }
        .material-icons.md-dark.md-inactive { color: rgba(0, 0, 0, 0.26); }
        /* Rules for using icons as white on a dark background. */
        .material-icons.md-light { color: rgba(255, 255, 255, 1); }
        .material-icons.md-light.md-inactive { color: rgba(255, 255, 255, 0.3); }


        h1 {
            color: white;
            font-family: 'Lato', sans-serif;
            font-size: 75px;
        }

        .datos-reporte {
            background-color: rgb(255, 255, 255);
            padding-top: 20px;
            padding-bottom: 20px;
            padding-left: 50px;
            margin: 30px 100px 30px 100px;
        }

        .tabla-errores {
            padding-top: 20px;
            padding-left: 20px;
            padding-right: 40px;
            text-align: center;
            font-family: 'Lato', sans-serif;
            font-size: 20px;
            letter-spacing: 1px;
        }

        table td{    
            color: white;
        }

        table th{    
            color: white;
        }

        #uno {
            background-color: rgb(61, 57, 48);
        }

        #dos {
            background-color: rgb(9, 2, 41);
        }

        footer {
            color: white;
            line-height: 10px;
            text-align: center;
            padding-top: 20px;
            padding-bottom: 5px;
            font-size: 15px;
            font-family: 'Lato', sans-serif;
            position: absolute;
            bottom: 0;
            width: 100%;
            background-image: url("images/footer.png");
        }'''
        try:
            reporte_errores = open("Errores/errores.html", "w",encoding="utf8")
            reporte_errores.write(html)
            reporte_errores.close()
            print("->HTML Errores generado")
            css_errores = open("Errores/errores.css", "w",encoding="utf8")
            css_errores.write(css)
            css_errores.close()
            print("->CSS Errores generado")
            return True
        except:
            traceback.print_exc()
            return False