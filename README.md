# Compilador EPK

## Nombre
Elías Abraham Vasquez Soto

## Carné
201900131

## Curso
LABORATORIO ORGANIZACION DE LENGUAJES Y COMPILADORES 1 Sección N

## Descripción
Aplicación de escritorio desarrollada en Python, que utiliza un analizador léxico, sintáctico y semántico sobre las fases de un compilador, para interpretar código de alto nivel, y realizar distintas operaciones. Genera reportes de errores, tabla de símbolos generada y árbol AST.
