class Simbolo:
    def __init__(self, tipo = "", nombre = "", valor = None, linea = 0, tipo_datos_array = None, num_dimensiones = None, tipo_retorno = None, parametros = None):
        self.tipo = tipo
        self. nombre = nombre
        # Arreglo cuando self.tipo == "array", Lista de instrucciones cuando self.tipo == "funcion"
        self.valor = valor
        self.linea = linea
        # cuando self.tipo == "array", cuando no: None
        self.tipo_datos_array = tipo_datos_array
        # cuando self.tipo == "array", cuando no: None
        self.num_dimensiones  = num_dimensiones
        # cuando self.tipo == "funcion", cuando no: None
        self.tipo_retorno = tipo_retorno
        # cuando self.tipo == "funcion" list o None, cuando no: None
        self.parametros = parametros