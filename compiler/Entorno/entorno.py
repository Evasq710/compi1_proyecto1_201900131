if __name__ == "compiler.Entorno.entorno":
    from compiler.Entorno.simbolo import Simbolo
else:
    from Entorno.simbolo import Simbolo

class Entorno:
    def __init__(self, anterior = None):
        self.anterior = anterior
        self.tabla_simbolos = {}
        self.tabla_simbolos_funciones = {}
        self.entorno_for = False
    
    def insertar_simbolo(self, simbolo: Simbolo):
        llave = simbolo.nombre
        existing_symbol = self.tabla_simbolos.get(llave)
        if existing_symbol is None:
            self.tabla_simbolos[llave] = simbolo
            return True
        return False
    
    def insertar_funcion(self, simbolo: Simbolo):
        llave = simbolo.nombre
        existing_function = self.tabla_simbolos_funciones.get(llave)
        if existing_function is None:
            self.tabla_simbolos_funciones[llave] = simbolo
            return True
        return False
    
    def editar_simbolo(self, nombre, nuevo: Simbolo):
        entorno_actual = self
        while entorno_actual is not None:
            existing_symbol = entorno_actual.tabla_simbolos.get(nombre)
            if existing_symbol is not None:
                entorno_actual.tabla_simbolos[nombre] = nuevo
                return True
            entorno_actual = entorno_actual.anterior
        return False
    
    def eliminar_simbolo(self, nombre):
        entorno_actual = self
        while entorno_actual is not None:
            existing_symbol = entorno_actual.tabla_simbolos.get(nombre)
            if existing_symbol is not None:
                del self.tabla_simbolos[nombre]
                return True
            entorno_actual = entorno_actual.anterior
        return False
    
    def buscar_simbolo(self, nombre):
        entorno_actual = self
        while entorno_actual is not None:
            existing_symbol = entorno_actual.tabla_simbolos.get(nombre)
            if existing_symbol is not None:
                return existing_symbol
            entorno_actual = entorno_actual.anterior
        return None
    
    def buscar_funcion(self, nombre):
        entorno_actual = self
        while entorno_actual is not None:
            existing_function = entorno_actual.tabla_simbolos_funciones.get(nombre)
            if existing_function is not None:
                return existing_function
            entorno_actual = entorno_actual.anterior
        return None