import ply.lex as lex

# **********LEXICO**********

class Scanner(object):
    def __init__(self):
        self.input_data = ""
        self.errores_lexicos = []

        self.reservadas = {
            # primitivos
            'int': 'int',
            'double': 'double',
            'boolean': 'boolean',
            'true': 'true', # tambien 1
            'false': 'false', # tambien 0
            'char': 'char',
            'string': 'string',
            'null': 'null',
            # arreglos
            'new': 'new',
            # sentencias de control
            'if': 'if',
            'cond': 'cond',
            'instr': 'instr',
            'else': 'else',
            'switch': 'switch',
            'expr': 'expr',
            'case': 'case',
            'default': 'default',
            'while': 'while',
            'do': 'do',
            'for': 'for',
            # sentencias de transferencia
            'break': 'break',
            'return': 'return',
            # funciones
            'func': 'func',
            'tipo': 'tipo',
            'void': 'void',
            'nombre': 'nombre',
            'param': 'param',
            # funciones nativas
            'imprimir': 'imprimir',
            'aminus': 'aminus',
            'amayus': 'amayus',
            'tamano': 'tamano',
            'truncate': 'truncate',
            'round': 'round',
            'tablasimbolos': 'tablasimbolos'
        }

        # Operadores aritméticos
        self.t_mas = r"\+"
        self.t_menos = r"-"
        self.t_producto = r"\*"
        self.t_cociente = r"/"
        self.t_potencia = r"\*\*"
        self.t_modulo = r"%"
        # Operadores relacionales
        self.t_igualigual = r"=="
        self.t_diferentede = r"!="
        self.t_menorque = r"<"
        self.t_mayorque = r">"
        self.t_menorigual = r"<="
        self.t_mayorigual = r">="
        # Operadores lógicos
        self.t_or = r"\|\|"
        self.t_and = r"&&"
        self.t_not = r"!"
        # Signos de agrupación
        self.t_parA = r"\("
        self.t_parC = r"\)"
        # Finalización y encapsulamiento
        self.t_pyc = r";"
        self.t_coma = r","
        self.t_llaveA = r"{"
        self.t_llaveC = r"}"
        # Asignación
        self.t_dosP = r":"
        # Inc - Dec
        self.t_incremento = r"\+\+"
        self.t_decremento = r"--"
        # Arreglos
        self.t_corchA = r"\["
        self.t_corchC = r"\]"

        self.tokens = [
            "mas", "menos", "producto", "cociente", "potencia", "modulo",
            "igualigual", "diferentede", "menorque", "mayorque", "menorigual", "mayorigual",
            "or", "and", "not", "parA", "parC", "pyc", "coma", "llaveA", "llaveC", "dosP",
            "incremento", "decremento", "corchA", "corchC",
            "id", "numero","caracter", "cadena", "comentario_simple", "comentario_multilinea"
        ] + list(self.reservadas.values())

    def t_id(self, t):
        r'[a-zA-Z_][a-zA-Z_0-9]*'
        # get(Key Palabra reservada que devuelve el token, id por default) <-- se le asigna a t.type
        t.type = self.reservadas.get(t.value.lower(), 'id')
        t.value = t.value.lower()
        return t
    
    def t_numero(self, t):
        r'\d+(\.\d+)?'
        try:
            if t.value.count('.') == 1:
                t.value = float(t.value)
            else:
                t.value = int(t.value)
        except ValueError:
            print("Valor numérico incorrecto %d", t.value)
            t.value = 0
        return t
    
    def t_caracter(self, t):
        r"'((\\n)|(\\\\)|(\\\")|(\\t)|(\\')|[^\\'])'"
        t.value = t.value[1:-1]
        t.value = t.value.replace('\\n', '\n').replace('\\r', '\r').replace('\\t', '\t').replace('\\"', '\"').replace('\\\'', '\'').replace('\\\\', '\\')
        t.value = t.value.replace('\\N', '\n').replace('\\R', '\r').replace('\\T', '\t')
        return t

    # TODO Verificar caracteres especiales en la cadena
    def t_cadena(self, t):
        r'"(\\n|\\r|\\t|\\"|\\\'|\\\\|[^\n\r])*?"'
        t.value = t.value[1:-1]
        t.value = t.value.replace('\\n', '\n').replace('\\r', '\r').replace('\\t', '\t').replace('\\"', '\"').replace('\\\'', '\'').replace('\\\\', '\\')
        t.value = t.value.replace('\\N', '\n').replace('\\R', '\r').replace('\\T', '\t')
        return t
    
    #TODO Verificar funcionamiento de comentarios
    def t_comentario_multilinea(self, t):
        r'\#&([^&#]|[^&]/|&[^#])*&*&\#'
        t.lexer.lineno += t.value.count('\n')
    
    #TODO Verificar funcionamiento de comentarios
    def t_comentario_simple(self, t):
        r'\#.*'
        #r'\#[^\*].*\n'

    t_ignore = " \t\r"

    # Compute column.
    # input is the input text string
    # token is a token instance
    def find_column(self, input, token):
        line_start = input.rfind('\n', 0, token.lexpos) + 1
        return (token.lexpos - line_start) + 1

    def t_newline(self, t):
        r'\n+'
        t.lexer.lineno += t.value.count("\n")

    def t_error(self, t):
        print("Caracter invalido '%s'" % t.value[0])
        self.errores_lexicos.append(Error("Léxico", "Caracter invalido '%s'" % t.value[0], t.lineno, self.find_column(self.input_data, t)))
        t.lexer.skip(1)

    def build_scanner(self, **kwargs):
        self.scanner = lex.lex(module=self, **kwargs)

    def test_scanner(self, data):
        self.scanner.input(data)
        while True:
            tok = self.scanner.token()
            if not tok: 
                break
            # print("tok:", tok, "tok.type:", tok.type, "tok.value:", tok.value, "tok.lineno:", tok.lineno)
            # print("tok:", tok)

if __name__ == "__main__":
    from Acciones.Interface.Error import Error

    scanner = Scanner()
    scanner.build_scanner()
    primitivos1 = '56632491 5.0 53246540.646546 0. .4 5.5'
    primitivos2 = "\"Esto es una cadena\" \"\\n \\t \\\\ \\' \\\" \\ \" 'a' '\\n' '\\t'"
    switch = '''switch : {
    expr : edad,
    case : {
    expr : 18,
    instr : {
    imprimir("tengo 18 años, uff");
    break;
    }
    },
    case : {
    expr : 24,
    instr : {
    imprimir("tengo casi 25, arre");
    break;
    }
    },
    case : {
    expr : 31,
    instr : {
    imprimir("ya estoy viejo :c");
    break;
    }
    },
    default : {
    instr : {
    imprimir("ah rayos, olvidé mi edad :c");
    # aca pueden ir mas instrucciones para realizar
    }
    }
    }'''
    potente = '''int global_b;              # Declara una variable con valor nulo
    int global_int : 25;      # Declara una variable de tipo entero
    double global_dou : 25.0;    # Declara una variable de tipo double
    boolean global_boo : true;     # Declara una variable de tipo boolean
    char global_cha : 'g';      # Declara una variable de tipo char
    String global_cad : "cadena"; # Declara una variable de tipo string
    String GlObAl_A;             # Error: La variable ya esta definida
    String global_salto : "Cadena con\\nsalto de linea";
    String global_barra : "Cadena con \\\\ barra invertida";
    String global_comilla_doble : "Cadena con \\"comilla doble\\"";
    String global_tab : "Cadena con \\t tabulacion";
    String global_comilla_simple : "Cadena con \\'comilla simple\\'";

    imprimir("---------- CARACTERES ESPECIALES ----------");
    imprimir("-> Salto de linea:");
    imprimir(global_salto);
    imprimir("-------------------------------------------");
    imprimir("-> Barra invertida:");
    imprimir(global_barra);
    imprimir("-------------------------------------------");
    imprimir("-> Comilla doble:");
    imprimir(global_comilla_doble);
    imprimir("-------------------------------------------");
    imprimir("-> Tabulacion:");
    imprimir(global_tab);
    imprimir("-------------------------------------------");
    imprimir("-> Comilla simple:");
    imprimir(global_comilla_simple);
    imprimir("-------------------------------------------\\n");

    imprimir("--------------------------------------------");
    int variablePrueba : 58;
    int i : 0;

    while : {
        cond : (i < (variablePrueba + (89/7))),
        instr : {
            double pruebita : 2**-30.88*3-1;
            if : {
                cond : (i == 0),
                instr : {
                    imprimir("Inicio del ciclo while\\t" + i);
                }
            }
            else if : {
                cond : (i >= pruebita && i < (pruebita+1)),
                instr : {
                    imprimir("Maomenos por la mitad de while\\t"+i);
                    tablaSimbolos("entornoWhile");
                }
            }
            i++;
        }
    }

    imprimir("--------------------------------------------");

    double N : 13;
    double factorial;
    for : {
        cond : (i : 0; i <= N ; i++),
        instr : {
            #&&&&
                para cada numero desde 1 hasta N
                se calcula su factorial
            &&&&&&#

            factorial : 1;
            for : {
                cond : (int j : 1; j <= i; j++),
                instr : {
                    factorial : factorial * j;
                }
            }

            #### se muestra el factorial
            imprimir(i + "! = " + factorial);
        }
    }

    imprimir("--------------------------------------------");

    #&& este ciclo va como spoiler para fase II
    para probar con casteo 

    for (char x : 'Z'; x >= 'A'; x--) {
        String control : "";
        for (char y : x; y >= 'A'; y--) {
            control : control + y;
        }
        imprimir(control);
    }

    imprimir("--------------------------------------------");

    &&&&#

    N : 9;
    double suma : 0;
    for : {
        cond : (i : 0; i <= N; i++) ,
        instr : {

            #&&&&
                para cada numero desde 1 hasta N
                se calcula su factorial
            &&&&&&#

            factorial : 1;
            for : {
                cond : (int j : 1; j <= i; j++),
                instr : {

                    factorial : factorial * j;
                    
                    #### se muestra la tabla de simbolos en cada iteracion
                    tablaSimbolos("entornoFactorial" + j);
                }
            }

            ### se muestra el factorial
            imprimir(i + "! = " + factorial);

            ## se suma el factorial
            suma : suma + factorial;
        } 
    }

    ### Al final del proceso se muestra la suma de todos los factoriales
    imprimir("Suma de los factoriales desde 0 hasta " + N + " es " + suma);

    imprimir("\\n--------------------------------------------");

    int x : 21;
    int sum : 0;

    do : {
        instr : {

            sum : sum + x;
            x--;
        },
        while : {
            cond : (x > 10)
        }
    }
    imprimir("\\nSuma: " + sum);

    imprimir("\\n--------------------------------------------");


    Double n1 : -1.0;
    double n2 : 4.5;
    double n3 : -5.3;
    Double largest;

    if : {
        cond : (n1 >= n2) ,
        instr : {
            if : {
                cond : (n1 >= n3),
                instr : {
                    largest : n1;
                }
            }

            else : {
                instr : {
                    largest : n3;
                }
            }
        }
    } else : {
        instr : {
            if : {
                cond : (n2 >= n3),
                instr : {
                    largest : n2;
                }
            }

            else : {
                instr : {
                    largest : n3;
                }
            }
        }
    }

    imprimir("Numero mas largo: " + largest);



    #&&&&&&& LA SALIDA DE ESTE ARCHIVO ES LA SIGUIENTE : 

    ---------- CARACTERES ESPECIALES ----------
    -> Salto de linea:
    Cadena con
    salto de linea
    -------------------------------------------
    -> Barra invertida:
    Cadena con \ barra invertida
    -------------------------------------------
    -> Comilla doble:
    Cadena con "comilla doble"
    -------------------------------------------
    -> Tabulacion:
    Cadena con 	 tabulacion
    -------------------------------------------
    -> Comilla simple:
    Cadena con 'comilla simple'
    -------------------------------------------

    --------------------------------------------
    Inicio del ciclo while	0
    Maomenos por la mitad de while	35
    --------------------------------------------
    0! = 1.0
    1! = 1.0
    2! = 2.0
    3! = 6.0
    4! = 24.0
    5! = 120.0
    6! = 720.0
    7! = 5040.0
    8! = 40320.0
    9! = 362880.0
    10! = 3628800.0
    11! = 3.99168E7
    12! = 4.790016E8
    13! = 6.2270208E9
    --------------------------------------------

    0! = 1.0
    1! = 1.0
    2! = 2.0
    3! = 6.0
    4! = 24.0
    5! = 120.0
    6! = 720.0
    7! = 5040.0
    8! = 40320.0
    9! = 362880.0
    Suma de los factoriales desde 0 hasta 9.0 es 409114.0

    --------------------------------------------

    Suma: 176

    --------------------------------------------
    Numero mas largo: 4.5

    &&&#'''
    prueba = "int ¡ hola  $ : 5; ?"
    scanner.input_data = prueba
    scanner.test_scanner(prueba)
    for error in scanner.errores_lexicos:
        print(error.reporte_str_error())
elif __name__ == "compiler.scanner":
    from compiler.Acciones.Interface.Error import Error
else:
    from Acciones.Interface.Error import Error