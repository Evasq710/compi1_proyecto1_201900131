import ply.yacc as yacc

# ******************* Control ID's y Nodos del Arbol CST *********************
i = 0
def id_node():
    global i
    i += 1
    return str(i)

nodos = []
arbol_CST = None
# ***************************************************************************

input_string = ""
txtbox_console = None
errores_sintacticos = []

precedence = (
    ('left', 'or'),
    ('left', 'and'),
    ('right', 'not'),
    ('left', 'igualigual', 'diferentede', 'menorque', 'menorigual', 'mayorque', 'mayorigual'),
    ('left', 'mas', 'menos'),
    ('left', 'cociente', 'producto', 'modulo'),
    ('nonassoc', 'potencia'),
    ('right', 'negativo'),
    ('right', 'casteo'),
)

def p_inicio(p):
    'INICIO : INSTRUCCIONES'
    p[0] = p[1]

    global nodos
    global arbol_CST
    id_init = id_node()
    n1 = Nodo(Dato(id_init, "INICIO", inicio=True))
    nodos[-1].dato.id_nodo_padre = id_init
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'INICIO'
    nodos.append(n1)

    # Creación del arbol CST en graphviz
    dot_file_nodos = nodos[0].crear_nodos_graphviz()
    arbol_CST = Arbol_Graphviz(dot_file_nodos)

def p_instrucciones_recursivas(p):
    'INSTRUCCIONES : INSTRUCCIONES ACCIONES'
    p[1].append(p[2])
    p[0] = p[1]

    try:
        global nodos
        id_inst = id_node()
        n1 = Nodo(Dato(id_inst, "INSTRUCCIONES"))
        nodos[-2].dato.id_nodo_padre = id_inst
        n1.insertar_hijo(nodos.pop(-2))
        nodos[-1].dato.id_nodo_padre = id_inst
        n1.insertar_hijo(nodos.pop())
        # NODOS REDUCIDOS EN 'INSTRUCCIONES'
        nodos.append(n1)
    except Exception as e:
        print(e)

def p_instrucciones_acciones(p):
    'INSTRUCCIONES : ACCIONES'
    p[0] = [p[1]]

    global nodos
    id_inst = id_node()
    n1 = Nodo(Dato(id_inst, "INSTRUCCIONES"))
    nodos[-1].dato.id_nodo_padre = id_inst
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'INSTRUCCIONES'
    nodos.append(n1)

def p_acciones(p):
    '''ACCIONES : DECLARACION
                | ASIGNACION
                | DECLARACION_ARR
                | NATIVAS
                | INC_DEC
                | WHILE
                | DO_WHILE
                | FOR
                | IF
                | SWITCH
                | DECLARACION_FUNCION
                | LLAMADA_FUNCION
                | BREAK
                | RETURN'''
    p[0] = p[1]

    global nodos
    id_acc = id_node()
    n1 = Nodo(Dato(id_acc, "ACCIONES"))
    nodos[-1].dato.id_nodo_padre = id_acc
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'ACCIONES'
    nodos.append(n1)

def p_declaracion(p):
    'DECLARACION : TIPO id dosP EXPRESION pyc'
    p[0] = Declaracion(p.lineno(2), find_column(input_string, p.slice[2]), p[2], p[1], p[4])

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "DECLARACION"))
    nodos[-2].dato.id_nodo_padre = id_decl
    n1.insertar_hijo(nodos.pop(-2))
    id_id = id_node()
    n2 = Nodo(Dato(id_id, str(p[2]), hoja_lexema=True, id_nodo_padre=id_decl))
    n1.insertar_hijo(n2)
    id_dosP = id_node()
    n3 = Nodo(Dato(id_dosP, str(p[3]), hoja_lexema=True, id_nodo_padre=id_decl))
    n1.insertar_hijo(n3)
    nodos[-1].dato.id_nodo_padre = id_decl
    n1.insertar_hijo(nodos.pop())
    id_pyc = id_node()
    n4 = Nodo(Dato(id_pyc, str(p[5]), hoja_lexema=True, id_nodo_padre=id_decl))
    n1.insertar_hijo(n4)
    # NODOS REDUCIDOS EN 'DECLARACION'
    nodos.append(n1)

def p_declaracion_nula(p):
    'DECLARACION : TIPO id pyc'
    prm_null = Primitivo(p.lineno(2), find_column(input_string, p.slice[2]), 'null', None)
    p[0] = Declaracion(p.lineno(2), find_column(input_string, p.slice[2]), p[2], p[1], prm_null)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "DECLARACION"))
    nodos[-1].dato.id_nodo_padre = id_decl
    n1.insertar_hijo(nodos.pop())
    id_id = id_node()
    n2 = Nodo(Dato(id_id, str(p[2]), hoja_lexema=True, id_nodo_padre=id_decl))
    n1.insertar_hijo(n2)
    id_pyc = id_node()
    n3 = Nodo(Dato(id_pyc, str(p[3]), hoja_lexema=True, id_nodo_padre=id_decl))
    n1.insertar_hijo(n3)
    # NODOS REDUCIDOS EN 'DECLARACION'
    nodos.append(n1)

def p_declaracion_error1(p):
    '''DECLARACION : TIPO id dosP EXPRESION error pyc'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado en Declaración de variable.", p.lineno(2), find_column(input_string, p.slice[2]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "DECLARACION"))
    nodos.pop()
    nodos.pop()
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'DECLARACION'
    nodos.append(n1)

def p_declaracion_error2(p):
    '''DECLARACION : TIPO id dosP error pyc
                    | TIPO id error pyc'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado en Declaración de variable.", p.lineno(2), find_column(input_string, p.slice[2]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "DECLARACION"))
    nodos.pop()
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'DECLARACION'
    nodos.append(n1)

def p_declaracion_error3(p):
    '''DECLARACION : TIPO error pyc'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado en Declaración de variable, se esperaba un ID.", p.lineno(3), find_column(input_string, p.slice[3]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "DECLARACION"))
    nodos.pop()
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'DECLARACION'
    nodos.append(n1)

def p_tipo(p):
    '''TIPO : int
            | double
            | boolean
            | char
            | string'''
    p[0] = p[1].lower()

    global nodos
    id_tipo = id_node()
    n1 = Nodo(Dato(id_tipo, "TIPO"))
    id = id_node()
    n2 = Nodo(Dato(id, str(p[1]), hoja_lexema=True, id_nodo_padre=id_tipo))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'TIPO'
    nodos.append(n1)

def p_exp1_null(p):
    # Expresion - Primitivos
    '''EXPRESION : null'''
    p[0] = Primitivo(p.lineno(1), find_column(input_string, p.slice[1]), p.slice[1].type, None)

    global nodos
    id_primitivo = id_node()
    n1 = Nodo(Dato(id_primitivo, "EXPRESION"))
    id_valor = id_node()
    n2 = Nodo(Dato(id_valor, str(p[1]), hoja_lexema=True, id_nodo_padre=id_primitivo))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'EXPRESION'
    nodos.append(n1)

def p_exp1_num(p):
    # Expresion - Primitivos
    '''EXPRESION : numero'''
    tipo = 'int' if type(p.slice[1].value) == int else 'double'
    p[0] = Primitivo(p.lineno(1), find_column(input_string, p.slice[1]), tipo,  p.slice[1].value)

    global nodos
    id_primitivo = id_node()
    n1 = Nodo(Dato(id_primitivo, "EXPRESION"))
    id_valor = id_node()
    n2 = Nodo(Dato(id_valor, str(p[1]), hoja_lexema=True, id_nodo_padre=id_primitivo))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'EXPRESION'
    nodos.append(n1)

def p_exp1_caracteres(p):
    # Expresion - Primitivos
    '''EXPRESION : caracter
                | cadena'''
    tipo = 'string' if p.slice[1].type == 'cadena' else 'char'
    p[0] = Primitivo(p.lineno(1), find_column(input_string, p.slice[1]), tipo,  p.slice[1].value)

    global nodos
    id_primitivo = id_node()
    n1 = Nodo(Dato(id_primitivo, "EXPRESION"))
    id_valor = id_node()
    cadena_graphviz = str(p[1]).replace('"', '\\"')
    n2 = Nodo(Dato(id_valor, cadena_graphviz, hoja_lexema=True, id_nodo_padre=id_primitivo))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'EXPRESION'
    nodos.append(n1)

def p_exp1_boolean(p):
    # Expresion - Primitivos
    '''EXPRESION : false
                | true'''
    valor = True if p.slice[1].value == "true" else False
    p[0] = Primitivo(p.lineno(1), find_column(input_string, p.slice[1]), 'boolean',  valor)
    
    global nodos
    id_primitivo = id_node()
    n1 = Nodo(Dato(id_primitivo, "EXPRESION"))
    id_valor = id_node()
    n2 = Nodo(Dato(id_valor, str(p[1]), hoja_lexema=True, id_nodo_padre=id_primitivo))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'EXPRESION'
    nodos.append(n1)

def p_e_inc_dec(p):
    'EXPRESION : EXP_INC_DEC'
    p[0] = p[1]

    global nodos
    id_exp = id_node()
    n1 = Nodo(Dato(id_exp, "EXPRESION"))
    nodos[-1].dato.id_nodo_padre = id_exp
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'EXPRESION'
    nodos.append(n1)

def p_exp_inc_dec(p):
    '''EXP_INC_DEC : id incremento
                | id decremento'''
    p[0] = E_Inc_Dec(p.lineno(1), find_column(input_string, p.slice[1]), p.slice[1].value, p.slice[2].value)

    global nodos
    id_exp = id_node()
    n1 = Nodo(Dato(id_exp, "EXP_INC_DEC"))
    id_id = id_node()
    n2 = Nodo(Dato(id_id, str(p[1]), hoja_lexema=True, id_nodo_padre=id_exp))
    n1.insertar_hijo(n2)
    id_inc_dec = id_node()
    n3 = Nodo(Dato(id_inc_dec, str(p[2]), hoja_lexema=True, id_nodo_padre=id_exp))
    n1.insertar_hijo(n3)
    # NODOS REDUCIDOS EN 'EXP_INC_DEC'
    nodos.append(n1)

def p_exp2_id(p):
    '''EXPRESION : id'''
    p[0] = Identificador(p.lineno(1), find_column(input_string, p.slice[1]), p[1])

    global nodos
    id_primitivo = id_node()
    n1 = Nodo(Dato(id_primitivo, "EXPRESION"))
    id_valor = id_node()
    n2 = Nodo(Dato(id_valor, str(p[1]), hoja_lexema=True, id_nodo_padre=id_primitivo))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'EXPRESION'
    nodos.append(n1)

# TODO Errores sintácticos Acceso a arreglos
def p_exp_id_arreglo(p):
    'EXPRESION : id SIZE_N_DIMENSIONES'
    p[0] = Identificador_Arr(p.lineno(1), find_column(input_string, p.slice[1]), p[1], p[2])

    global nodos
    id_exp = id_node()
    n1 = Nodo(Dato(id_exp, "EXPRESION"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_exp))
    n1.insertar_hijo(n2)
    nodos[-1].dato.id_nodo_padre = id_exp
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'EXPRESION'
    nodos.append(n1)

def p_exp3_aritmeticas(p):
    # Expresion -> Operaciones aritméticas
    '''EXPRESION : EXPRESION mas EXPRESION
                | EXPRESION menos EXPRESION
                | EXPRESION producto EXPRESION
                | EXPRESION cociente EXPRESION
                | EXPRESION potencia EXPRESION
                | EXPRESION modulo EXPRESION'''
    p[0] = Aritmetica(p.lineno(2), 1, p.slice[2].value, p[1], p[3])

    global nodos
    id_op_aritmetica = id_node()
    n1 = Nodo(Dato(id_op_aritmetica, "EXPRESION"))
    nodos[-2].dato.id_nodo_padre = id_op_aritmetica
    n1.insertar_hijo(nodos.pop(-2))
    id_operando = id_node()
    n2 = Nodo(Dato(id_operando, str(p[2]), hoja_lexema=True, id_nodo_padre=id_op_aritmetica))
    n1.insertar_hijo(n2)
    nodos[-1].dato.id_nodo_padre = id_op_aritmetica
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'EXPRESION'
    nodos.append(n1)

def p_exp4_logicas(p):
    '''EXPRESION : EXPRESION or EXPRESION
                | EXPRESION and EXPRESION'''
    p[0] = Logica(p.lineno(2), 1,p.slice[2].value,p[1], p[3])

    global nodos
    id_exp_logica = id_node()
    n1 = Nodo(Dato(id_exp_logica, "EXPRESION"))
    nodos[-2].dato.id_nodo_padre = id_exp_logica
    n1.insertar_hijo(nodos.pop(-2))
    id_operando = id_node()
    n2 = Nodo(Dato(id_operando, str(p[2]), hoja_lexema=True, id_nodo_padre=id_exp_logica))
    n1.insertar_hijo(n2)
    nodos[-1].dato.id_nodo_padre = id_exp_logica
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'EXPRESION'
    nodos.append(n1)

def p_exp5_relacionales(p):
    '''EXPRESION : EXPRESION igualigual EXPRESION
                | EXPRESION diferentede EXPRESION
                | EXPRESION menorque EXPRESION
                | EXPRESION mayorque EXPRESION
                | EXPRESION menorigual EXPRESION
                | EXPRESION mayorigual EXPRESION'''
    p[0] = Relacional(p.lineno(2), 1, p.slice[2].value, p[1], p[3])

    global nodos
    id_op_relacional = id_node()
    n1 = Nodo(Dato(id_op_relacional, "EXPRESION"))
    nodos[-2].dato.id_nodo_padre = id_op_relacional
    n1.insertar_hijo(nodos.pop(-2))
    id_operando = id_node()
    n2 = Nodo(Dato(id_operando, str(p[2]), hoja_lexema=True, id_nodo_padre=id_op_relacional))
    n1.insertar_hijo(n2)
    nodos[-1].dato.id_nodo_padre = id_op_relacional
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'EXPRESION'
    nodos.append(n1)

def p_exp6_unarias(p):
    '''EXPRESION : not EXPRESION
                | menos EXPRESION %prec negativo'''
    p[0] = Negacion(p.lineno(1), find_column(input_string, p.slice[1]), p.slice[1].value, p[2])

    global nodos
    id_exp = id_node()
    n1 = Nodo(Dato(id_exp, "EXPRESION"))
    id_tk = id_node()
    n2 = Nodo(Dato(id_tk, str(p[1]), hoja_lexema=True, id_nodo_padre=id_exp))
    n1.insertar_hijo(n2)
    nodos[-1].dato.id_nodo_padre = id_exp
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'DECLARACION'
    nodos.append(n1)

def p_agrupacion(p):
    #Expresión -> Agrupación de expresiones
    '''EXPRESION : parA EXPRESION parC'''
    p[0] = p[2]

    global nodos
    id_exp = id_node()
    n1 = Nodo(Dato(id_exp, "EXPRESION"))
    id_parA = id_node()
    n2 = Nodo(Dato(id_parA, str(p[1]), hoja_lexema=True, id_nodo_padre=id_exp))
    n1.insertar_hijo(n2)
    nodos[-1].dato.id_nodo_padre = id_exp
    n1.insertar_hijo(nodos.pop())
    id_parC = id_node()
    n3 = Nodo(Dato(id_parC, str(p[3]), hoja_lexema=True, id_nodo_padre=id_exp))
    n1.insertar_hijo(n3)
    # NODOS REDUCIDOS EN 'EXPRESION'
    nodos.append(n1)

# TODO Errores sintácticos Casteo
def p_exp_casteo(p):
    'EXPRESION : corchA TIPO corchC EXPRESION %prec casteo'
    p[0] = Casteo(p.lineno(1), find_column(input_string, p.slice[1]), p[2], p[4])

    global nodos
    id_exp = id_node()
    n1 = Nodo(Dato(id_exp, "EXPRESION"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_exp))
    n1.insertar_hijo(n2)
    nodos[-2].dato.id_nodo_padre = id_exp
    n1.insertar_hijo(nodos.pop(-2))
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[3]), hoja_lexema=True, id_nodo_padre=id_exp))
    n1.insertar_hijo(n3)
    nodos[-1].dato.id_nodo_padre = id_exp
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'EXPRESION'
    nodos.append(n1)

def p_exp_nativas(p):
    '''EXPRESION : EXPR_MINUS
                | EXPR_MAYUS
                | EXPR_TRUNCATE
                | EXPR_ROUND
                | EXPR_TAMANO'''
    p[0] = p[1]

    global nodos
    id_nat = id_node()
    n1 = Nodo(Dato(id_nat, "EXPRESION"))
    nodos[-1].dato.id_nodo_padre = id_nat
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'EXPRESION'
    nodos.append(n1)

# TODO Errores sintácticos expresiones nativas
def p_exp_minus(p):
    'EXPR_MINUS : aminus parA EXPRESION parC'
    p[0] = AMinus(p.lineno(1), find_column(input_string, p.slice[1]), p[3])

    global nodos
    id_instr = id_node()
    n1 = Nodo(Dato(id_instr, "EXPR_MINUS"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n3)
    nodos[-1].dato.id_nodo_padre = id_instr
    n1.insertar_hijo(nodos.pop())
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[4]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n4)
    # NODOS REDUCIDOS EN 'EXPR_MINUS'
    nodos.append(n1)

def p_exp_mayus(p):
    'EXPR_MAYUS : amayus parA EXPRESION parC'
    p[0] = AMayus(p.lineno(1), find_column(input_string, p.slice[1]), p[3])

    global nodos
    id_instr = id_node()
    n1 = Nodo(Dato(id_instr, "EXPR_MAYUS"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n3)
    nodos[-1].dato.id_nodo_padre = id_instr
    n1.insertar_hijo(nodos.pop())
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[4]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n4)
    # NODOS REDUCIDOS EN 'EXPR_MAYUS'
    nodos.append(n1)

def p_exp_truncate(p):
    'EXPR_TRUNCATE : truncate parA EXPRESION parC'
    p[0] = Truncate(p.lineno(1), find_column(input_string, p.slice[1]), p[3])

    global nodos
    id_instr = id_node()
    n1 = Nodo(Dato(id_instr, "EXPR_TRUNCATE"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n3)
    nodos[-1].dato.id_nodo_padre = id_instr
    n1.insertar_hijo(nodos.pop())
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[4]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n4)
    # NODOS REDUCIDOS EN 'EXPR_TRUNCATE'
    nodos.append(n1)

def p_exp_round(p):
    'EXPR_ROUND : round parA EXPRESION parC'
    p[0] = Round(p.lineno(1), find_column(input_string, p.slice[1]), p[3])

    global nodos
    id_instr = id_node()
    n1 = Nodo(Dato(id_instr, "EXPR_ROUND"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n3)
    nodos[-1].dato.id_nodo_padre = id_instr
    n1.insertar_hijo(nodos.pop())
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[4]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n4)
    # NODOS REDUCIDOS EN 'EXPR_ROUND'
    nodos.append(n1)

def p_exp_tamano(p):
    'EXPR_TAMANO : tamano parA EXPRESION parC'
    p[0] = Tamano(p.lineno(1), find_column(input_string, p.slice[1]), p[3])

    global nodos
    id_instr = id_node()
    n1 = Nodo(Dato(id_instr, "EXPR_TAMANO"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n3)
    nodos[-1].dato.id_nodo_padre = id_instr
    n1.insertar_hijo(nodos.pop())
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[4]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n4)
    # NODOS REDUCIDOS EN 'EXPR_TAMANO'
    nodos.append(n1)

def p_declaracion_arr(p):
    '''DECLARACION_ARR : ARRAY_TIPO1
                        | ARRAY_TIPO2'''
    p[0] = p[1]

    global nodos
    id_dec_arr = id_node()
    n1 = Nodo(Dato(id_dec_arr, "DECLARACION_ARR"))
    nodos[-1].dato.id_nodo_padre = id_dec_arr
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'DECLARACION_ARR'
    nodos.append(n1)

# TODO Errores sintácticos Declaracion_Array1
def p_array_tipo1(p):
    'ARRAY_TIPO1 : TIPO N_DIMENSIONES id dosP new TIPO SIZE_N_DIMENSIONES pyc'
    p[0] = Declaracion_Array1(p.lineno(3), find_column(input_string, p.slice[3]), p[1], p[2], p[3], p[6], p[7])

    global nodos
    id_arr1 = id_node()
    n1 = Nodo(Dato(id_arr1, "ARRAY_TIPO1"))
    nodos[-4].dato.id_nodo_padre = id_arr1
    n1.insertar_hijo(nodos.pop(-4))
    nodos[-3].dato.id_nodo_padre = id_arr1
    n1.insertar_hijo(nodos.pop(-3))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[3]), hoja_lexema=True, id_nodo_padre=id_arr1))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[4]), hoja_lexema=True, id_nodo_padre=id_arr1))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[5]), hoja_lexema=True, id_nodo_padre=id_arr1))
    n1.insertar_hijo(n4)
    nodos[-2].dato.id_nodo_padre = id_arr1
    n1.insertar_hijo(nodos.pop(-2))
    nodos[-1].dato.id_nodo_padre = id_arr1
    n1.insertar_hijo(nodos.pop())
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[8]), hoja_lexema=True, id_nodo_padre=id_arr1))
    n1.insertar_hijo(n5)
    # NODOS REDUCIDOS EN 'ARRAY_TIPO1'
    nodos.append(n1)

def p_n_dimensiones_recursivo(p):
    'N_DIMENSIONES : N_DIMENSIONES corchA corchC'
    p[0] = p[1] + 1

    global nodos
    id_n_dim = id_node()
    n1 = Nodo(Dato(id_n_dim, "N_DIMENSIONES"))
    nodos[-1].dato.id_nodo_padre = id_n_dim
    n1.insertar_hijo(nodos.pop())
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[2]), hoja_lexema=True, id_nodo_padre=id_n_dim))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[3]), hoja_lexema=True, id_nodo_padre=id_n_dim))
    n1.insertar_hijo(n3)
    # NODOS REDUCIDOS EN 'N_DIMENSIONES'
    nodos.append(n1)

def p_n_dimensiones(p):
    'N_DIMENSIONES : corchA corchC'
    p[0] = 1

    global nodos
    id_n_dim = id_node()
    n1 = Nodo(Dato(id_n_dim, "N_DIMENSIONES"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_n_dim))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_n_dim))
    n1.insertar_hijo(n3)
    # NODOS REDUCIDOS EN 'N_DIMENSIONES'
    nodos.append(n1)

def p_size_n_dimensiones_recursivo(p):
    'SIZE_N_DIMENSIONES : SIZE_N_DIMENSIONES corchA EXPRESION corchC'
    p[1].append(p[3])
    p[0] = p[1]

    global nodos
    id_n_dim = id_node()
    n1 = Nodo(Dato(id_n_dim, "SIZE_N_DIMENSIONES"))
    nodos[-2].dato.id_nodo_padre = id_n_dim
    n1.insertar_hijo(nodos.pop(-2))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[2]), hoja_lexema=True, id_nodo_padre=id_n_dim))
    n1.insertar_hijo(n2)
    nodos[-1].dato.id_nodo_padre = id_n_dim
    n1.insertar_hijo(nodos.pop())
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[4]), hoja_lexema=True, id_nodo_padre=id_n_dim))
    n1.insertar_hijo(n3)
    # NODOS REDUCIDOS EN 'SIZE_N_DIMENSIONES'
    nodos.append(n1)

def p_size_n_dimensiones(p):
    'SIZE_N_DIMENSIONES : corchA EXPRESION corchC'
    p[0] = [p[2]]

    global nodos
    id_n_dim = id_node()
    n1 = Nodo(Dato(id_n_dim, "SIZE_N_DIMENSIONES"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_n_dim))
    n1.insertar_hijo(n2)
    nodos[-1].dato.id_nodo_padre = id_n_dim
    n1.insertar_hijo(nodos.pop())
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[3]), hoja_lexema=True, id_nodo_padre=id_n_dim))
    n1.insertar_hijo(n3)
    # NODOS REDUCIDOS EN 'SIZE_N_DIMENSIONES'
    nodos.append(n1)

# TODO Errores sintácticos Declaracion_Array2
def p_array_tipo2(p):
    'ARRAY_TIPO2 : TIPO N_DIMENSIONES id dosP ARREGLO pyc'
    p[0] = Declaracion_Array2(p.lineno(3), find_column(input_string, p.slice[3]), p[1], p[2], p[3], p[5])

    global nodos
    id_arr2 = id_node()
    n1 = Nodo(Dato(id_arr2, "ARRAY_TIPO2"))
    nodos[-3].dato.id_nodo_padre = id_arr2
    n1.insertar_hijo(nodos.pop(-3))
    nodos[-2].dato.id_nodo_padre = id_arr2
    n1.insertar_hijo(nodos.pop(-2))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[3]), hoja_lexema=True, id_nodo_padre=id_arr2))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[4]), hoja_lexema=True, id_nodo_padre=id_arr2))
    n1.insertar_hijo(n3)
    nodos[-1].dato.id_nodo_padre = id_arr2
    n1.insertar_hijo(nodos.pop())
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[6]), hoja_lexema=True, id_nodo_padre=id_arr2))
    n1.insertar_hijo(n4)
    # NODOS REDUCIDOS EN 'ARRAY_TIPO2'
    nodos.append(n1)

def p_arreglo(p):
    '''ARREGLO : llaveA LISTA_VALORES llaveC
            | llaveA LISTA_ARR_RECURSIVO llaveC'''
    p[0] = p[2]

    global nodos
    id_arr = id_node()
    n1 = Nodo(Dato(id_arr, "ARREGLO"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_arr))
    n1.insertar_hijo(n2)
    nodos[-1].dato.id_nodo_padre = id_arr
    n1.insertar_hijo(nodos.pop())
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[3]), hoja_lexema=True, id_nodo_padre=id_arr))
    n1.insertar_hijo(n3)
    # NODOS REDUCIDOS EN 'ARREGLO'
    nodos.append(n1)

def p_lista_valores_recursivo(p):
    'LISTA_VALORES : LISTA_VALORES coma EXPRESION'
    p[1].append(p[3])
    p[0] = p[1]

    global nodos
    id_valores = id_node()
    n1 = Nodo(Dato(id_valores, "LISTA_VALORES"))
    nodos[-2].dato.id_nodo_padre = id_valores
    n1.insertar_hijo(nodos.pop(-2))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[2]), hoja_lexema=True, id_nodo_padre=id_valores))
    n1.insertar_hijo(n2)
    nodos[-1].dato.id_nodo_padre = id_valores
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'LISTA_VALORES'
    nodos.append(n1)

def p_lista_valores(p):
    'LISTA_VALORES : EXPRESION'
    p[0] = [p[1]]

    global nodos
    id_arr_recursivo = id_node()
    n1 = Nodo(Dato(id_arr_recursivo, "LISTA_VALORES"))
    nodos[-1].dato.id_nodo_padre = id_arr_recursivo
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'LISTA_VALORES'
    nodos.append(n1)

def p_lista_arr_recursivo(p):
    'LISTA_ARR_RECURSIVO : LISTA_ARR_RECURSIVO coma ARREGLO'
    p[1].append(p[3])
    p[0] = p[1]

    global nodos
    id_arr_recursivo = id_node()
    n1 = Nodo(Dato(id_arr_recursivo, "LISTA_ARR_RECURSIVO"))
    nodos[-2].dato.id_nodo_padre = id_arr_recursivo
    n1.insertar_hijo(nodos.pop(-2))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[2]), hoja_lexema=True, id_nodo_padre=id_arr_recursivo))
    n1.insertar_hijo(n2)
    nodos[-1].dato.id_nodo_padre = id_arr_recursivo
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'LISTA_ARR_RECURSIVO'
    nodos.append(n1)

def p_lista_arr_arr(p):
    'LISTA_ARR_RECURSIVO :  ARREGLO'
    p[0] = [p[1]]

    global nodos
    id_arr_recursivo = id_node()
    n1 = Nodo(Dato(id_arr_recursivo, "LISTA_ARR_RECURSIVO"))
    nodos[-1].dato.id_nodo_padre = id_arr_recursivo
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'LISTA_ARR_RECURSIVO'
    nodos.append(n1)

def p_asignacion_arreglos(p):
    'ASIGNACION : id SIZE_N_DIMENSIONES dosP EXPRESION pyc'
    p[0] = Asignacion_Arr(p.lineno(1), find_column(input_string, p.slice[1]), p[1], p[2], p[4])

    global nodos
    id_asig = id_node()
    n1 = Nodo(Dato(id_asig, "ASIGNACION"))
    id_id = id_node()
    n2 = Nodo(Dato(id_id, str(p[1]), hoja_lexema=True, id_nodo_padre=id_asig))
    n1.insertar_hijo(n2)
    nodos[-2].dato.id_nodo_padre = id_asig
    n1.insertar_hijo(nodos.pop(-2))
    id_dosP = id_node()
    n3 = Nodo(Dato(id_dosP, str(p[3]), hoja_lexema=True, id_nodo_padre=id_asig))
    n1.insertar_hijo(n3)
    nodos[-1].dato.id_nodo_padre = id_asig
    n1.insertar_hijo(nodos.pop())
    id_pyc = id_node()
    n4 = Nodo(Dato(id_pyc, str(p[5]), hoja_lexema=True, id_nodo_padre=id_asig))
    n1.insertar_hijo(n4)
    # NODOS REDUCIDOS EN 'ASIGNACION'
    nodos.append(n1)

def p_asignacion2(p):
    'ASIGNACION : id dosP EXPRESION pyc'
    p[0] = Asignacion(p.lineno(1), find_column(input_string, p.slice[1]), p[1], p[3])

    global nodos
    id_asig = id_node()
    n1 = Nodo(Dato(id_asig, "ASIGNACION"))
    id_id = id_node()
    n2 = Nodo(Dato(id_id, str(p[1]), hoja_lexema=True, id_nodo_padre=id_asig))
    n1.insertar_hijo(n2)
    id_dosP = id_node()
    n3 = Nodo(Dato(id_dosP, str(p[2]), hoja_lexema=True, id_nodo_padre=id_asig))
    n1.insertar_hijo(n3)
    nodos[-1].dato.id_nodo_padre = id_asig
    n1.insertar_hijo(nodos.pop())
    id_pyc = id_node()
    n4 = Nodo(Dato(id_pyc, str(p[4]), hoja_lexema=True, id_nodo_padre=id_asig))
    n1.insertar_hijo(n4)
    # NODOS REDUCIDOS EN 'ASIGNACION'
    nodos.append(n1)

def p_asignacion2_error1(p):
    '''ASIGNACION : id dosP EXPRESION error pyc'''
    syntax_error = Error("Sintáctico", "Error sintáctico al realizar asignación de valor a una variable.", p.lineno(1), find_column(input_string, p.slice[1]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "ASIGNACION"))
    nodos.pop()
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'ASIGNACION'
    nodos.append(n1)

def p_asignacion2_error2(p):
    '''ASIGNACION : id dosP error pyc
                | id error pyc'''
    syntax_error = Error("Sintáctico", "Error sintáctico al hacer uso de nombre de variable.", p.lineno(1), find_column(input_string, p.slice[1]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "ASIGNACION"))
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'ASIGNACION'
    nodos.append(n1)

def p_nativas(p):
    '''NATIVAS : IMPRIMIR
                | TABLA_SIMBOLOS
                | INSTR_MINUS
                | INSTR_MAYUS
                | INSTR_TRUNCATE
                | INSTR_ROUND
                | INSTR_TAMANO'''
    p[0] = p[1]

    global nodos
    id_nat = id_node()
    n1 = Nodo(Dato(id_nat, "NATIVAS"))
    nodos[-1].dato.id_nodo_padre = id_nat
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'NATIVAS'
    nodos.append(n1)

def p_imprimir(p):
    '''IMPRIMIR : imprimir parA EXPRESION parC pyc'''
    global txtbox_console
    p[0] = Imprimir(p.lineno(1), find_column(input_string, p.slice[1]), p[3], txtbox_console)

    global nodos
    id_imp = id_node()
    n1 = Nodo(Dato(id_imp, "IMPRIMIR"))
    id_pr_imp = id_node()
    n2 = Nodo(Dato(id_pr_imp, str(p[1]), hoja_lexema=True, id_nodo_padre=id_imp))
    n1.insertar_hijo(n2)
    id_parA = id_node()
    n3 = Nodo(Dato(id_parA, str(p[2]), hoja_lexema=True, id_nodo_padre=id_imp))
    n1.insertar_hijo(n3)
    nodos[-1].dato.id_nodo_padre = id_imp
    n1.insertar_hijo(nodos.pop())
    id_parC = id_node()
    n4 = Nodo(Dato(id_parC, str(p[4]), hoja_lexema=True, id_nodo_padre=id_imp))
    n1.insertar_hijo(n4)
    id_pyc = id_node()
    n5 = Nodo(Dato(id_pyc, str(p[5]), hoja_lexema=True, id_nodo_padre=id_imp))
    n1.insertar_hijo(n5)
    # NODOS REDUCIDOS EN 'IMPRIMIR'
    nodos.append(n1)

def p_imprimir_error_recuperado(p):
    '''IMPRIMIR : imprimir parA EXPRESION error pyc'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado en el llamado a la función imprimir. Se recuperó del error.", p.lineno(1), find_column(input_string, p.slice[1]))
    errores_sintacticos.append(syntax_error)
    global txtbox_console
    p[0] = Imprimir(p.lineno(1), find_column(input_string, p.slice[1]), p[3], txtbox_console)

    global nodos
    id_imp = id_node()
    n1 = Nodo(Dato(id_imp, "IMPRIMIR"))
    id_pr_imp = id_node()
    n2 = Nodo(Dato(id_pr_imp, str(p[1]), hoja_lexema=True, id_nodo_padre=id_imp))
    n1.insertar_hijo(n2)
    id_parA = id_node()
    n3 = Nodo(Dato(id_parA, str(p[2]), hoja_lexema=True, id_nodo_padre=id_imp))
    n1.insertar_hijo(n3)
    nodos[-1].dato.id_nodo_padre = id_imp
    n1.insertar_hijo(nodos.pop())
    id_syntax_error = id_node()
    n4 = Nodo(Dato(id_syntax_error, "Syntax Error!", hoja_error=True, id_nodo_padre=id_imp))
    n1.insertar_hijo(n4)
    id_pyc = id_node()
    n5 = Nodo(Dato(id_pyc, str(p[5]), hoja_lexema=True, id_nodo_padre=id_imp))
    n1.insertar_hijo(n5)
    # NODOS REDUCIDOS EN 'IMPRIMIR'
    nodos.append(n1)

def p_imprimir_error(p):
    '''IMPRIMIR : imprimir parA error pyc
                | imprimir error pyc'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado en el llamado a la función imprimir.", p.lineno(1), find_column(input_string, p.slice[1]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "IMPRIMIR"))
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'IMPRIMIR'
    nodos.append(n1)

def p_tabla_simbolos(p):
    '''TABLA_SIMBOLOS : tablasimbolos parA EXPRESION parC pyc'''
    p[0] = Tabla_Simbolos(p.lineno(1), find_column(input_string, p.slice[1]), p[3])
    
    global nodos
    id_tabla = id_node()
    n1 = Nodo(Dato(id_tabla, "TABLA_SIMBOLOS"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_tabla))
    n1.insertar_hijo(n2)
    id_parA = id_node()
    n3 = Nodo(Dato(id_parA, str(p[2]), hoja_lexema=True, id_nodo_padre=id_tabla))
    n1.insertar_hijo(n3)
    nodos[-1].dato.id_nodo_padre = id_tabla
    n1.insertar_hijo(nodos.pop())
    id_parC = id_node()
    n4 = Nodo(Dato(id_parC, str(p[4]), hoja_lexema=True, id_nodo_padre=id_tabla))
    n1.insertar_hijo(n4)
    id_pyc = id_node()
    n5 = Nodo(Dato(id_pyc, str(p[5]), hoja_lexema=True, id_nodo_padre=id_tabla))
    n1.insertar_hijo(n5)
    # NODOS REDUCIDOS EN 'TABLA_SIMBOLOS'
    nodos.append(n1)

def p_tabla_simbolos_error1(p):
    '''TABLA_SIMBOLOS : tablasimbolos parA EXPRESION error pyc'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado en el llamado a la función tablaSimbolos.", p.lineno(1), find_column(input_string, p.slice[1]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "TABLA_SIMBOLOS"))
    nodos.pop()
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'TABLA_SIMBOLOS'
    nodos.append(n1)

def p_tabla_simbolos_error2(p):
    '''TABLA_SIMBOLOS : tablasimbolos parA error pyc
                    | tablasimbolos error pyc'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado en el llamado a la función tablaSimbolos.", p.lineno(1), find_column(input_string, p.slice[1]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "TABLA_SIMBOLOS"))
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'TABLA_SIMBOLOS'
    nodos.append(n1)

# TODO Errores sintácticos instrucciones nativas
def p_instr_minus(p):
    'INSTR_MINUS : aminus parA EXPRESION parC pyc'
    p[0] = AMinus(p.lineno(1), find_column(input_string, p.slice[1]), p[3])

    global nodos
    id_instr = id_node()
    n1 = Nodo(Dato(id_instr, "INSTR_MINUS"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n3)
    nodos[-1].dato.id_nodo_padre = id_instr
    n1.insertar_hijo(nodos.pop())
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[4]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n4)
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[5]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n5)
    # NODOS REDUCIDOS EN 'INSTR_MINUS'
    nodos.append(n1)

def p_instr_mayus(p):
    'INSTR_MAYUS : amayus parA EXPRESION parC pyc'
    p[0] = AMayus(p.lineno(1), find_column(input_string, p.slice[1]), p[3])

    global nodos
    id_instr = id_node()
    n1 = Nodo(Dato(id_instr, "INSTR_MAYUS"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n3)
    nodos[-1].dato.id_nodo_padre = id_instr
    n1.insertar_hijo(nodos.pop())
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[4]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n4)
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[5]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n5)
    # NODOS REDUCIDOS EN 'INSTR_MAYUS'
    nodos.append(n1)

def p_instr_truncate(p):
    'INSTR_TRUNCATE : truncate parA EXPRESION parC pyc'
    p[0] = Truncate(p.lineno(1), find_column(input_string, p.slice[1]), p[3])

    global nodos
    id_instr = id_node()
    n1 = Nodo(Dato(id_instr, "INSTR_TRUNCATE"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n3)
    nodos[-1].dato.id_nodo_padre = id_instr
    n1.insertar_hijo(nodos.pop())
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[4]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n4)
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[5]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n5)
    # NODOS REDUCIDOS EN 'INSTR_TRUNCATE'
    nodos.append(n1)

def p_instr_round(p):
    'INSTR_ROUND : round parA EXPRESION parC pyc'
    p[0] = Round(p.lineno(1), find_column(input_string, p.slice[1]), p[3])

    global nodos
    id_instr = id_node()
    n1 = Nodo(Dato(id_instr, "INSTR_ROUND"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n3)
    nodos[-1].dato.id_nodo_padre = id_instr
    n1.insertar_hijo(nodos.pop())
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[4]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n4)
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[5]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n5)
    # NODOS REDUCIDOS EN 'INSTR_ROUND'
    nodos.append(n1)

def p_instr_tamano(p):
    'INSTR_TAMANO : tamano parA EXPRESION parC pyc'
    p[0] = Tamano(p.lineno(1), find_column(input_string, p.slice[1]), p[3])

    global nodos
    id_instr = id_node()
    n1 = Nodo(Dato(id_instr, "INSTR_TAMANO"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n3)
    nodos[-1].dato.id_nodo_padre = id_instr
    n1.insertar_hijo(nodos.pop())
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[4]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n4)
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[5]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n5)
    # NODOS REDUCIDOS EN 'INSTR_TAMANO'
    nodos.append(n1)

def p_inc_dec(p):
    '''INC_DEC : id incremento pyc
                | id decremento pyc'''
    p[0] = Inc_Dec(p.lineno(1), find_column(input_string, p.slice[1]), p.slice[1].value, p.slice[2].value)

    global nodos
    id_exp = id_node()
    n1 = Nodo(Dato(id_exp, "INC_DEC"))
    id_id = id_node()
    n2 = Nodo(Dato(id_id, str(p[1]), hoja_lexema=True, id_nodo_padre=id_exp))
    n1.insertar_hijo(n2)
    id_inc_dec = id_node()
    n3 = Nodo(Dato(id_inc_dec, str(p[2]), hoja_lexema=True, id_nodo_padre=id_exp))
    n1.insertar_hijo(n3)
    id_pyc = id_node()
    n4 = Nodo(Dato(id_pyc, str(p[3]), hoja_lexema=True, id_nodo_padre=id_exp))
    n1.insertar_hijo(n4)
    # NODOS REDUCIDOS EN 'EXPRESION'
    nodos.append(n1)

def p_ciclo_while(p):
    'WHILE : while dosP llaveA CONDICION coma INSTR llaveC'
    if not isinstance(p[4], Error) and not isinstance(p[6], Error):
        p[0] = While(p.lineno(1), find_column(input_string, p.slice[1]), p[4], p[6])
    elif isinstance(p[4], Error) and isinstance(p[6], Error):
        syntax_error = Error("Sintáctico", "Error sintáctico detectado en condición y bloque de instrucciones del while, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)
    elif isinstance(p[4], Error):
        syntax_error = Error("Sintáctico", "Error sintáctico detectado en condición del while, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)
    else:
        syntax_error = Error("Sintáctico", "Error sintáctico detectado en bloque de instrucciones del while, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)
    
    global nodos
    id_while = id_node()
    n1 = Nodo(Dato(id_while, "WHILE"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_while))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_while))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_while))
    n1.insertar_hijo(n4)
    nodos[-2].dato.id_nodo_padre = id_while
    n1.insertar_hijo(nodos.pop(-2))
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[5]), hoja_lexema=True, id_nodo_padre=id_while))
    n1.insertar_hijo(n5)
    nodos[-1].dato.id_nodo_padre = id_while
    n1.insertar_hijo(nodos.pop())
    id5 = id_node()
    n6 = Nodo(Dato(id5, str(p[7]), hoja_lexema=True, id_nodo_padre=id_while))
    n1.insertar_hijo(n6)
    # NODOS REDUCIDOS EN 'WHILE'
    nodos.append(n1)

def p_ciclo_while_error1(p):
    '''WHILE : while dosP llaveA CONDICION coma INSTR error llaveC'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado luego del bloque INSTR del while.", p.lineno(1), find_column(input_string, p.slice[1]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "WHILE"))
    nodos.pop()
    nodos.pop()
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'WHILE'
    nodos.append(n1)

def p_ciclo_while_error2(p):
    '''WHILE : while dosP llaveA CONDICION coma error llaveC
            | while dosP llaveA CONDICION error llaveC'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado luego del bloque CONDICION del while.", p.lineno(1), find_column(input_string, p.slice[1]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "WHILE"))
    nodos.pop()
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'WHILE'
    nodos.append(n1)

def p_ciclo_while_error3(p):
    '''WHILE : while dosP llaveA error llaveC
            | while error llaveC'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado luego de la declaración while.", p.lineno(1), find_column(input_string, p.slice[1]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "WHILE"))
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'WHILE'
    nodos.append(n1)

def p_condicion(p):
    'CONDICION : cond dosP parA EXPRESION parC'
    p[0] = p[4]

    global nodos
    id_condicion = id_node()
    n1 = Nodo(Dato(id_condicion, "CONDICION"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_condicion))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_condicion))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_condicion))
    n1.insertar_hijo(n4)
    nodos[-1].dato.id_nodo_padre = id_condicion
    n1.insertar_hijo(nodos.pop())
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[5]), hoja_lexema=True, id_nodo_padre=id_condicion))
    n1.insertar_hijo(n5)
    # NODOS REDUCIDOS EN 'CONDICION'
    nodos.append(n1)

def p_condicion_error1(p):
    '''CONDICION : cond dosP parA EXPRESION error parC'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado en la declaración de una condición.", p.lineno(1), find_column(input_string, p.slice[1]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "CONDICION"))
    nodos.pop()
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'CONDICION'
    nodos.append(n1)

def p_condicion_error2(p):
    '''CONDICION : cond dosP parA error parC
                | cond dosP error parC
                | cond error parC'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado en la declaración de una condición.", p.lineno(1), find_column(input_string, p.slice[1]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "CONDICION"))
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'CONDICION'
    nodos.append(n1)

def p_instr(p):
    'INSTR : instr dosP llaveA INSTRUCCIONES llaveC'
    p[0] = p[4]
    
    global nodos
    id_instr = id_node()
    n1 = Nodo(Dato(id_instr, "INSTR"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n4)
    nodos[-1].dato.id_nodo_padre = id_instr
    n1.insertar_hijo(nodos.pop())
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[5]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n5)
    # NODOS REDUCIDOS EN 'INSTR'
    nodos.append(n1)

def p_instr_error_recuperado(p):
    'INSTR : instr dosP llaveA INSTRUCCIONES error llaveC'
    p[0] = p[4]
    syntax_error = Error("Sintáctico", "Error sintáctico detectado dentro de un entorno de instrucciones. Se ejecutaron las encontradas antes del error.", p.lineno(3), find_column(input_string, p.slice[3]))
    errores_sintacticos.append(syntax_error)
    
    global nodos
    id_instr = id_node()
    n1 = Nodo(Dato(id_instr, "INSTR"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n4)
    nodos[-1].dato.id_nodo_padre = id_instr
    n1.insertar_hijo(nodos.pop())
    id_error = id_node()
    n5 = Nodo(Dato(id_error, "Syntax Error!", id_nodo_padre=id_instr, hoja_error=True))
    n1.insertar_hijo(n5)
    id4 = id_node()
    n6 = Nodo(Dato(id4, str(p[6]), hoja_lexema=True, id_nodo_padre=id_instr))
    n1.insertar_hijo(n6)
    # NODOS REDUCIDOS EN 'INSTR'
    nodos.append(n1)

def p_instr_error(p):
    '''INSTR : instr dosP llaveA error llaveC
            | instr dosP error llaveC
            | instr error llaveC'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado en la declaración de un entorno de instrucciones. Estas no pudieron guardarse.", p.lineno(3), find_column(input_string, p.slice[3]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "INSTR"))
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'INSTR'
    nodos.append(n1)

def p_do_while(p):
    'DO_WHILE : do dosP llaveA INSTR coma while dosP llaveA CONDICION llaveC llaveC'
    if not isinstance(p[4], Error) and not isinstance(p[9], Error):
        p[0] = Do_While(p.lineno(1), find_column(input_string, p.slice[1]), p[4], p[9])
    elif isinstance(p[4], Error) and isinstance(p[9], Error):
        syntax_error = Error("Sintáctico", "Error sintáctico detectado en bloque de instrucciones y condicón del do-while, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)
    elif isinstance(p[4], Error):
        syntax_error = Error("Sintáctico", "Error sintáctico detectado en bloque de instrucciones del do-while, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)
    else:
        syntax_error = Error("Sintáctico", "Error sintáctico detectado en condicón del do-while, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)

    global nodos
    id_do_while = id_node()
    n1 = Nodo(Dato(id_do_while, "DO_WHILE"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_do_while))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_do_while))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_do_while))
    n1.insertar_hijo(n4)
    nodos[-2].dato.id_nodo_padre = id_do_while
    n1.insertar_hijo(nodos.pop(-2))
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[5]), hoja_lexema=True, id_nodo_padre=id_do_while))
    n1.insertar_hijo(n5)
    id5 = id_node()
    n6 = Nodo(Dato(id5, str(p[6]), hoja_lexema=True, id_nodo_padre=id_do_while))
    n1.insertar_hijo(n6)
    id6 = id_node()
    n7 = Nodo(Dato(id6, str(p[7]), hoja_lexema=True, id_nodo_padre=id_do_while))
    n1.insertar_hijo(n7)
    id7 = id_node()
    n8 = Nodo(Dato(id7, str(p[8]), hoja_lexema=True, id_nodo_padre=id_do_while))
    n1.insertar_hijo(n8)
    nodos[-1].dato.id_nodo_padre = id_do_while
    n1.insertar_hijo(nodos.pop())
    id8 = id_node()
    n9 = Nodo(Dato(id8, str(p[10]), hoja_lexema=True, id_nodo_padre=id_do_while))
    n1.insertar_hijo(n9)
    id9 = id_node()
    n10 = Nodo(Dato(id9, str(p[11]), hoja_lexema=True, id_nodo_padre=id_do_while))
    n1.insertar_hijo(n10)
    # NODOS REDUCIDOS EN 'DO_WHILE'
    nodos.append(n1)

def p_do_while_error1(p):
    '''DO_WHILE : do dosP llaveA INSTR coma while dosP llaveA CONDICION llaveC error llaveC
                | do dosP llaveA INSTR coma while dosP llaveA CONDICION error llaveC'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado luego del bloque CONDICION del do-while.", p.lineno(1), find_column(input_string, p.slice[1]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "DO_WHILE"))
    nodos.pop()
    nodos.pop()
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'DO_WHILE'
    nodos.append(n1)

def p_do_while_error2(p):
    '''DO_WHILE : do dosP llaveA INSTR coma while dosP llaveA error llaveC
                | do dosP llaveA INSTR coma while dosP error llaveC
                | do dosP llaveA INSTR coma while error llaveC
                | do dosP llaveA INSTR coma error llaveC
                | do dosP llaveA INSTR error llaveC'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado luego del bloque INSTR del do-while.", p.lineno(1), find_column(input_string, p.slice[1]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "DO_WHILE"))
    nodos.pop()
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'DO_WHILE'
    nodos.append(n1)

def p_do_while_error3(p):
    '''DO_WHILE : do dosP llaveA error llaveC
                | do dosP error llaveC
                | do error llaveC'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado al inicio de la declaración del do-while.", p.lineno(1), find_column(input_string, p.slice[1]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "DO_WHILE"))
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'DO_WHILE'
    nodos.append(n1)

def p_ciclo_for(p):
    'FOR : for dosP llaveA CONDICION_FOR coma INSTR llaveC'
    if not isinstance(p[4], Error) and not isinstance(p[6], Error):
        p[0] = For(p.lineno(1), find_column(input_string, p.slice[1]), p[4][0], p[4][1], p[4][2], p[6])
    elif isinstance(p[4], Error) and isinstance(p[6], Error):
        syntax_error = Error("Sintáctico", "Error sintáctico detectado en los bloques de condición e instrucciones del for, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)
    elif isinstance(p[4], Error):
        syntax_error = Error("Sintáctico", "Error sintáctico detectado en el bloque de condición del for, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)
    else:
        syntax_error = Error("Sintáctico", "Error sintáctico detectado en el bloque de instrucciones del for, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)

    global nodos
    id_for = id_node()
    n1 = Nodo(Dato(id_for, "FOR"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_for))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_for))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_for))
    n1.insertar_hijo(n4)
    nodos[-2].dato.id_nodo_padre = id_for
    n1.insertar_hijo(nodos.pop(-2))
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[5]), hoja_lexema=True, id_nodo_padre=id_for))
    n1.insertar_hijo(n5)
    nodos[-1].dato.id_nodo_padre = id_for
    n1.insertar_hijo(nodos.pop())
    id5 = id_node()
    n6 = Nodo(Dato(id5, str(p[7]), hoja_lexema=True, id_nodo_padre=id_for))
    n1.insertar_hijo(n6)
    # NODOS REDUCIDOS EN 'FOR'
    nodos.append(n1)

def p_condicion_for(p):
    'CONDICION_FOR : cond dosP parA INICIALIZACION pyc EXPRESION pyc EXP_INC_DEC parC'
    p[0] = [p[4], p[6], p[8]]

    global nodos
    id_cond_for = id_node()
    n1 = Nodo(Dato(id_cond_for, "CONDICION_FOR"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_cond_for))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[3]), hoja_lexema=True, id_nodo_padre=id_cond_for))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[5]), hoja_lexema=True, id_nodo_padre=id_cond_for))
    n1.insertar_hijo(n4)
    nodos[-3].dato.id_nodo_padre = id_cond_for
    n1.insertar_hijo(nodos.pop(-3))
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[5]), hoja_lexema=True, id_nodo_padre=id_cond_for))
    n1.insertar_hijo(n5)
    nodos[-2].dato.id_nodo_padre = id_cond_for
    n1.insertar_hijo(nodos.pop(-2))
    id5 = id_node()
    n6 = Nodo(Dato(id5, str(p[5]), hoja_lexema=True, id_nodo_padre=id_cond_for))
    n1.insertar_hijo(n6)
    nodos[-1].dato.id_nodo_padre = id_cond_for
    n1.insertar_hijo(nodos.pop())
    id6 = id_node()
    n7 = Nodo(Dato(id6, str(p[5]), hoja_lexema=True, id_nodo_padre=id_cond_for))
    n1.insertar_hijo(n7)
    # NODOS REDUCIDOS EN 'DECLARACION'
    nodos.append(n1)

def p_condicion_for_error_recuperado(p):
    '''CONDICION_FOR : cond dosP parA INICIALIZACION pyc EXPRESION pyc EXP_INC_DEC error parC'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado al final de la condición del for, se logró recuperar del error.", p.lineno(7), find_column(input_string, p.slice[7]))
    errores_sintacticos.append(syntax_error)
    p[0] = [p[4], p[6], p[8]]

    global nodos
    id_cond_for = id_node()
    n1 = Nodo(Dato(id_cond_for, "CONDICION_FOR"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_cond_for))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_cond_for))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_cond_for))
    n1.insertar_hijo(n4)
    nodos[-3].dato.id_nodo_padre = id_cond_for
    n1.insertar_hijo(nodos.pop(-3))
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[5]), hoja_lexema=True, id_nodo_padre=id_cond_for))
    n1.insertar_hijo(n5)
    nodos[-2].dato.id_nodo_padre = id_cond_for
    n1.insertar_hijo(nodos.pop(-2))
    id5 = id_node()
    n6 = Nodo(Dato(id5, str(p[7]), hoja_lexema=True, id_nodo_padre=id_cond_for))
    n1.insertar_hijo(n6)
    nodos[-1].dato.id_nodo_padre = id_cond_for
    n1.insertar_hijo(nodos.pop())
    id_error = id_node()
    n2 = Nodo(Dato(id_error, "Syntax Error!", id_nodo_padre=id_cond_for, hoja_error=True))
    n1.insertar_hijo(n2)
    id6 = id_node()
    n7 = Nodo(Dato(id6, str(p[10]), hoja_lexema=True, id_nodo_padre=id_cond_for))
    n1.insertar_hijo(n7)
    # NODOS REDUCIDOS EN 'DECLARACION'
    nodos.append(n1)

def p_condicion_for_error1(p):
    '''CONDICION_FOR : cond dosP parA INICIALIZACION pyc EXPRESION pyc error parC
                    | cond dosP parA INICIALIZACION pyc EXPRESION error parC'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado luego de declarada la condición del for.", p.lineno(1), find_column(input_string, p.slice[1]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "CONDICION_FOR"))
    nodos.pop()
    nodos.pop()
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'CONDICION_FOR'
    nodos.append(n1)

def p_condicion_for_error2(p):
    '''CONDICION_FOR : cond dosP parA INICIALIZACION pyc error parC
                    | cond dosP parA INICIALIZACION error parC'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado luego de declarada la variable de inicio del for.", p.lineno(1), find_column(input_string, p.slice[1]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "CONDICION_FOR"))
    nodos.pop()
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'CONDICION_FOR'
    nodos.append(n1)

def p_condicion_for_error3(p):
    '''CONDICION_FOR : cond dosP parA error parC
                    | cond dosP error parC
                    | cond error parC'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado al inicio del bloque CONDICION_FOR del for.", p.lineno(1), find_column(input_string, p.slice[1]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "CONDICION_FOR"))
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'CONDICION_FOR'
    nodos.append(n1)

def p_inicializacion_for_declaracion(p):
    'INICIALIZACION : TIPO id dosP EXPRESION'
    p[0] = Declaracion(p.lineno(2), find_column(input_string, p.slice[2]), p[2], p[1], p[4])

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "INICIALIZACION"))
    nodos[-2].dato.id_nodo_padre = id_decl
    n1.insertar_hijo(nodos.pop(-2))
    id_id = id_node()
    n2 = Nodo(Dato(id_id, str(p[2]), hoja_lexema=True, id_nodo_padre=id_decl))
    n1.insertar_hijo(n2)
    id_dosP = id_node()
    n3 = Nodo(Dato(id_dosP, str(p[3]), hoja_lexema=True, id_nodo_padre=id_decl))
    n1.insertar_hijo(n3)
    nodos[-1].dato.id_nodo_padre = id_decl
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'INICIALIZACION'
    nodos.append(n1)

def p_inicializacion_for_declaracion_nula(p):
    'INICIALIZACION : TIPO id'
    prm_null = Primitivo(p.lineno(2), find_column(input_string, p.slice[2]), 'null', None)
    p[0] = Declaracion(p.lineno(2), find_column(input_string, p.slice[2]), p[2], p[1], prm_null)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "INICIALIZACION"))
    nodos[-1].dato.id_nodo_padre = id_decl
    n1.insertar_hijo(nodos.pop())
    id_id = id_node()
    n2 = Nodo(Dato(id_id, str(p[2]), hoja_lexema=True, id_nodo_padre=id_decl))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'INICIALIZACION'
    nodos.append(n1)

def p_inicializacion_for_asignacion(p):
    'INICIALIZACION : id dosP EXPRESION'
    p[0] = Asignacion(p.lineno(1), find_column(input_string, p.slice[1]), p[1], p[3])

    global nodos
    id_asig = id_node()
    n1 = Nodo(Dato(id_asig, "INICIALIZACION"))
    id_id = id_node()
    n2 = Nodo(Dato(id_id, str(p[1]), hoja_lexema=True, id_nodo_padre=id_asig))
    n1.insertar_hijo(n2)
    id_dosP = id_node()
    n3 = Nodo(Dato(id_dosP, str(p[2]), hoja_lexema=True, id_nodo_padre=id_asig))
    n1.insertar_hijo(n3)
    nodos[-1].dato.id_nodo_padre = id_asig
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'INICIALIZACION'
    nodos.append(n1)

def p_if_elseif_else(p):
    'IF : if dosP llaveA CONDICION coma INSTR llaveC ELSE_IF_RECURSIVO else dosP llaveA INSTR llaveC'
    elseif_error = False
    for elseif in p[8]:
        if isinstance(elseif, Error):
            elseif_error = True
            break
        
    if not isinstance(p[4], Error) and not isinstance(p[6], Error) and not elseif_error and not isinstance(p[12], Error):
        p[0] = If(p.lineno(1), find_column(input_string, p.slice[1]), p[4], p[6], lista_else_if=p[8], lista_instr_else=p[12])
    else:
        syntax_error = Error("Sintáctico", "Error sintáctico detectado dentro del if-elseif-else, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)

    global nodos
    id_if = id_node()
    n1 = Nodo(Dato(id_if, "IF"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n4)
    nodos[-4].dato.id_nodo_padre = id_if
    n1.insertar_hijo(nodos.pop(-4))
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[5]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n5)
    nodos[-3].dato.id_nodo_padre = id_if
    n1.insertar_hijo(nodos.pop(-3))
    id5 = id_node()
    n6 = Nodo(Dato(id5, str(p[7]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n6)
    nodos[-2].dato.id_nodo_padre = id_if
    n1.insertar_hijo(nodos.pop(-2))
    id6 = id_node()
    n7 = Nodo(Dato(id6, str(p[9]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n7)
    id7 = id_node()
    n8 = Nodo(Dato(id7, str(p[10]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n8)
    id8 = id_node()
    n9 = Nodo(Dato(id8, str(p[11]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n9)
    nodos[-1].dato.id_nodo_padre = id_if
    n1.insertar_hijo(nodos.pop())
    id9 = id_node()
    n10 = Nodo(Dato(id9, str(p[13]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n10)
    # NODOS REDUCIDOS EN 'IF'
    nodos.append(n1)

def p_if_elseif_else_error_recuperado(p):
    'IF : if dosP llaveA CONDICION coma INSTR llaveC ELSE_IF_RECURSIVO else dosP llaveA INSTR error llaveC'
    elseif_error = False
    for elseif in p[8]:
        if isinstance(elseif, Error):
            elseif_error = True
            break
        
    if not isinstance(p[4], Error) and not isinstance(p[6], Error) and not elseif_error and not isinstance(p[12], Error):
        p[0] = If(p.lineno(1), find_column(input_string, p.slice[1]), p[4], p[6], lista_else_if=p[8], lista_instr_else=p[12])
    else:
        syntax_error = Error("Sintáctico", "Error sintáctico detectado dentro del if-elseif-else, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)
    syntax_error = Error("Sintáctico", "Error sintáctico detectado al final del bloque INSTR del ELSE, se logró recuperar del error.", p.lineno(1), find_column(input_string, p.slice[1]))
    errores_sintacticos.append(syntax_error)

    global nodos
    id_if = id_node()
    n1 = Nodo(Dato(id_if, "IF"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n4)
    nodos[-4].dato.id_nodo_padre = id_if
    n1.insertar_hijo(nodos.pop(-4))
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[5]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n5)
    nodos[-3].dato.id_nodo_padre = id_if
    n1.insertar_hijo(nodos.pop(-3))
    id5 = id_node()
    n6 = Nodo(Dato(id5, str(p[7]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n6)
    nodos[-2].dato.id_nodo_padre = id_if
    n1.insertar_hijo(nodos.pop(-2))
    id6 = id_node()
    n7 = Nodo(Dato(id6, str(p[9]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n7)
    id7 = id_node()
    n8 = Nodo(Dato(id7, str(p[10]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n8)
    id8 = id_node()
    n9 = Nodo(Dato(id8, str(p[11]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n9)
    nodos[-1].dato.id_nodo_padre = id_if
    n1.insertar_hijo(nodos.pop())
    id_error = id_node()
    n_error = Nodo(Dato(id_error, "Syntax Error!", id_nodo_padre=id_if, hoja_error=True))
    n1.insertar_hijo(n_error)
    id9 = id_node()
    n10 = Nodo(Dato(id9, str(p[14]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n10)
    # NODOS REDUCIDOS EN 'IF'
    nodos.append(n1)

def p_if_elseif_else_error1(p):
    '''IF : if dosP llaveA CONDICION coma INSTR llaveC ELSE_IF_RECURSIVO else dosP llaveA error llaveC
            | if dosP llaveA CONDICION coma INSTR llaveC ELSE_IF_RECURSIVO else dosP error llaveC
            | if dosP llaveA CONDICION coma INSTR llaveC ELSE_IF_RECURSIVO else error llaveC'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado luego de declarado el ELSE del IF, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "IF"))
    nodos.pop()
    nodos.pop()
    nodos.pop()
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'IF'
    nodos.append(n1)

def p_if_elseif(p):
    'IF : if dosP llaveA CONDICION coma INSTR llaveC ELSE_IF_RECURSIVO'
    elseif_error = False
    for elseif in p[8]:
        if isinstance(elseif, Error):
            elseif_error = True
            break
        
    if not isinstance(p[4], Error) and not isinstance(p[6], Error) and not elseif_error:
        p[0] = If(p.lineno(1), find_column(input_string, p.slice[1]), p[4], p[6], lista_else_if=p[8])
    else:
        syntax_error = Error("Sintáctico", "Error sintáctico detectado dentro del if-elseif, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)

    global nodos
    id_if = id_node()
    n1 = Nodo(Dato(id_if, "IF"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n4)
    nodos[-3].dato.id_nodo_padre = id_if
    n1.insertar_hijo(nodos.pop(-3))
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[5]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n5)
    nodos[-2].dato.id_nodo_padre = id_if
    n1.insertar_hijo(nodos.pop(-2))
    id5 = id_node()
    n6 = Nodo(Dato(id5, str(p[7]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n6)
    nodos[-1].dato.id_nodo_padre = id_if
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'IF'
    nodos.append(n1)

def p_if_else(p):
    'IF : if dosP llaveA CONDICION coma INSTR llaveC else dosP llaveA INSTR llaveC'
    if not isinstance(p[4], Error) and not isinstance(p[6], Error) and not isinstance(p[11], Error):
        p[0] = If(p.lineno(1), find_column(input_string, p.slice[1]), p[4], p[6], lista_instr_else=p[11])
    else:
        syntax_error = Error("Sintáctico", "Error sintáctico detectado dentro del if-else, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)

    global nodos
    id_if = id_node()
    n1 = Nodo(Dato(id_if, "IF"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n4)
    nodos[-3].dato.id_nodo_padre = id_if
    n1.insertar_hijo(nodos.pop(-3))
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[5]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n5)
    nodos[-2].dato.id_nodo_padre = id_if
    n1.insertar_hijo(nodos.pop(-2))
    id5 = id_node()
    n6 = Nodo(Dato(id5, str(p[7]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n6)
    id6 = id_node()
    n7 = Nodo(Dato(id6, str(p[8]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n7)
    id7 = id_node()
    n8 = Nodo(Dato(id7, str(p[9]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n8)
    id8 = id_node()
    n9 = Nodo(Dato(id8, str(p[10]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n9)
    nodos[-1].dato.id_nodo_padre = id_if
    n1.insertar_hijo(nodos.pop())
    id9 = id_node()
    n10 = Nodo(Dato(id9, str(p[12]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n10)
    # NODOS REDUCIDOS EN 'IF'
    nodos.append(n1)

def p_if_else_error_recuperado(p):
    'IF : if dosP llaveA CONDICION coma INSTR llaveC else dosP llaveA INSTR error llaveC'
    if not isinstance(p[4], Error) and not isinstance(p[6], Error) and not isinstance(p[11], Error):
        p[0] = If(p.lineno(1), find_column(input_string, p.slice[1]), p[4], p[6], lista_instr_else=p[11])
    else:
        syntax_error = Error("Sintáctico", "Error sintáctico detectado dentro del if-else, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)
    syntax_error = Error("Sintáctico", "Error sintáctico detectado al final del bloque INSTR del ELSE, se logró recuperar del error.", p.lineno(1), find_column(input_string, p.slice[1]))
    errores_sintacticos.append(syntax_error)

    global nodos
    id_if = id_node()
    n1 = Nodo(Dato(id_if, "IF"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n4)
    nodos[-3].dato.id_nodo_padre = id_if
    n1.insertar_hijo(nodos.pop(-3))
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[5]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n5)
    nodos[-2].dato.id_nodo_padre = id_if
    n1.insertar_hijo(nodos.pop(-2))
    id5 = id_node()
    n6 = Nodo(Dato(id5, str(p[7]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n6)
    id6 = id_node()
    n7 = Nodo(Dato(id6, str(p[8]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n7)
    id7 = id_node()
    n8 = Nodo(Dato(id7, str(p[9]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n8)
    id8 = id_node()
    n9 = Nodo(Dato(id8, str(p[10]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n9)
    nodos[-1].dato.id_nodo_padre = id_if
    n1.insertar_hijo(nodos.pop())
    id_error = id_node()
    n_error = Nodo(Dato(id_error, "Syntax Error!", id_nodo_padre=id_if, hoja_error=True))
    n1.insertar_hijo(n_error)
    id9 = id_node()
    n10 = Nodo(Dato(id9, str(p[13]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n10)
    # NODOS REDUCIDOS EN 'IF'
    nodos.append(n1)

def p_if_else_error1(p):
    '''IF : if dosP llaveA CONDICION coma INSTR llaveC else dosP llaveA error llaveC
            | if dosP llaveA CONDICION coma INSTR llaveC else dosP error llaveC
            | if dosP llaveA CONDICION coma INSTR llaveC else error llaveC'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado luego de declarado el ELSE del IF, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "IF"))
    nodos.pop()
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'IF'
    nodos.append(n1)

def p_if(p):
    'IF : if dosP llaveA CONDICION coma INSTR llaveC'
    if not isinstance(p[4], Error) and not isinstance(p[6], Error):
        p[0] = If(p.lineno(1), find_column(input_string, p.slice[1]), p[4], p[6])
    elif isinstance(p[4], Error) and isinstance(p[6], Error):
        syntax_error = Error("Sintáctico", "Error sintáctico detectado en los bloques de condición e instrucciones del if, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)
    elif isinstance(p[4], Error):
        syntax_error = Error("Sintáctico", "Error sintáctico detectado en el bloque de condición del if, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)
    else:
        syntax_error = Error("Sintáctico", "Error sintáctico detectado en el bloque de instrucciones del if, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)

    global nodos
    id_if = id_node()
    n1 = Nodo(Dato(id_if, "IF"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n4)
    nodos[-2].dato.id_nodo_padre = id_if
    n1.insertar_hijo(nodos.pop(-2))
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[5]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n5)
    nodos[-1].dato.id_nodo_padre = id_if
    n1.insertar_hijo(nodos.pop())
    id5 = id_node()
    n6 = Nodo(Dato(id5, str(p[7]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n6)
    # NODOS REDUCIDOS EN 'IF'
    nodos.append(n1)

def p_if_error_recuperado(p):
    'IF : if dosP llaveA CONDICION coma INSTR error llaveC'
    if not isinstance(p[4], Error) and not isinstance(p[6], Error):
        p[0] = If(p.lineno(1), find_column(input_string, p.slice[1]), p[4], p[6])
    elif isinstance(p[4], Error) and isinstance(p[6], Error):
        syntax_error = Error("Sintáctico", "Error sintáctico detectado en los bloques de condición e instrucciones del if, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)
    elif isinstance(p[4], Error):
        syntax_error = Error("Sintáctico", "Error sintáctico detectado en el bloque de condición del if, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)
    else:
        syntax_error = Error("Sintáctico", "Error sintáctico detectado en el bloque de instrucciones del if, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)
    syntax_error = Error("Sintáctico", "Error sintáctico detectado al final del bloque INSTR del IF, se logró recuperar del error.", p.lineno(1), find_column(input_string, p.slice[1]))
    errores_sintacticos.append(syntax_error)

    # print("----ELEMENTO DEL IF RECUPERADO----")
    # print("Token que generó el error: " + str(p[7]))
    # print("Valor del token que generó el error: " + str(p[7].value))
    # print("Linea del token que generó el error: " + str(p[7].lineno))
    # print("Token de sincronización: " + str(p[8]))
    # print("------------------------------")

    global nodos
    id_if = id_node()
    n1 = Nodo(Dato(id_if, "IF"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n4)
    nodos[-2].dato.id_nodo_padre = id_if
    n1.insertar_hijo(nodos.pop(-2))
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[5]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n5)
    nodos[-1].dato.id_nodo_padre = id_if
    n1.insertar_hijo(nodos.pop())
    id_error = id_node()
    n_error = Nodo(Dato(id_error, "Syntax Error!", id_nodo_padre=id_if, hoja_error=True))
    n1.insertar_hijo(n_error)
    id5 = id_node()
    n6 = Nodo(Dato(id5, str(p[8]), hoja_lexema=True, id_nodo_padre=id_if))
    n1.insertar_hijo(n6)
    # NODOS REDUCIDOS EN 'IF'
    nodos.append(n1)

def p_if_error1(p):
    '''IF : if dosP llaveA CONDICION coma error llaveC
            | if dosP llaveA CONDICION error llaveC'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado luego de declarada la condición del IF, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "IF"))
    nodos.pop()
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'IF'
    nodos.append(n1)

def p_if_error2(p):
    '''IF : if dosP llaveA error llaveC
            | if dosP error llaveC
            | if error llaveC'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado al inicio de la declaración del IF, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "IF"))
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'IF'
    nodos.append(n1)

def p_elseif_anidados_recursivo(p):
    'ELSE_IF_RECURSIVO : ELSE_IF_RECURSIVO ELSE_IF'
    p[1].append(p[2])
    p[0] = p[1]

    global nodos
    id_else_if = id_node()
    n1 = Nodo(Dato(id_else_if, "ELSE_IF_RECURSIVO"))
    nodos[-2].dato.id_nodo_padre = id_else_if
    n1.insertar_hijo(nodos.pop(-2))
    nodos[-1].dato.id_nodo_padre = id_else_if
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'ELSE_IF_RECURSIVO'
    nodos.append(n1)

def p_else_if_anidados(p):
    'ELSE_IF_RECURSIVO : ELSE_IF'
    p[0] = [p[1]]

    global nodos
    id_else_if = id_node()
    n1 = Nodo(Dato(id_else_if, "ELSE_IF_RECURSIVO"))
    nodos[-1].dato.id_nodo_padre = id_else_if
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'ELSE_IF_RECURSIVO'
    nodos.append(n1)

def p_else_if(p):
    'ELSE_IF : else if dosP llaveA CONDICION coma INSTR llaveC'
    if not isinstance(p[5], Error) and not isinstance(p[7], Error):
        p[0] = [p[5], p[7]]
    elif isinstance(p[5], Error) and isinstance(p[7], Error):
        syntax_error = Error("Sintáctico", "Error sintáctico detectado en los bloques de condición e instrucciones del else-if, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)
    elif isinstance(p[5], Error):
        syntax_error = Error("Sintáctico", "Error sintáctico detectado en el bloque de condición del else-if, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)
    else:
        syntax_error = Error("Sintáctico", "Error sintáctico detectado en el bloque de instrucciones del else-if, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)

    global nodos
    id_else_if = id_node()
    n1 = Nodo(Dato(id_else_if, "ELSE_IF"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_else_if))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_else_if))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_else_if))
    n1.insertar_hijo(n4)
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[4]), hoja_lexema=True, id_nodo_padre=id_else_if))
    n1.insertar_hijo(n5)
    nodos[-2].dato.id_nodo_padre = id_else_if
    n1.insertar_hijo(nodos.pop(-2))
    id5 = id_node()
    n6 = Nodo(Dato(id5, str(p[6]), hoja_lexema=True, id_nodo_padre=id_else_if))
    n1.insertar_hijo(n6)
    nodos[-1].dato.id_nodo_padre = id_else_if
    n1.insertar_hijo(nodos.pop())
    id6 = id_node()
    n7 = Nodo(Dato(id6, str(p[8]), hoja_lexema=True, id_nodo_padre=id_else_if))
    n1.insertar_hijo(n7)
    # NODOS REDUCIDOS EN 'ELSE_IF'
    nodos.append(n1)

def p_else_if_error_recuperado(p):
    'ELSE_IF : else if dosP llaveA CONDICION coma INSTR error llaveC'
    if not isinstance(p[5], Error) and not isinstance(p[7], Error):
        p[0] = [p[5], p[7]]
    elif isinstance(p[5], Error) and isinstance(p[7], Error):
        syntax_error = Error("Sintáctico", "Error sintáctico detectado en los bloques de condición e instrucciones del else-if, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)
    elif isinstance(p[5], Error):
        syntax_error = Error("Sintáctico", "Error sintáctico detectado en el bloque de condición del else-if, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)
    else:
        syntax_error = Error("Sintáctico", "Error sintáctico detectado en el bloque de instrucciones del else-if, no pudo ejecutarse el código.", p.lineno(1), find_column(input_string, p.slice[1]))
        p[0] = syntax_error
        errores_sintacticos.append(syntax_error)
    syntax_error = Error("Sintáctico", "Error sintáctico detectado al final del bloque INSTR del ELSE_IF, se logró recuperar del error.", p.lineno(1), find_column(input_string, p.slice[1]))
    errores_sintacticos.append(syntax_error)

    global nodos
    id_else_if = id_node()
    n1 = Nodo(Dato(id_else_if, "ELSE_IF"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_else_if))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_else_if))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_else_if))
    n1.insertar_hijo(n4)
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[4]), hoja_lexema=True, id_nodo_padre=id_else_if))
    n1.insertar_hijo(n5)
    nodos[-2].dato.id_nodo_padre = id_else_if
    n1.insertar_hijo(nodos.pop(-2))
    id5 = id_node()
    n6 = Nodo(Dato(id5, str(p[6]), hoja_lexema=True, id_nodo_padre=id_else_if))
    n1.insertar_hijo(n6)
    nodos[-1].dato.id_nodo_padre = id_else_if
    n1.insertar_hijo(nodos.pop())
    id_error = id_node()
    n_error = Nodo(Dato(id_error, "Syntax Error!", id_nodo_padre=id_else_if, hoja_error=True))
    n1.insertar_hijo(n_error)
    id6 = id_node()
    n7 = Nodo(Dato(id6, str(p[9]), hoja_lexema=True, id_nodo_padre=id_else_if))
    n1.insertar_hijo(n7)
    # NODOS REDUCIDOS EN 'ELSE_IF'
    nodos.append(n1)

def p_else_if_error1(p):
    '''ELSE_IF : else if dosP llaveA CONDICION coma error llaveC
                | else if dosP llaveA CONDICION error llaveC'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado luego de declarada la condición del ELSE_IF.", p.lineno(1), find_column(input_string, p.slice[1]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "ELSE_IF"))
    nodos.pop()
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'ELSE_IF'
    nodos.append(n1)

def p_else_if_error2(p):
    '''ELSE_IF : else if dosP llaveA error llaveC
                | else if dosP error llaveC
                | else if error llaveC'''
    syntax_error = Error("Sintáctico", "Error sintáctico detectado luego al inicio de la declaración del ELSE_IF.", p.lineno(1), find_column(input_string, p.slice[1]))
    p[0] = syntax_error
    errores_sintacticos.append(syntax_error)

    global nodos
    id_decl = id_node()
    n1 = Nodo(Dato(id_decl, "ELSE_IF"))
    id_id = id_node()
    n2 = Nodo(Dato(id_id, "Syntax Error!", id_nodo_padre=id_decl, hoja_error=True))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'ELSE_IF'
    nodos.append(n1)

# TODO Errores sintácticos switch
def p_swith_case_default(p):
    'SWITCH : switch dosP llaveA expr dosP EXPRESION coma CASE_RECURSIVOS coma DEFAULT llaveC'
    p[0] = Switch(p.lineno(1), find_column(input_string, p.slice[1]), p[6], lista_cases=p[8], default=p[10])
    
    global nodos
    id_switch = id_node()
    n1 = Nodo(Dato(id_switch, "SWITCH"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_switch))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_switch))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_switch))
    n1.insertar_hijo(n4)
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[4]), hoja_lexema=True, id_nodo_padre=id_switch))
    n1.insertar_hijo(n5)
    id5 = id_node()
    n6 = Nodo(Dato(id5, str(p[5]), hoja_lexema=True, id_nodo_padre=id_switch))
    n1.insertar_hijo(n6)
    nodos[-3].dato.id_nodo_padre = id_switch
    n1.insertar_hijo(nodos.pop(-3))
    id6 = id_node()
    n7 = Nodo(Dato(id6, str(p[7]), hoja_lexema=True, id_nodo_padre=id_switch))
    n1.insertar_hijo(n7)
    nodos[-2].dato.id_nodo_padre = id_switch
    n1.insertar_hijo(nodos.pop(-2))
    id7 = id_node()
    n8 = Nodo(Dato(id7, str(p[9]), hoja_lexema=True, id_nodo_padre=id_switch))
    n1.insertar_hijo(n8)
    nodos[-1].dato.id_nodo_padre = id_switch
    n1.insertar_hijo(nodos.pop())
    id8 = id_node()
    n9 = Nodo(Dato(id8, str(p[11]), hoja_lexema=True, id_nodo_padre=id_switch))
    n1.insertar_hijo(n9)
    # NODOS REDUCIDOS EN 'SWITCH'
    nodos.append(n1)

def p_switch_case(p):
    'SWITCH : switch dosP llaveA expr dosP EXPRESION coma CASE_RECURSIVOS llaveC'
    p[0] = Switch(p.lineno(1), find_column(input_string, p.slice[1]), p[6], lista_cases=p[8])

    global nodos
    id_switch = id_node()
    n1 = Nodo(Dato(id_switch, "SWITCH"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_switch))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_switch))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_switch))
    n1.insertar_hijo(n4)
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[4]), hoja_lexema=True, id_nodo_padre=id_switch))
    n1.insertar_hijo(n5)
    id5 = id_node()
    n6 = Nodo(Dato(id5, str(p[5]), hoja_lexema=True, id_nodo_padre=id_switch))
    n1.insertar_hijo(n6)
    nodos[-2].dato.id_nodo_padre = id_switch
    n1.insertar_hijo(nodos.pop(-2))
    id6 = id_node()
    n7 = Nodo(Dato(id6, str(p[7]), hoja_lexema=True, id_nodo_padre=id_switch))
    n1.insertar_hijo(n7)
    nodos[-1].dato.id_nodo_padre = id_switch
    n1.insertar_hijo(nodos.pop())
    id7 = id_node()
    n8 = Nodo(Dato(id7, str(p[9]), hoja_lexema=True, id_nodo_padre=id_switch))
    n1.insertar_hijo(n8)
    # NODOS REDUCIDOS EN 'SWITCH'
    nodos.append(n1)

def p_switch_default(p):
    'SWITCH : switch dosP llaveA expr dosP EXPRESION coma DEFAULT llaveC'
    p[0] = Switch(p.lineno(1), find_column(input_string, p.slice[1]), p[6], default=p[8])

    global nodos
    id_switch = id_node()
    n1 = Nodo(Dato(id_switch, "SWITCH"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_switch))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_switch))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_switch))
    n1.insertar_hijo(n4)
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[4]), hoja_lexema=True, id_nodo_padre=id_switch))
    n1.insertar_hijo(n5)
    id5 = id_node()
    n6 = Nodo(Dato(id5, str(p[5]), hoja_lexema=True, id_nodo_padre=id_switch))
    n1.insertar_hijo(n6)
    nodos[-2].dato.id_nodo_padre = id_switch
    n1.insertar_hijo(nodos.pop(-2))
    id6 = id_node()
    n7 = Nodo(Dato(id6, str(p[7]), hoja_lexema=True, id_nodo_padre=id_switch))
    n1.insertar_hijo(n7)
    nodos[-1].dato.id_nodo_padre = id_switch
    n1.insertar_hijo(nodos.pop())
    id7 = id_node()
    n8 = Nodo(Dato(id7, str(p[9]), hoja_lexema=True, id_nodo_padre=id_switch))
    n1.insertar_hijo(n8)
    # NODOS REDUCIDOS EN 'SWITCH'
    nodos.append(n1)

def p_case_recursivos(p):
    'CASE_RECURSIVOS : CASE_RECURSIVOS coma CASE'
    p[1].append(p[3])
    p[0] = p[1]

    global nodos
    id_case = id_node()
    n1 = Nodo(Dato(id_case, "CASE_RECURSIVOS"))
    nodos[-2].dato.id_nodo_padre = id_case
    n1.insertar_hijo(nodos.pop(-2))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[2]), hoja_lexema=True, id_nodo_padre=id_case))
    n1.insertar_hijo(n2)
    nodos[-1].dato.id_nodo_padre = id_case
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'CASE_RECURSIVOS'
    nodos.append(n1)

def p_case_recursivos_case(p):
    'CASE_RECURSIVOS : CASE'
    p[0] = [p[1]]

    global nodos
    id_case = id_node()
    n1 = Nodo(Dato(id_case, "CASE_RECURSIVOS"))
    nodos[-1].dato.id_nodo_padre = id_case
    n1.insertar_hijo(nodos.pop())
    # NODOS REDUCIDOS EN 'CASE_RECURSIVOS'
    nodos.append(n1)

def p_case(p):
    'CASE : case dosP llaveA expr dosP EXPRESION coma INSTR llaveC'
    p[0] = [p[6], p[8]]

    global nodos
    id_case = id_node()
    n1 = Nodo(Dato(id_case, "CASE"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_case))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_case))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_case))
    n1.insertar_hijo(n4)
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[4]), hoja_lexema=True, id_nodo_padre=id_case))
    n1.insertar_hijo(n5)
    id5 = id_node()
    n6 = Nodo(Dato(id5, str(p[5]), hoja_lexema=True, id_nodo_padre=id_case))
    n1.insertar_hijo(n6)
    nodos[-2].dato.id_nodo_padre = id_case
    n1.insertar_hijo(nodos.pop(-2))
    id6 = id_node()
    n7 = Nodo(Dato(id6, str(p[7]), hoja_lexema=True, id_nodo_padre=id_case))
    n1.insertar_hijo(n7)
    nodos[-1].dato.id_nodo_padre = id_case
    n1.insertar_hijo(nodos.pop())
    id7 = id_node()
    n8 = Nodo(Dato(id7, str(p[9]), hoja_lexema=True, id_nodo_padre=id_case))
    n1.insertar_hijo(n8)
    # NODOS REDUCIDOS EN 'CASE'
    nodos.append(n1)

def p_default(p):
    'DEFAULT : default dosP llaveA INSTR llaveC'
    p[0] = p[4]

    global nodos
    id_case = id_node()
    n1 = Nodo(Dato(id_case, "DEFAULT"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_case))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_case))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_case))
    n1.insertar_hijo(n4)
    nodos[-1].dato.id_nodo_padre = id_case
    n1.insertar_hijo(nodos.pop())
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[5]), hoja_lexema=True, id_nodo_padre=id_case))
    n1.insertar_hijo(n5)
    # NODOS REDUCIDOS EN 'DEFAULT'
    nodos.append(n1)

# TODO Errores sintácticos funciones
def p_declaracion_funcion_retorno1(p):
    'DECLARACION_FUNCION : func dosP llaveA tipo dosP TIPO coma nombre dosP id coma param dosP corchA PARAMETROS corchC coma INSTR llaveC'
    p[0] = Declaracion_Func(p.lineno(1), find_column(input_string, p.slice[1]), p[6], p[10], p[18], p[15])

    global nodos
    id_dec_func = id_node()
    n1 = Nodo(Dato(id_dec_func, "DECLARACION_FUNCION"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n4)
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[4]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n5)
    id5 = id_node()
    n6 = Nodo(Dato(id5, str(p[5]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n6)
    nodos[-3].dato.id_nodo_padre = id_dec_func
    n1.insertar_hijo(nodos.pop(-3))
    id6 = id_node()
    n7 = Nodo(Dato(id6, str(p[7]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n7)
    id7 = id_node()
    n8 = Nodo(Dato(id7, str(p[8]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n8)
    id8 = id_node()
    n9 = Nodo(Dato(id8, str(p[9]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n9)
    id9 = id_node()
    n10 = Nodo(Dato(id9, str(p[10]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n10)
    id10 = id_node()
    n11 = Nodo(Dato(id10, str(p[11]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n11)
    id11 = id_node()
    n12 = Nodo(Dato(id11, str(p[12]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n12)
    id12 = id_node()
    n13 = Nodo(Dato(id12, str(p[13]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n13)
    id13 = id_node()
    n14 = Nodo(Dato(id13, str(p[14]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n14)
    nodos[-2].dato.id_nodo_padre = id_dec_func
    n1.insertar_hijo(nodos.pop(-2))
    id14 = id_node()
    n15 = Nodo(Dato(id14, str(p[16]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n15)
    id15 = id_node()
    n16 = Nodo(Dato(id15, str(p[17]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n16)
    nodos[-1].dato.id_nodo_padre = id_dec_func
    n1.insertar_hijo(nodos.pop())
    id16 = id_node()
    n17 = Nodo(Dato(id16, str(p[19]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n17)
    # NODOS REDUCIDOS EN 'DECLARACION_FUNCION'
    nodos.append(n1)

def p_declaracion_funcion_retorno2(p):
    'DECLARACION_FUNCION : func dosP llaveA tipo dosP TIPO coma nombre dosP id coma param dosP corchA corchC coma INSTR llaveC'
    p[0] = Declaracion_Func(p.lineno(1), find_column(input_string, p.slice[1]), p[6], p[10], p[17])

    global nodos
    id_dec_func = id_node()
    n1 = Nodo(Dato(id_dec_func, "DECLARACION_FUNCION"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n4)
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[4]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n5)
    id5 = id_node()
    n6 = Nodo(Dato(id5, str(p[5]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n6)
    nodos[-2].dato.id_nodo_padre = id_dec_func
    n1.insertar_hijo(nodos.pop(-2))
    id6 = id_node()
    n7 = Nodo(Dato(id6, str(p[7]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n7)
    id7 = id_node()
    n8 = Nodo(Dato(id7, str(p[8]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n8)
    id8 = id_node()
    n9 = Nodo(Dato(id8, str(p[9]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n9)
    id9 = id_node()
    n10 = Nodo(Dato(id9, str(p[10]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n10)
    id10 = id_node()
    n11 = Nodo(Dato(id10, str(p[11]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n11)
    id11 = id_node()
    n12 = Nodo(Dato(id11, str(p[12]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n12)
    id12 = id_node()
    n13 = Nodo(Dato(id12, str(p[13]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n13)
    id13 = id_node()
    n14 = Nodo(Dato(id13, str(p[14]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n14)
    id14 = id_node()
    n15 = Nodo(Dato(id14, str(p[15]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n15)
    id15 = id_node()
    n16 = Nodo(Dato(id15, str(p[16]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n16)
    nodos[-1].dato.id_nodo_padre = id_dec_func
    n1.insertar_hijo(nodos.pop())
    id16 = id_node()
    n17 = Nodo(Dato(id16, str(p[18]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n17)
    # NODOS REDUCIDOS EN 'DECLARACION_FUNCION'
    nodos.append(n1)

def p_declaracion_funcion_void1(p):
    'DECLARACION_FUNCION : func dosP llaveA tipo dosP void coma nombre dosP id coma param dosP corchA PARAMETROS corchC coma INSTR llaveC'
    p[0] = Declaracion_Func(p.lineno(1), find_column(input_string, p.slice[1]), "void", p[10], p[18], p[15])

    global nodos
    id_dec_func = id_node()
    n1 = Nodo(Dato(id_dec_func, "DECLARACION_FUNCION"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n4)
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[4]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n5)
    id5 = id_node()
    n6 = Nodo(Dato(id5, str(p[5]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n6)
    id5_5 = id_node()
    n6_6 = Nodo(Dato(id5_5, str(p[6]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n6_6)
    id6 = id_node()
    n7 = Nodo(Dato(id6, str(p[7]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n7)
    id7 = id_node()
    n8 = Nodo(Dato(id7, str(p[8]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n8)
    id8 = id_node()
    n9 = Nodo(Dato(id8, str(p[9]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n9)
    id9 = id_node()
    n10 = Nodo(Dato(id9, str(p[10]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n10)
    id10 = id_node()
    n11 = Nodo(Dato(id10, str(p[11]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n11)
    id11 = id_node()
    n12 = Nodo(Dato(id11, str(p[12]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n12)
    id12 = id_node()
    n13 = Nodo(Dato(id12, str(p[13]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n13)
    id13 = id_node()
    n14 = Nodo(Dato(id13, str(p[14]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n14)
    nodos[-2].dato.id_nodo_padre = id_dec_func
    n1.insertar_hijo(nodos.pop(-2))
    id14 = id_node()
    n15 = Nodo(Dato(id14, str(p[16]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n15)
    id15 = id_node()
    n16 = Nodo(Dato(id15, str(p[17]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n16)
    nodos[-1].dato.id_nodo_padre = id_dec_func
    n1.insertar_hijo(nodos.pop())
    id16 = id_node()
    n17 = Nodo(Dato(id16, str(p[19]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n17)
    # NODOS REDUCIDOS EN 'DECLARACION_FUNCION'
    nodos.append(n1)

def p_declaracion_funcion_void2(p):
    'DECLARACION_FUNCION : func dosP llaveA tipo dosP void coma nombre dosP id coma param dosP corchA corchC coma INSTR llaveC'
    p[0] = Declaracion_Func(p.lineno(1), find_column(input_string, p.slice[1]), "void", p[10], p[17])

    global nodos
    id_dec_func = id_node()
    n1 = Nodo(Dato(id_dec_func, "DECLARACION_FUNCION"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n4)
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[4]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n5)
    id5 = id_node()
    n6 = Nodo(Dato(id5, str(p[5]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n6)
    id5_5 = id_node()
    n6_6 = Nodo(Dato(id5_5, str(p[6]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n6_6)
    id6 = id_node()
    n7 = Nodo(Dato(id6, str(p[7]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n7)
    id7 = id_node()
    n8 = Nodo(Dato(id7, str(p[8]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n8)
    id8 = id_node()
    n9 = Nodo(Dato(id8, str(p[9]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n9)
    id9 = id_node()
    n10 = Nodo(Dato(id9, str(p[10]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n10)
    id10 = id_node()
    n11 = Nodo(Dato(id10, str(p[11]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n11)
    id11 = id_node()
    n12 = Nodo(Dato(id11, str(p[12]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n12)
    id12 = id_node()
    n13 = Nodo(Dato(id12, str(p[13]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n13)
    id13 = id_node()
    n14 = Nodo(Dato(id13, str(p[14]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n14)
    id14 = id_node()
    n15 = Nodo(Dato(id14, str(p[15]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n15)
    id15 = id_node()
    n16 = Nodo(Dato(id15, str(p[16]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n16)
    nodos[-1].dato.id_nodo_padre = id_dec_func
    n1.insertar_hijo(nodos.pop())
    id16 = id_node()
    n17 = Nodo(Dato(id16, str(p[18]), hoja_lexema=True, id_nodo_padre=id_dec_func))
    n1.insertar_hijo(n17)
    # NODOS REDUCIDOS EN 'DECLARACION_FUNCION'
    nodos.append(n1)

def p_parametros_recursivo_primitivo(p):
    'PARAMETROS : PARAMETROS coma TIPO id'
    p[1].append(Parametro(p.lineno(2), find_column(input_string, p.slice[2]), p[3], p[4]))
    p[0] = p[1]

    global nodos
    id_param = id_node()
    n1 = Nodo(Dato(id_param, "PARAMETROS"))
    nodos[-2].dato.id_nodo_padre = id_param
    n1.insertar_hijo(nodos.pop(-2))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[2]), hoja_lexema=True, id_nodo_padre=id_param))
    n1.insertar_hijo(n2)
    nodos[-1].dato.id_nodo_padre = id_param
    n1.insertar_hijo(nodos.pop())
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[4]), hoja_lexema=True, id_nodo_padre=id_param))
    n1.insertar_hijo(n3)
    # NODOS REDUCIDOS EN 'PARAMETROS'
    nodos.append(n1)

def p_parametros_primitivo(p):
    'PARAMETROS : TIPO id'
    p[0] = [Parametro(p.lineno(2), find_column(input_string, p.slice[2]), p[1], p[2])]

    global nodos
    id_param = id_node()
    n1 = Nodo(Dato(id_param, "PARAMETROS"))
    nodos[-1].dato.id_nodo_padre = id_param
    n1.insertar_hijo(nodos.pop())
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[2]), hoja_lexema=True, id_nodo_padre=id_param))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'PARAMETROS'
    nodos.append(n1)

def p_parametros_recursivo_array(p):
    'PARAMETROS : PARAMETROS coma TIPO N_DIMENSIONES id'
    p[1].append(Parametro(p.lineno(2), find_column(input_string, p.slice[2]), p[3], p[5], p[4]))
    p[0] = p[1]

    global nodos
    id_param = id_node()
    n1 = Nodo(Dato(id_param, "PARAMETROS"))
    nodos[-3].dato.id_nodo_padre = id_param
    n1.insertar_hijo(nodos.pop(-3))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[2]), hoja_lexema=True, id_nodo_padre=id_param))
    n1.insertar_hijo(n2)
    nodos[-2].dato.id_nodo_padre = id_param
    n1.insertar_hijo(nodos.pop(-2))
    nodos[-1].dato.id_nodo_padre = id_param
    n1.insertar_hijo(nodos.pop())
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[5]), hoja_lexema=True, id_nodo_padre=id_param))
    n1.insertar_hijo(n3)
    # NODOS REDUCIDOS EN 'PARAMETROS'
    nodos.append(n1)

def p_parametros_array(p):
    'PARAMETROS : TIPO N_DIMENSIONES id'
    p[0] = [Parametro(p.lineno(3), find_column(input_string, p.slice[3]), p[1], p[3], p[2])]

    global nodos
    id_param = id_node()
    n1 = Nodo(Dato(id_param, "PARAMETROS"))
    nodos[-2].dato.id_nodo_padre = id_param
    n1.insertar_hijo(nodos.pop(-2))
    nodos[-1].dato.id_nodo_padre = id_param
    n1.insertar_hijo(nodos.pop())
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[3]), hoja_lexema=True, id_nodo_padre=id_param))
    n1.insertar_hijo(n2)
    # NODOS REDUCIDOS EN 'PARAMETROS'
    nodos.append(n1)

def p_llamada_funcion_parametros(p):
    'LLAMADA_FUNCION : id parA LISTA_VALORES parC pyc'
    p[0] = Funcion(p.lineno(1), find_column(input_string, p.slice[1]), p[1], p[3])

    global nodos
    id_llamada = id_node()
    n1 = Nodo(Dato(id_llamada, "LLAMADA_FUNCION"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_llamada))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_llamada))
    n1.insertar_hijo(n3)
    nodos[-1].dato.id_nodo_padre = id_llamada
    n1.insertar_hijo(nodos.pop())
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[4]), hoja_lexema=True, id_nodo_padre=id_llamada))
    n1.insertar_hijo(n4)
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[5]), hoja_lexema=True, id_nodo_padre=id_llamada))
    n1.insertar_hijo(n5)
    # NODOS REDUCIDOS EN 'LLAMADA_FUNCION'
    nodos.append(n1)

def p_llamada_funcion(p):
    'LLAMADA_FUNCION : id parA parC pyc'
    p[0] = Funcion(p.lineno(1), find_column(input_string, p.slice[1]), p[1])

    global nodos
    id_llamada = id_node()
    n1 = Nodo(Dato(id_llamada, "LLAMADA_FUNCION"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_llamada))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_llamada))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_llamada))
    n1.insertar_hijo(n4)
    id4 = id_node()
    n5 = Nodo(Dato(id4, str(p[4]), hoja_lexema=True, id_nodo_padre=id_llamada))
    n1.insertar_hijo(n5)
    # NODOS REDUCIDOS EN 'LLAMADA_FUNCION'
    nodos.append(n1)

def p_expresion_funcion_parametros(p):
    'EXPRESION : id parA LISTA_VALORES parC'
    p[0] = Funcion(p.lineno(1), find_column(input_string, p.slice[1]), p[1], p[3], True)

    global nodos
    id_exp = id_node()
    n1 = Nodo(Dato(id_exp, "EXPRESION"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_exp))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_exp))
    n1.insertar_hijo(n3)
    nodos[-1].dato.id_nodo_padre = id_exp
    n1.insertar_hijo(nodos.pop())
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[4]), hoja_lexema=True, id_nodo_padre=id_exp))
    n1.insertar_hijo(n4)
    # NODOS REDUCIDOS EN 'EXPRESION'
    nodos.append(n1)

def p_expresion_funcion(p):
    'EXPRESION : id parA parC'
    p[0] = Funcion(fila=p.lineno(1), columna=find_column(input_string, p.slice[1]), identificador=p[1], es_expresion=True)

    global nodos
    id_exp = id_node()
    n1 = Nodo(Dato(id_exp, "EXPRESION"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_exp))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_exp))
    n1.insertar_hijo(n3)
    id3 = id_node()
    n4 = Nodo(Dato(id3, str(p[3]), hoja_lexema=True, id_nodo_padre=id_exp))
    n1.insertar_hijo(n4)
    # NODOS REDUCIDOS EN 'EXPRESION'
    nodos.append(n1)

# TODO Errores sintácticos break
def p_break(p):
    'BREAK : break pyc'
    p[0] = Break(p.lineno(1), find_column(input_string, p.slice[1]))

    global nodos
    id_break = id_node()
    n1 = Nodo(Dato(id_break, "BREAK"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_break))
    n1.insertar_hijo(n2)
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[2]), hoja_lexema=True, id_nodo_padre=id_break))
    n1.insertar_hijo(n3)
    # NODOS REDUCIDOS EN 'BREAK'
    nodos.append(n1)

# TODO Errores sintácticos return
def p_return(p):
    'RETURN : return EXPRESION pyc'
    p[0] = Return(p.lineno(1), find_column(input_string, p.slice[1]), p[2])

    global nodos
    id_return = id_node()
    n1 = Nodo(Dato(id_return, "RETURN"))
    id1 = id_node()
    n2 = Nodo(Dato(id1, str(p[1]), hoja_lexema=True, id_nodo_padre=id_return))
    n1.insertar_hijo(n2)
    nodos[-1].dato.id_nodo_padre = id_return
    n1.insertar_hijo(nodos.pop())
    id2 = id_node()
    n3 = Nodo(Dato(id2, str(p[3]), hoja_lexema=True, id_nodo_padre=id_return))
    n1.insertar_hijo(n3)
    # NODOS REDUCIDOS EN 'RETURN'
    nodos.append(n1)

# Compute column.
# input is the input text string
# token is a token instance
def find_column(input, token):
    line_start = input.rfind('\n', 0, token.lexpos) + 1
    return (token.lexpos - line_start) + 1

# Error rule for syntax errors
def p_error(p):
    if p is not None:
        print(f"Error sintáctico! {p.type, p.value, p.lineno, find_column(input_string, p)}")
    else:
        print("Fin del archivo en un error!")
        return

if __name__ == "__main__":
    from scanner import Scanner
    from syntaxTree.cst import *
    from Entorno.entorno import Entorno
    from Expresiones.funcion import Funcion
    from Expresiones.aritmetica import Aritmetica
    from Expresiones.relacional import Relacional
    from Expresiones.logica import Logica
    from Expresiones.e_inc_dec import E_Inc_Dec
    from Expresiones.primitivo import Primitivo
    from Expresiones.identificador import Identificador
    from Expresiones.identificador_arr import Identificador_Arr
    from Expresiones.negacion import Negacion
    from Expresiones.casteo import Casteo
    from Expresiones.aminus import AMinus
    from Expresiones.amayus import AMayus
    from Expresiones.truncate import Truncate
    from Expresiones.exp_round import Round
    from Expresiones.tamano import Tamano
    from Acciones.declaracion import Declaracion
    from Acciones.declaracion_arr1 import Declaracion_Array1
    from Acciones.declaracion_arr2 import Declaracion_Array2
    from Acciones.declaracion_func import Declaracion_Func
    from Acciones.parametro import Parametro
    from Acciones.asignacion import Asignacion
    from Acciones.asignacion_arr import Asignacion_Arr
    from Acciones.imprimir import Imprimir
    from Acciones.tabla_simbolos import Tabla_Simbolos
    from Acciones.inc_dec import Inc_Dec
    from Acciones.ciclo_while import While
    from Acciones.do_while import Do_While
    from Acciones.ciclo_for import For
    from Acciones.control_if import If
    from Acciones.switch import Switch
    from Acciones.instr_break import Break
    from Acciones.instr_return import Return
    from Acciones.Interface.Error import Error

    scan = Scanner()
    scan.build_scanner()
    lexer = scan.scanner
    tokens = scan.tokens
    parser = yacc.yacc()
    input_string =  ''''''
    errores_semanticos = []
    
    entorno_global = Entorno(None)
    scan.input_data = input_string
    acciones = parser.parse(input_string)
    if acciones is not None:
        for accion in acciones:
            if accion is not None and not isinstance(accion, Error):
                respuesta = accion.ejecutar(entorno_global)
                if isinstance(respuesta, Error):
                    errores_semanticos.append(respuesta)
                elif isinstance(respuesta, list):
                    # Lista de errores que devolvió una instrucción (While, for, ...)
                    errores_semanticos.extend(respuesta)
                elif isinstance(respuesta, dict):
                    # Si viene dict, se detectaron errores semánticos y un break fuera de un bucle, o un return fuera de una función
                    errores_semanticos.extend(respuesta.get("errores"))
                    if respuesta.get("break"):
                        errores_semanticos.append(Error("Error semántico", "Se detectó un break fuera de un bucle.", respuesta.get("break").fila, respuesta.get("break").columna))
                    elif respuesta.get("return"):
                        errores_semanticos.append(Error("Error semántico", "Se detectó un return fuera de una función.", respuesta.get("return").fila, respuesta.get("return").columna))
                    print("Error que no debería de pasar, se detectó un dict, sin que viniera break o return")
                elif isinstance(respuesta, Break):
                    errores_semanticos.append(Error("Error semántico", "Se detectó un break fuera de un bucle.", respuesta.fila, respuesta.columna))
                elif isinstance(respuesta, Return):
                    errores_semanticos.append(Error("Error semántico", "Se detectó un return fuera de una función.", respuesta.fila, respuesta.columna))
    
    print("\n->Variables global")
    for clave in entorno_global.tabla_simbolos:
        print(entorno_global.tabla_simbolos[clave].nombre)
    print("->Funciones global")
    for clave in entorno_global.tabla_simbolos_funciones:
        print(entorno_global.tabla_simbolos_funciones[clave].nombre)

    for e_s in errores_semanticos:
        print(e_s.reporte_str_error())

    # dot_file_arbol = arbol_CST.str_graphviz
    # print(dot_file_arbol)

    for syntax_error in errores_sintacticos:
        print(syntax_error.reporte_str_error())
else:
    from compiler.syntaxTree.cst import *
    from compiler.Entorno.entorno import Entorno
    from compiler.Expresiones.funcion import Funcion
    from compiler.Expresiones.aritmetica import Aritmetica
    from compiler.Expresiones.relacional import Relacional
    from compiler.Expresiones.logica import Logica
    from compiler.Expresiones.e_inc_dec import E_Inc_Dec
    from compiler.Expresiones.primitivo import Primitivo
    from compiler.Expresiones.identificador import Identificador
    from compiler.Expresiones.identificador_arr import Identificador_Arr
    from compiler.Expresiones.negacion import Negacion
    from compiler.Expresiones.casteo import Casteo
    from compiler.Expresiones.aminus import AMinus
    from compiler.Expresiones.amayus import AMayus
    from compiler.Expresiones.truncate import Truncate
    from compiler.Expresiones.exp_round import Round
    from compiler.Expresiones.tamano import Tamano
    from compiler.Acciones.declaracion import Declaracion
    from compiler.Acciones.declaracion_arr1 import Declaracion_Array1
    from compiler.Acciones.declaracion_arr2 import Declaracion_Array2
    from compiler.Acciones.declaracion_func import Declaracion_Func
    from compiler.Acciones.parametro import Parametro
    from compiler.Acciones.asignacion import Asignacion
    from compiler.Acciones.asignacion_arr import Asignacion_Arr
    from compiler.Acciones.imprimir import Imprimir
    from compiler.Acciones.tabla_simbolos import Tabla_Simbolos
    from compiler.Acciones.inc_dec import Inc_Dec
    from compiler.Acciones.ciclo_while import While
    from compiler.Acciones.do_while import Do_While
    from compiler.Acciones.ciclo_for import For
    from compiler.Acciones.control_if import If
    from compiler.Acciones.switch import Switch
    from compiler.Acciones.instr_break import Break
    from compiler.Acciones.instr_return import Return
    from compiler.Acciones.Interface.Error import Error