if __name__ == "compiler.Expresiones.identificador_arr":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Entorno.entorno import Entorno
    from compiler.Entorno.simbolo import Simbolo
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Entorno.entorno import Entorno
    from Entorno.simbolo import Simbolo

class Identificador_Arr(Accion):
    def __init__(self, fila, columna, identificador, expresiones_posicion : list):
        super().__init__(fila, columna)
        self.identificador = identificador
        self.expresiones_posicion = expresiones_posicion
    
    def ejecutar(self, entorno: Entorno):
        simbolo: Simbolo = entorno.buscar_simbolo(str(self.identificador).lower())

        if simbolo is not None:
            if simbolo.tipo == "array":
                posiciones = []
                for expresion_posicion in self.expresiones_posicion:
                    posicion = expresion_posicion.ejecutar(entorno)
                    if isinstance(posicion, Error):
                        self.tipo = 'ERROR_SEMANTICO'
                        return posicion
                    if expresion_posicion.tipo != "int":
                        self.tipo = 'ERROR_SEMANTICO'
                        return Error("Error semántico", f"En la llamada de arreglos, el índice debe ser de tipo int, {expresion_posicion.tipo} fue encontrado.", self.fila, self.columna)
                    posiciones.append(posicion)
                
                valor = self.obtener_valor(simbolo.valor.copy(), posiciones)
                if isinstance(valor, Error):
                    self.tipo = 'ERROR_SEMANTICO'
                    return valor
                
                self.tipo = "array" if isinstance(valor, list) else simbolo.tipo_datos_array
                if self.tipo == "array":
                    self.num_dimensiones = 0
                    self.comprobar_num_dimensiones(valor.copy())
                    self.tipo_datos_array = simbolo.tipo_datos_array
                return valor
            else:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", f"El identificador se llamó utilizando la sintaxis de un arreglo, cuando es de tipo {simbolo.tipo}.", self.fila, self.columna)
        self.tipo = 'ERROR_SEMANTICO'
        return Error("Error semántico", f"No se encontró el identificador del arreglo '{self.identificador}' en el entorno actual, ni en los entornos padre.", self.fila, self.columna)
    
    def obtener_valor(self, valor, arreglo_posiciones: list):
        if len(arreglo_posiciones) == 0:
            return valor
        
        if isinstance(valor, list):
            nuevo_index = arreglo_posiciones.pop(0)
            try:
                return self.obtener_valor(valor[nuevo_index], arreglo_posiciones)
            except:
                return Error("Error semántico", "Se hizo referencia al index de un arreglo fuera de los límites de este.", self.fila, self.columna)
        return Error("Error semántico", "En la llamada a un arreglo, se intentó acceder a una dimensión que no existe", self.fila, self.columna)
    
    def comprobar_num_dimensiones(self, arreglo):
        if isinstance(arreglo, list):
            self.num_dimensiones += 1
            self.comprobar_num_dimensiones(arreglo[0])