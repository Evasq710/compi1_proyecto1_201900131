import distutils.util as d_util

if __name__ == "compiler.Expresiones.aritmetica":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error

class Aritmetica(Accion):
    def __init__(self, fila, columna, operador, hijo_izquierdo, hijo_derecho):
        super().__init__(fila, columna)
        self.operador = operador
        self.hijo_izquierdo = hijo_izquierdo
        self.hijo_derecho = hijo_derecho
        self.tipo = None
    
    def ejecutar(self, entorno):
        # PRIMITIVOS AL PRINCIPIO, RETORNAN ARITMÉTICOS
        izquierdo = self.hijo_izquierdo.ejecutar(entorno)
        if isinstance(izquierdo, Error):
            return izquierdo
        derecho = self.hijo_derecho.ejecutar(entorno)
        if isinstance(derecho, Error):
            return derecho
        
        if self.operador == "+":
            if self.hijo_izquierdo.tipo == 'int':
                if self.hijo_derecho.tipo == 'int':
                    self.tipo = 'int'
                    return izquierdo + derecho
                elif self.hijo_derecho.tipo == 'double':
                    self.tipo = 'double'
                    return self.obtener_valor('double', izquierdo) + derecho
                elif self.hijo_derecho.tipo == 'boolean':
                    self.tipo = 'int'
                    return izquierdo + self.obtener_valor('int', derecho)
                elif self.hijo_derecho.tipo == 'string':
                    self.tipo = 'string'
                    return self.obtener_valor('string', izquierdo) + derecho
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "No se pueden sumar enteros con valores de tipo char o nulos.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'double':
                if self.hijo_derecho.tipo == 'int':
                    self.tipo = 'double'
                    return izquierdo + self.obtener_valor('double', derecho)
                elif self.hijo_derecho.tipo == 'double':
                    self.tipo = 'double'
                    return izquierdo + derecho
                elif self.hijo_derecho.tipo == 'boolean':
                    self.tipo = 'double'
                    return izquierdo + self.obtener_valor('double', derecho)
                elif self.hijo_derecho.tipo == 'string':
                    self.tipo = 'string'
                    return self.obtener_valor('string', izquierdo) + derecho
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "No se pueden sumar doubles con valores de tipo char o nulos.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'boolean':
                if self.hijo_derecho.tipo == 'int':
                    self.tipo = 'int'
                    return self.obtener_valor('int', izquierdo) + derecho
                elif self.hijo_derecho.tipo == 'double':
                    self.tipo = 'double'
                    return self.obtener_valor('double', izquierdo) + derecho
                elif self.hijo_derecho.tipo == 'boolean':
                    self.tipo = 'int'
                    return self.obtener_valor('int', izquierdo) + self.obtener_valor('int', derecho)
                elif self.hijo_derecho.tipo == 'string':
                    self.tipo = 'string'
                    return self.obtener_valor('string', izquierdo).lower() + derecho
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "No se pueden sumar booleanos con valores de tipo char o nulos.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'char':
                if self.hijo_derecho.tipo == 'char':
                    self.tipo = 'string'
                    return self.obtener_valor('string', izquierdo) + self.obtener_valor('string', derecho)
                elif self.hijo_derecho.tipo == 'string':
                    self.tipo = 'string'
                    return self.obtener_valor('string', izquierdo) + derecho
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Unicamente se pueden sumar valores de tipo char con char o string.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'string':
                self.tipo = 'string'
                if self.hijo_derecho.tipo == 'boolean':
                    return izquierdo + self.obtener_valor('string', derecho).lower()
                else:
                    return izquierdo + self.obtener_valor('string', derecho)
            else:
                # TIPO NULL
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", "No se puede sumar 'null' con ningún valor.", self.fila, self.columna)
        elif self.operador == "-":
            if self.hijo_izquierdo.tipo == 'int':
                if self.hijo_derecho.tipo == 'int':
                    self.tipo = 'int'
                    return izquierdo - derecho
                elif self.hijo_derecho.tipo == 'double':
                    self.tipo = 'double'
                    return self.obtener_valor('double', izquierdo) - derecho
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Unicamente se pueden restar valores enteros con enteros o double.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'double':
                if self.hijo_derecho.tipo == 'int':
                    self.tipo = 'double'
                    return izquierdo - self.obtener_valor('double', derecho)
                elif self.hijo_derecho.tipo == 'double':
                    self.tipo = 'double'
                    return izquierdo - derecho
                elif self.hijo_derecho.tipo == 'boolean':
                    self.tipo = 'double'
                    return izquierdo - self.obtener_valor('double', derecho)
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Unicamente se pueden restar valores double con enteros, double o booleanos.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'boolean':
                if self.hijo_derecho.tipo == 'int':
                    self.tipo = 'int'
                    return self.obtener_valor('int', izquierdo) - derecho
                elif self.hijo_derecho.tipo == 'double':
                    self.tipo = 'double'
                    return self.obtener_valor('double', izquierdo) - derecho
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Unicamente se pueden restar valores booleanos con enteros o double.", self.fila, self.columna)
            else:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", "Unicamente se pueden restar valores de enteros, double o booleanos.", self.fila, self.columna)
        elif self.operador == "*":
            if self.hijo_izquierdo.tipo == 'int':
                if self.hijo_derecho.tipo == 'int':
                    self.tipo = 'int'
                    return izquierdo * derecho
                elif self.hijo_derecho.tipo == 'double':
                    self.tipo = 'double'
                    return self.obtener_valor('double', izquierdo) * derecho
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Unicamente se pueden multiplicar enteros con enteros o double.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'double':
                if self.hijo_derecho.tipo == 'int':
                    self.tipo = 'double'
                    return izquierdo * self.obtener_valor('double', derecho)
                elif self.hijo_derecho.tipo == 'double':
                    self.tipo = 'double'
                    return izquierdo * derecho
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Unicamente se pueden multiplicar double con enteros o double.", self.fila, self.columna)
            else:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", "Unicamente se pueden multiplicar enteros o double.", self.fila, self.columna)
        elif self.operador == "/":
            if self.hijo_izquierdo.tipo == 'int':
                if self.hijo_derecho.tipo == 'int':
                    self.tipo = 'double'
                    if derecho != 0:
                        return self.obtener_valor('double', izquierdo) / self.obtener_valor('double', derecho)
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "No se puede dividir entre cero.", self.fila, self.columna)
                elif self.hijo_derecho.tipo == 'double':
                    self.tipo = 'double'
                    if derecho != 0:
                        return self.obtener_valor('double', izquierdo) / derecho
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "No se puede dividir entre cero.", self.fila, self.columna)
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Unicamente se pueden dividir enteros con enteros o double.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'double':
                if self.hijo_derecho.tipo == 'int':
                    self.tipo = 'double'
                    if derecho != 0:
                        return izquierdo / self.obtener_valor('double', derecho)
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "No se puede dividir entre cero.", self.fila, self.columna)
                elif self.hijo_derecho.tipo == 'double':
                    self.tipo = 'double'
                    if derecho != 0:
                        return izquierdo / derecho
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "No se puede dividir entre cero.", self.fila, self.columna)
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Unicamente se pueden dividir double con enteros o double.", self.fila, self.columna)
            else:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", "Unicamente se pueden dividir enteros o double.", self.fila, self.columna)
        elif self.operador == "**":
            if self.hijo_izquierdo.tipo == 'int':
                if self.hijo_derecho.tipo == 'int':
                    self.tipo = 'int'
                    return izquierdo ** derecho
                elif self.hijo_derecho.tipo == 'double':
                    self.tipo = 'double'
                    return self.obtener_valor('double', izquierdo) ** derecho
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "A los tipo int se les debe aplicar potencia solo con enteros o double.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'double':
                if self.hijo_derecho.tipo == 'int':
                    self.tipo = 'double'
                    return izquierdo ** self.obtener_valor('double', derecho)
                elif self.hijo_derecho.tipo == 'double':
                    self.tipo = 'double'
                    return izquierdo ** derecho
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "A los tipo double se les debe aplicar potencia solo con enteros o double.", self.fila, self.columna)
            else:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", "Unicamente se le puede aplicar potencia a enteros o double.", self.fila, self.columna)
        elif self.operador == "%":
            if self.hijo_izquierdo.tipo == 'int':
                if self.hijo_derecho.tipo == 'int':
                    self.tipo = 'double'
                    if derecho != 0:
                        return self.obtener_valor('double', izquierdo) % self.obtener_valor('double', derecho)
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "No se puede sacar el módulo entre cero.", self.fila, self.columna)
                elif self.hijo_derecho.tipo == 'double':
                    self.tipo = 'double'
                    if derecho != 0:
                        return self.obtener_valor('double', izquierdo) % derecho
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "No se puede sacar el módulo entre cero.", self.fila, self.columna)
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "A los tipo int se les debe aplicar módulo solo con enteros o double.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'double':
                if self.hijo_derecho.tipo == 'int':
                    self.tipo = 'double'
                    if derecho != 0:
                        return izquierdo % self.obtener_valor('double', derecho)
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "No se puede sacar el módulo entre cero.", self.fila, self.columna)
                elif self.hijo_derecho.tipo == 'double':
                    self.tipo = 'double'
                    if derecho != 0:
                        return izquierdo % derecho
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "No se puede sacar el módulo entre cero.", self.fila, self.columna)
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "A los tipo double se les debe aplicar módulo solo con enteros o double.", self.fila, self.columna)
            else:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", "Unicamente se le puede aplicar módulo a enteros o double.", self.fila, self.columna)


    def obtener_valor(self, tipo, valor):
        if tipo == 'int':
            return int(valor)
        elif tipo == 'double':
            return float(valor)
        elif tipo == 'string':
            return str(valor)
        elif tipo == 'boolean':
            return bool(valor)