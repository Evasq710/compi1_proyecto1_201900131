if __name__ == "compiler.Expresiones.negacion":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Entorno.entorno import Entorno
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Entorno.entorno import Entorno

class Negacion(Accion):
    def __init__(self, fila, columna, operador, expresion):
        super().__init__(fila, columna)
        self.operador = operador
        self.expresion = expresion
        self.tipo = None
    
    def ejecutar(self, entorno: Entorno):
        valor = self.expresion.ejecutar(entorno)
        if isinstance(valor, Error):
            return valor

        if self.operador == "-":
            if self.expresion.tipo == 'int':
                self.tipo = self.expresion.tipo
                new = - (valor)
                return new
            elif self.expresion.tipo == 'double':
                self.tipo = self.expresion.tipo
                new = - (valor)
                return new
            else:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", "Solo se pueden negar '-' enteros o double.", self.fila, self.columna)
        elif self.operador == "!":
            if self.expresion.tipo == 'boolean':
                self.tipo = self.expresion.tipo
                new = True if valor is False else False
                return new
            else:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", "Solo se pueden contradecir '!' booleanos.", self.fila, self.columna)