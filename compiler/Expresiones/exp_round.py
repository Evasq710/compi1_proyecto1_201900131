if __name__ == "compiler.Expresiones.exp_round":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Entorno.entorno import Entorno
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Entorno.entorno import Entorno

class Round(Accion):
    def __init__(self, fila, columna, expresion):
        super().__init__(fila, columna)
        self.expresion = expresion
    
    def ejecutar(self, entorno: Entorno):
        valor = self.expresion.ejecutar(entorno)
        if isinstance(valor, Error):
            self.tipo = 'ERROR_SEMANTICO'
            return valor
        
        if self.expresion.tipo == "double":
            self.tipo = "int"
            valor = float(valor)
            str_numero = str(valor)
            posicion = 1
            for caracter in str_numero:
                if caracter == ".":
                    break
                posicion += 1
            primer_decimal = str_numero[-(len(str_numero) - posicion)]
            if primer_decimal == "5":
                nuevo_valor = round(valor)
                if nuevo_valor > valor:
                    return nuevo_valor
                nuevo_valor += 1
                return nuevo_valor
            nuevo_valor = round(valor)
            return nuevo_valor
        self.tipo = "ERROR_SEMANTICO"
        return Error("Error semántico", f"La función round solo puede recibir una expresión de tipo double como parámetro, {self.expresion.tipo} fue recibido.", self.fila, self.columna)