if __name__ == "compiler.Expresiones.casteo":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Entorno.entorno import Entorno
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Entorno.entorno import Entorno

class Casteo(Accion):
    def __init__(self, fila, columna, tipo, expresion):
        super().__init__(fila, columna)
        self.tipo = tipo
        self.expresion = expresion
    
    def ejecutar(self, entorno: Entorno):
        valor = self.expresion.ejecutar(entorno)
        if isinstance(valor, Error):
            return valor
        
        if self.tipo == "int":
            if self.expresion.tipo == "double":
                return self.castear_valor(self.tipo, valor)
            elif self.expresion.tipo == "char":
                if len(valor) == 1:
                    return self.castear_valor('int_ascii', valor)
                print(f"Error que no debería pasar: char posee len de {len(valor)}, no se realizó el casteo a int")
                return Error("Error semántico", "Se intentó castear un char a entero, el char posee longitud > 1 caracter, por lo que no se realizó el casteo.", self.fila, self.columna)
            elif self.expresion.tipo == "string":
                return self.castear_valor(self.tipo, valor)
            else:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", f"Los casteos a int admiten únicamente epresiones double, char o string; {self.expresion.tipo} fue detectado.", self.fila, self.columna)
        elif self.tipo == "double":
            if self.expresion.tipo == "int":
                return self.castear_valor(self.tipo, valor)
            elif self.expresion.tipo == "char":
                if len(valor) == 1:
                    return self.castear_valor('double_ascii', valor)
                print(f"Error que no debería pasar: char posee len de {len(valor)}, no se realizó el casteo a double")
                return Error("Error semántico", "Se intentó castear un char a double, el char posee longitud > 1 caracter, por lo que no se realizó el casteo.", self.fila, self.columna)
            elif self.expresion.tipo == "string":
                return self.castear_valor(self.tipo, valor)
            else:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", f"Los casteos a double admiten únicamente epresiones int, char o string; {self.expresion.tipo} fue detectado.", self.fila, self.columna)
        elif self.tipo == "boolean":
            if self.expresion.tipo == "string":
                if valor.lower() == "true":
                    return True
                elif valor.lower() == "false":
                    return False
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", f"Se intentó castear un string a boolean, la cadena recibida ({valor}) no es válida.", self.fila, self.columna)
            else:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", "Casteos a valores booleanos se admiten solo de tipo string.", self.fila, self.columna)
        elif self.tipo == "char":
            if self.expresion.tipo == "int":
                return self.castear_valor(self.tipo, valor)
            else:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", f"Los casteos a char admiten únicamente epresiones int; {self.expresion.tipo} fue detectado.", self.fila, self.columna)
        elif self.tipo == "string":
            if self.expresion.tipo == "int" or self.expresion.tipo == "double":
                return self.castear_valor(self.tipo, valor)
            else:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", f"Los casteos a string admiten únicamente epresiones int o double; {self.expresion.tipo} fue detectado.", self.fila, self.columna)
        else:
            intento = self.tipo
            self.tipo = 'ERROR_SEMANTICO'
            return Error("Error semántico", f"Los únicos casteos permitidos son int, double, boolean, char y string; {intento} fue detectado.", self.fila, self.columna)
    
    def castear_valor(self, tipo, valor):
        if tipo == 'int':
            try:
                valor = int(valor)
                return valor
            except:
                return Error("Error semántico", "Se intentó castear un string a entero, la cadena contiene caracteres inválidos para el casteo.", self.fila, self.columna)
        elif tipo == 'double':
            try:
                valor = float(valor)
                return valor
            except:
                return Error("Error semántico", "Se intentó castear un string a double, la cadena contiene caracteres inválidos para el casteo.", self.fila, self.columna)
        elif tipo == 'string':
            return str(valor)
        elif tipo == 'boolean':
            return bool(valor)
        elif tipo == 'int_ascii':
            # Recibe solo CARACTERES (string), devuelve el valor unicode en int
            return ord(valor)
        elif tipo == 'double_ascii':
            # Recibe solo CARACTERES (string), devuelve el valor unicode en int
            return float(ord(valor))
        elif tipo == 'char':
            # Recibe solo ENTEROS, devuelve el caracter como string (de 0 hasta 1114111)
            try:
                valor = chr(int(valor))
                return valor
            except:
                if valor < 0 or valor > 1114111:
                    return Error("Error semántico", "Se intentó castear un número a char, el número se encontraba fuera del rango válido (0 a 1114111).", self.fila, self.columna)
                return Error("Error semántico", f"Se intentó castear un número a char, no fue posible castear el número {valor}.", self.fila, self.columna)
                