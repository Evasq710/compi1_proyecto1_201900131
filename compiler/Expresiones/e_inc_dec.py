if __name__ == "compiler.Expresiones.e_inc_dec":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Entorno.entorno import Entorno
    from compiler.Entorno.simbolo import Simbolo
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Entorno.entorno import Entorno
    from Entorno.simbolo import Simbolo

class E_Inc_Dec(Accion):
    def __init__(self, fila, columna, identificador, operacion):
        super().__init__(fila, columna)
        self.identificador = identificador
        self.operacion = operacion
        self.tipo = None
    
    def ejecutar(self, entorno: Entorno):
        variable: Simbolo = entorno.buscar_simbolo(str(self.identificador).lower())
        if variable is not None:
            if variable.tipo == 'int' or variable.tipo == 'double':
                if self.operacion == "++":
                    self.tipo = variable.tipo
                    entorno.editar_simbolo(variable.nombre, Simbolo(variable.tipo, variable.nombre, variable.valor + 1, variable.linea))
                    return variable.valor + 1
                elif self.operacion == "--":
                    self.tipo = variable.tipo
                    entorno.editar_simbolo(variable.nombre, Simbolo(variable.tipo, variable.nombre, variable.valor - 1, variable.linea))
                    return variable.valor - 1
            else:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", "No se puede incrementar/decrementar una variable que no sea de tipo entero o double.", self.fila, self.columna)
        self.tipo = 'ERROR_SEMANTICO'
        return Error("Error semántico", "No se encontró el identificador en el entorno actual, ni en los padre, no se realizó el incremento/decremento.", self.fila, self.columna)