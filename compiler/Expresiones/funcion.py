if __name__ == "compiler.Expresiones.funcion":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Acciones.instr_return import Return
    from compiler.Acciones.instr_break import Break
    from compiler.Acciones.parametro import Parametro
    from compiler.Entorno.entorno import Entorno
    from compiler.Entorno.simbolo import Simbolo
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Acciones.instr_return import Return
    from Acciones.instr_break import Break
    from Acciones.parametro import Parametro
    from Entorno.entorno import Entorno
    from Entorno.simbolo import Simbolo

class Funcion(Accion):
    def __init__(self, fila, columna, identificador, lista_parametros_expresiones = None, es_expresion = False):
        super().__init__(fila, columna)
        self.identificador = identificador
        self.lista_parametros_expresiones = lista_parametros_expresiones
        self.es_expresion = es_expresion
    
    def ejecutar(self, entorno: Entorno):
        func : Simbolo = entorno.buscar_funcion(self.identificador)
        if func is None:
            self.tipo = 'ERROR_SEMANTICO'
            return Error("Error semántico", "No se encontró el identificador de la función en el entorno actual, ni en los entornos padre.", self.fila, self.columna)
        
        if func.tipo_retorno == "void" and self.es_expresion:
            self.tipo = 'ERROR_SEMANTICO'
            return Error("Error semántico", "Se hizo el llamado a una función de tipo 'void', dentro de un contexto en donde se espera que retorne un valor.", self.fila, self.columna)
        
        if self.lista_parametros_expresiones is None and func.parametros is not None:
            self.tipo = 'ERROR_SEMANTICO'
            return Error("Error semántico", f"Se hizo el llamado a una función. Se esperaban {len(func.parametros)} parametros, ninguno fue enviado.", self.fila, self.columna)
        
        if self.lista_parametros_expresiones is not None and func.parametros is None:
            self.tipo = 'ERROR_SEMANTICO'
            return Error("Error semántico", f"Se hizo el llamado a una función. No se esperaban parametros, {len(self.lista_parametros_expresiones)} fueron enviados.", self.fila, self.columna)
        
        if self.lista_parametros_expresiones is not None and func.parametros is not None:
            if len(self.lista_parametros_expresiones) != len(func.parametros):
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", f"Se hizo el llamado a una función. Se esperaban {len(func.parametros)} parametros, {len(self.lista_parametros_expresiones)} fueron enviados.", self.fila, self.columna)
        
        entorno_funcion = Entorno(entorno)
        if func.parametros is not None:
            parametros = func.parametros.copy()
            # Declarando los parámetros en el entorno actual
            for expresion in self.lista_parametros_expresiones:
                # fila, columna, tipo, identificador, n_dimensiones : int = None
                parametro_esperado : Parametro = parametros.pop(0)
                valor = expresion.ejecutar(entorno)
                if isinstance(valor, Error):
                    self.tipo = 'ERROR_SEMANTICO'
                    return valor
                
                if parametro_esperado.n_dimensiones is None:
                    # parametro esperado no es array
                    if parametro_esperado.tipo != expresion.tipo:
                        self.tipo = 'ERROR_SEMANTICO'
                        return Error("Error semántico", f"En la llamada a una función, ocurrió un error con los parámetros recibidos. Se esperaba uno de tipo {parametro_esperado.tipo}, pero vino de tipo {expresion.tipo}.", self.fila, self.columna)
                    
                    simbolo = Simbolo(parametro_esperado.tipo, parametro_esperado.identificador.lower(), valor, parametro_esperado.fila)
                    insertado = entorno_funcion.insertar_simbolo(simbolo)
                    if not insertado:
                        # Ya existe el símbolo en el entorno, no se inserta
                        self.tipo = 'ERROR_SEMANTICO'
                        return Error("Error semántico", "No se puede declarar la variable del parámetro, esta ya existe en el entorno de la función.", self.fila, self.columna)
                else:
                    # parametro esperado es un array
                    if expresion.tipo != "array":
                        self.tipo = 'ERROR_SEMANTICO'
                        return Error("Error semántico", f"En la llamada a una función, ocurrió un error con los parámetros recibidos. Se esperaba uno de tipo 'array', pero vino de tipo {expresion.tipo}.", self.fila, self.columna)
                    
                    if expresion.num_dimensiones != parametro_esperado.n_dimensiones:
                        self.tipo = 'ERROR_SEMANTICO'
                        return Error("Error semántico", f"En la llamada a una función, ocurrió un error con los parámetros recibidos. Se esperaba un array de {parametro_esperado.n_dimensiones} dimension(es), uno de {expresion.num_dimensiones} fue recibido.", self.fila, self.columna)
                    
                    if expresion.tipo_datos_array != parametro_esperado.tipo:
                        self.tipo = 'ERROR_SEMANTICO'
                        return Error("Error semántico", f"En la llamada a una función, ocurrió un error con los parámetros recibidos. Se esperaba un array de tipo {parametro_esperado.tipo}, uno de tipo {expresion.tipo_datos_array} fue recibido.", self.fila, self.columna)
                    
                    simbolo = Simbolo(tipo="array", nombre=parametro_esperado.identificador.lower(), valor=valor, linea=parametro_esperado.fila, tipo_datos_array=parametro_esperado.tipo, num_dimensiones=parametro_esperado.n_dimensiones)
                    insertado = entorno_funcion.insertar_simbolo(simbolo)

                    if not insertado:
                        # Ya existe el símbolo en el entorno, no se inserta
                        self.tipo = 'ERROR_SEMANTICO'
                        return Error("Error semántico", "No se puede declarar el arreglo del parámetro, su identificador ya existe en el entorno de la función.", self.fila, self.columna)

        err_semanticos_internos = []
        vino_return = False
        instancia_return = None
        for accion in func.valor:
            if accion is not None and not isinstance(accion, Error):
                respuesta = accion.ejecutar(entorno_funcion)
                if isinstance(respuesta, Error):
                    err_semanticos_internos.append(respuesta)
                elif isinstance(respuesta, list):
                    err_semanticos_internos.extend(respuesta)
                elif isinstance(respuesta, dict):
                    # Si viene dict, se detectaron errores semánticos y un break o un return
                    err_semanticos_internos.extend(respuesta.get("errores"))
                    if respuesta.get("break"):
                        err_semanticos_internos.append(Error("Error semántico", "Se detectó un break fuera de un bucle.", respuesta.get("break").fila, respuesta.get("break").columna))
                        break
                    instancia_return = respuesta.get("return")
                    if instancia_return:
                        vino_return = True
                        break
                    print("Error que no debería de pasar, se detectó un dict en una función, sin que viniera break o return")
                elif isinstance(respuesta, Break):
                    err_semanticos_internos.append(Error("Error semántico", "Se detectó un break fuera de un bucle.", respuesta.fila, respuesta.columna))
                    break
                elif isinstance(respuesta, Return):
                    instancia_return = respuesta
                    vino_return = True
                    break
        
        if vino_return and func.tipo_retorno == "void":
            print("Error que no debería de pasar, se llamó a una función de tipo 'void', pero se encontró un return")
            err_semanticos_internos.append(Error("Error semántico", "Se llamó a una función de tipo 'void', pero se encontró un return.", self.fila, self.columna))
        
        if not vino_return and func.tipo_retorno != "void":
            print(f"Error que no debería de pasar, se llamó a una función de tipo '{func.tipo_retorno}', pero no se encontró un return")
            err_semanticos_internos.append(Error("Error semántico", f"Se llamó a una función de tipo '{func.tipo_retorno}', pero no se encontró un return.", self.fila, self.columna))
        
        if err_semanticos_internos:
            self.tipo = 'ERROR_SEMANTICO'
            if not self.es_expresion:
                return err_semanticos_internos
            if len(err_semanticos_internos) == 1:
                return err_semanticos_internos[0]
            print("***** DETALLE DE ERRORES SEMÁNTICOS EN FUNCIÓN COMO EXPRESIÓN *****")
            for error in err_semanticos_internos:
                print(error.reporte_str_error())
            print("*******************************************************************")
            return Error("Error semántico", "Se encontraron errores semánticos en la llamada a una función, en un contexto de epxresión. Ver consola para más detalles.", self.fila, self.columna)
        
        if instancia_return is not None:
            for clave in instancia_return.entorno_return.tabla_simbolos:
                entorno_funcion.tabla_simbolos[clave] = instancia_return.entorno_return.tabla_simbolos[clave]
            valor = instancia_return.expresion.ejecutar(entorno_funcion)
            if isinstance(valor, Error):
                self.tipo = 'ERROR_SEMANTICO'
                return valor
            
            if func.tipo_retorno != instancia_return.expresion.tipo:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", f"Se llamó a una función de tipo '{func.tipo_retorno}', pero se encontró un return de tipo {instancia_return.expresion.tipo}.", self.fila, self.columna)
            self.tipo = func.tipo_retorno
            return valor