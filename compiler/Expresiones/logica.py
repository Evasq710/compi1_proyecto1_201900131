if __name__ == "compiler.Expresiones.logica":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error

class Logica(Accion):
    def __init__(self, fila, columna, operador, hijo_izquierdo, hijo_derecho):
        super().__init__(fila, columna)
        self.operador = operador
        self.hijo_izquierdo = hijo_izquierdo
        self.hijo_derecho = hijo_derecho
        self.tipo = None
    
    def ejecutar(self, entorno):
        izquierdo = self.hijo_izquierdo.ejecutar(entorno)
        if isinstance(izquierdo, Error):
            return izquierdo
        derecho = self.hijo_derecho.ejecutar(entorno)
        if isinstance(derecho, Error):
            return derecho

        if self.operador == "||":
            if self.hijo_izquierdo.tipo == 'boolean' and self.hijo_derecho.tipo == 'boolean':
                self.tipo = 'boolean'
                retorno = True if izquierdo is True or derecho is True else False
                return retorno
            else:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", "Solo se puede comparar con '||' si ambos son booleanos.", self.fila, self.columna)
        elif self.operador == "&&":
            if self.hijo_izquierdo.tipo == 'boolean' and self.hijo_derecho.tipo == 'boolean':
                self.tipo = 'boolean'
                retorno = True if izquierdo is True and derecho is True else False
                return retorno
            else:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", "Solo se puede comparar con '&&' si ambos son booleanos.", self.fila, self.columna)
