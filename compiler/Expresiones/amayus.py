if __name__ == "compiler.Expresiones.amayus":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Entorno.entorno import Entorno
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Entorno.entorno import Entorno

class AMayus(Accion):
    def __init__(self, fila, columna, expresion):
        super().__init__(fila, columna)
        self.expresion = expresion
    
    def ejecutar(self, entorno: Entorno):
        valor = self.expresion.ejecutar(entorno)
        if isinstance(valor, Error):
            self.tipo = 'ERROR_SEMANTICO'
            return valor
        
        if self.expresion.tipo == "string":
            self.tipo = "string"
            valor = str(valor).upper()
            return valor
        self.tipo = "ERROR_SEMANTICO"
        return Error("Error semántico", f"La función aMayus solo puede recibir una expresión de tipo string como parámetro, {self.expresion.tipo} fue recibido.", self.fila, self.columna)