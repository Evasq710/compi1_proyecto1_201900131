if __name__ == "compiler.Expresiones.primitivo":
    from compiler.Acciones.Interface.Accion import Accion
else:
    from Acciones.Interface.Accion import Accion

class Primitivo(Accion):
    def __init__(self, fila, columna, tipo, valor):
        super().__init__(fila, columna)
        self.tipo = tipo
        self.valor = valor
    
    def ejecutar(self, entorno):
        if self.tipo != "null":
            return self.valor
        return "null"