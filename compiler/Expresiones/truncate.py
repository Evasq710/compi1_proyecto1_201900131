if __name__ == "compiler.Expresiones.truncate":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Entorno.entorno import Entorno
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Entorno.entorno import Entorno

class Truncate(Accion):
    def __init__(self, fila, columna, expresion):
        super().__init__(fila, columna)
        self.expresion = expresion
    
    def ejecutar(self, entorno: Entorno):
        valor = self.expresion.ejecutar(entorno)
        if isinstance(valor, Error):
            self.tipo = 'ERROR_SEMANTICO'
            return valor
        
        if self.expresion.tipo == "double":
            self.tipo = "int"
            valor = int(valor)
            return valor
        self.tipo = "ERROR_SEMANTICO"
        return Error("Error semántico", f"La función truncate solo puede recibir una expresión de tipo double como parámetro, {self.expresion.tipo} fue recibido.", self.fila, self.columna)