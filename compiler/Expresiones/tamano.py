if __name__ == "compiler.Expresiones.tamano":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Entorno.entorno import Entorno
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Entorno.entorno import Entorno

class Tamano(Accion):
    def __init__(self, fila, columna, expresion):
        super().__init__(fila, columna)
        self.expresion = expresion
    
    def ejecutar(self, entorno: Entorno):
        valor = self.expresion.ejecutar(entorno)
        if isinstance(valor, Error):
            self.tipo = 'ERROR_SEMANTICO'
            return valor
        
        if self.expresion.tipo == "array":
            if not isinstance(valor, list):
                self.tipo = 'ERROR_SEMANTICO'
                print("Error que no debe pasar: En el argumento de la función nativa \"tamano\" se recibió un valor que no es un array")
                return Error("Error semántico", "En el argumento de la función nativa \"tamano\" se recibió un valor que no es un array.", self.fila, self.columna)
                
            self.tipo = "int"
            return len(valor)
        
        self.tipo = "ERROR_SEMANTICO"
        return Error("Error semántico", f"La función tamano solo puede recibir una expresión de tipo array como parámetro, {self.expresion.tipo} fue recibido.", self.fila, self.columna)