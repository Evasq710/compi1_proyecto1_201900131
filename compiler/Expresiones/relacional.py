if __name__ == "compiler.Expresiones.relacional":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error

class Relacional(Accion):
    def __init__(self, fila, columna, operador, hijo_izquierdo, hijo_derecho):
        super().__init__(fila, columna)
        self.operador = operador
        self.hijo_izquierdo = hijo_izquierdo
        self.hijo_derecho = hijo_derecho
        self.tipo = None
    
    def ejecutar(self, entorno):
        izquierdo = self.hijo_izquierdo.ejecutar(entorno)
        if isinstance(izquierdo, Error):
            return izquierdo
        derecho = self.hijo_derecho.ejecutar(entorno)
        if isinstance(derecho, Error):
            return derecho

        
        if self.operador == "==":
            if self.hijo_izquierdo.tipo == 'int':
                if self.hijo_derecho.tipo == 'int':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo == derecho else False
                    return retorno
                elif self.hijo_derecho.tipo == 'double':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo == derecho else False
                    return retorno
                elif self.hijo_derecho.tipo == 'string':
                    self.tipo = 'boolean'
                    retorno = True if str(izquierdo) == derecho else False
                    return retorno
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Solo se pueden comparar con '==' enteros con enteros, double o string.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'double':
                if self.hijo_derecho.tipo == 'int':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo == derecho else False
                    return retorno
                elif self.hijo_derecho.tipo == 'double':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo == derecho else False
                    return retorno
                elif self.hijo_derecho.tipo == 'string':
                    self.tipo = 'boolean'
                    retorno = True if str(izquierdo) == derecho else False
                    return retorno
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Solo se pueden comparar con '==' double con enteros, double o string.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'boolean':
                if self.hijo_derecho.tipo == 'boolean':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo == derecho else False
                    return retorno
                elif self.hijo_derecho.tipo == 'string':
                    self.tipo = 'boolean'
                    retorno = True if str(izquierdo).lower() == derecho else False
                    return retorno
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Solo se pueden comparar con '==' booleanos con booleanos o string.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'char':
                if self.hijo_derecho.tipo == 'char':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo == derecho else False
                    return retorno
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Solo se puede comparar con '==' char con char.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'string':
                if self.hijo_derecho.tipo == 'int' or self.hijo_derecho.tipo == 'double':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo == str(derecho) else False
                    return retorno
                elif self.hijo_derecho.tipo == 'boolean':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo == str(derecho).lower() else False
                    return retorno
                elif self.hijo_derecho.tipo == 'string':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo == derecho else False
                    return retorno
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "No se pueden comparar con '==' string con char o null.", self.fila, self.columna)
            else:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", "No se puede comparar con '==' un valor nulo.", self.fila, self.columna)
        elif self.operador == "!=":
            if self.hijo_izquierdo.tipo == 'int':
                if self.hijo_derecho.tipo == 'int':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo != derecho else False
                    return retorno
                elif self.hijo_derecho.tipo == 'double':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo != derecho else False
                    return retorno
                elif self.hijo_derecho.tipo == 'string':
                    self.tipo = 'boolean'
                    retorno = True if str(izquierdo) != derecho else False
                    return retorno
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Solo se pueden comparar con '!=' enteros con enteros, double o string.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'double':
                if self.hijo_derecho.tipo == 'int':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo != derecho else False
                    return retorno
                elif self.hijo_derecho.tipo == 'double':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo != derecho else False
                    return retorno
                elif self.hijo_derecho.tipo == 'string':
                    self.tipo = 'boolean'
                    retorno = True if str(izquierdo) != derecho else False
                    return retorno
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Solo se pueden comparar con '!=' double con enteros, double o string.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'boolean':
                if self.hijo_derecho.tipo == 'boolean':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo != derecho else False
                    return retorno
                elif self.hijo_derecho.tipo == 'string':
                    self.tipo = 'boolean'
                    retorno = True if str(izquierdo).lower() != derecho else False
                    return retorno
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Solo se pueden comparar con '!=' booleanos con booleanos o string.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'char':
                if self.hijo_derecho.tipo == 'char':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo != derecho else False
                    return retorno
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Solo se puede comparar con '!=' char con char.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'string':
                if self.hijo_derecho.tipo == 'int' or self.hijo_derecho.tipo == 'double':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo != str(derecho) else False
                    return retorno
                elif self.hijo_derecho.tipo == 'boolean':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo != str(derecho).lower() else False
                    return retorno
                elif self.hijo_derecho.tipo == 'string':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo != derecho else False
                    return retorno
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "No se pueden comparar con '!=' string con char o null.", self.fila, self.columna)
            else:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", "No se puede comparar con '!=' un valor nulo.", self.fila, self.columna)
        elif self.operador == "<":
            if self.hijo_izquierdo.tipo == 'int':
                if self.hijo_derecho.tipo == 'int' or self.hijo_derecho.tipo == 'double':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo < derecho else False
                    return retorno
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Solo se pueden comparar con '<' enteros con enteros o double.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'double':
                if self.hijo_derecho.tipo == 'int' or self.hijo_derecho.tipo == 'double':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo < derecho else False
                    return retorno
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Solo se pueden comparar con '<' double con enteros o double.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'boolean':
                if self.hijo_derecho.tipo == 'boolean':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo < derecho else False
                    return retorno
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Solo se pueden comparar con '<' booleanos con booleanos.", self.fila, self.columna)
            else:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", "Solo se puede comparar con '<' valores enteros, double y booleanos.", self.fila, self.columna)
        elif self.operador == ">":
            if self.hijo_izquierdo.tipo == 'int':
                if self.hijo_derecho.tipo == 'int' or self.hijo_derecho.tipo == 'double':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo > derecho else False
                    return retorno
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Solo se pueden comparar con '>' enteros con enteros o double.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'double':
                if self.hijo_derecho.tipo == 'int' or self.hijo_derecho.tipo == 'double':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo > derecho else False
                    return retorno
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Solo se pueden comparar con '>' double con enteros o double.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'boolean':
                if self.hijo_derecho.tipo == 'boolean':
                    self.tipo = 'boolean'
                    retorno = True if int(izquierdo) > int(derecho) else False
                    return retorno
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Solo se pueden comparar con '>' booleanos con booleanos.", self.fila, self.columna)
            else:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", "Solo se puede comparar con '>' valores enteros, double y booleanos.", self.fila, self.columna)
        elif self.operador == "<=":
            if self.hijo_izquierdo.tipo == 'int':
                if self.hijo_derecho.tipo == 'int' or self.hijo_derecho.tipo == 'double':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo <= derecho else False
                    return retorno
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Solo se pueden comparar con '<=' enteros con enteros o double.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'double':
                if self.hijo_derecho.tipo == 'int' or self.hijo_derecho.tipo == 'double':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo <= derecho else False
                    return retorno
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Solo se pueden comparar con '<=' double con enteros o double.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'boolean':
                if self.hijo_derecho.tipo == 'boolean':
                    self.tipo = 'boolean'
                    retorno = True if int(izquierdo) <= int(derecho) else False
                    return retorno
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Solo se pueden comparar con '<=' booleanos con booleanos.", self.fila, self.columna)
            else:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", "Solo se puede comparar con '<=' valores enteros, double y booleanos.", self.fila, self.columna)
        elif self.operador == ">=":
            if self.hijo_izquierdo.tipo == 'int':
                if self.hijo_derecho.tipo == 'int' or self.hijo_derecho.tipo == 'double':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo >= derecho else False
                    return retorno
                else:
                    # TODO error semántico
                    pass
            elif self.hijo_izquierdo.tipo == 'double':
                if self.hijo_derecho.tipo == 'int' or self.hijo_derecho.tipo == 'double':
                    self.tipo = 'boolean'
                    retorno = True if izquierdo >= derecho else False
                    return retorno
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Solo se pueden comparar con '>=' enteros con enteros o double.", self.fila, self.columna)
            elif self.hijo_izquierdo.tipo == 'boolean':
                if self.hijo_derecho.tipo == 'boolean':
                    self.tipo = 'boolean'
                    retorno = True if int(izquierdo) >= int(derecho) else False
                    return retorno
                else:
                    self.tipo = 'ERROR_SEMANTICO'
                    return Error("Error semántico", "Solo se pueden comparar con '>=' double con enteros o double.", self.fila, self.columna)
            else:
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", "Solo se puede comparar con '>=' valores enteros, double y booleanos.", self.fila, self.columna)
