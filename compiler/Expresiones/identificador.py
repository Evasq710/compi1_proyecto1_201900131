if __name__ == "compiler.Expresiones.identificador":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Entorno.entorno import Entorno
    from compiler.Entorno.simbolo import Simbolo
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Entorno.entorno import Entorno
    from Entorno.simbolo import Simbolo

class Identificador(Accion):
    def __init__(self, fila, columna, identificador):
        super().__init__(fila, columna)
        self.identificador = identificador
        self.numero_dimensiones = None
    
    def ejecutar(self, entorno: Entorno):
        simbolo: Simbolo = entorno.buscar_simbolo(str(self.identificador).lower())

        if simbolo is not None:
            self.tipo = simbolo.tipo
            if self.tipo == "array":
                self.num_dimensiones = simbolo.num_dimensiones
                self.tipo_datos_array = simbolo.tipo_datos_array
            return simbolo.valor
        self.tipo = 'ERROR_SEMANTICO'
        return Error("Error semántico", f"No se encontró el identificador '{self.identificador}' en el entorno actual, ni en los entornos padre.", self.fila, self.columna)