if __name__ == "compiler.Acciones.control_if":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Acciones.instr_break import Break
    from compiler.Acciones.instr_return import Return
    from compiler.Entorno.entorno import Entorno
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Acciones.instr_break import Break
    from Acciones.instr_return import Return
    from Entorno.entorno import Entorno

class If(Accion):
    def __init__(self, fila, columna, condicion_if, lista_instrucciones, lista_else_if = None, lista_instr_else = None):
        super().__init__(fila, columna)
        self.condicion_if = condicion_if
        self.lista_instrucciones = lista_instrucciones
        # 0 -> condicion, 1 -> instrucciones
        self.lista_else_if = lista_else_if
        self.lista_instr_else = lista_instr_else
    
    def ejecutar(self, entorno: Entorno):
        condicion_if = self.condicion_if.ejecutar(entorno)
        if isinstance(condicion_if, Error):
            return condicion_if
        
        if condicion_if is True or condicion_if is False:
            err_semanticos_internos = []
            vino_break = False
            instancia_break = None
            vino_return = False
            instancia_return = None
            # IF
            if condicion_if:
                nuevo_entorno = Entorno(entorno)
                for accion in self.lista_instrucciones:
                    if accion is not None and not isinstance(accion, Error):
                        respuesta = accion.ejecutar(nuevo_entorno)
                        if isinstance(respuesta, Error):
                            err_semanticos_internos.append(respuesta)
                        elif isinstance(respuesta, list):
                            err_semanticos_internos.extend(respuesta)
                        elif isinstance(respuesta, dict):
                            # Si viene dict, se detectaron errores semánticos y un break o un return
                            err_semanticos_internos.extend(respuesta.get("errores"))
                            instancia_break = respuesta.get("break")
                            if instancia_break:
                                vino_break = True
                                break
                            instancia_return = respuesta.get("return")
                            if instancia_return:
                                vino_return = True
                                break
                            print("Error que no debería de pasar, se detectó un dict en if, sin que viniera break o return")
                        elif isinstance(respuesta, Break):
                            instancia_break = respuesta
                            vino_break = True
                            break
                        elif isinstance(respuesta, Return):
                            instancia_return = respuesta
                            vino_return = True
                            break

                if vino_break:    
                    if err_semanticos_internos:
                        # Vinieron errores y un break
                        retorno = {"errores": err_semanticos_internos, "break": instancia_break}
                        return retorno
                    # Vino solo un break
                    return instancia_break
                if vino_return:
                    if err_semanticos_internos:
                        # Vinieron errores y un return
                        retorno = {"errores": err_semanticos_internos, "return": instancia_return}
                        return retorno
                    # Vino solo un return
                    return instancia_return
                if err_semanticos_internos:
                    # No vino break ni return, vinieron errores
                    return err_semanticos_internos
                # No vino break ni return ni errores
            else:
                cumplio_condicion = False

                if self.lista_else_if is not None:
                    # ELSE IF
                    for else_if in self.lista_else_if:
                        condicion = else_if[0].ejecutar(entorno)
                        if isinstance(condicion, Error):
                            return condicion
                            
                        if condicion is True or condicion is False:
                            if condicion:
                                cumplio_condicion = True
                                nuevo_entorno = Entorno(entorno)
                                for accion in else_if[1]:
                                    if accion is not None and not isinstance(accion, Error):
                                        respuesta = accion.ejecutar(nuevo_entorno)
                                        if isinstance(respuesta, Error):
                                            err_semanticos_internos.append(respuesta)
                                        elif isinstance(respuesta, list):
                                            err_semanticos_internos.extend(respuesta)
                                        elif isinstance(respuesta, dict):
                                            # Si viene dict, se detectaron errores semánticos y un break o un return
                                            err_semanticos_internos.extend(respuesta.get("errores"))
                                            instancia_break = respuesta.get("break")
                                            if instancia_break:
                                                vino_break = True
                                                break
                                            instancia_return = respuesta.get("return")
                                            if instancia_return:
                                                vino_return = True
                                                break
                                            print("Error que no debería de pasar, se detectó un dict en else if, sin que viniera break o return")
                                        elif isinstance(respuesta, Break):
                                            instancia_break = respuesta
                                            vino_break = True
                                            break
                                        elif isinstance(respuesta, Return):
                                            instancia_return = respuesta
                                            vino_return = True
                                            break

                                if vino_break:    
                                    if err_semanticos_internos:
                                        # Vinieron errores y un break
                                        retorno = {"errores": err_semanticos_internos, "break": instancia_break}
                                        return retorno
                                    # Vino solo un break
                                    return instancia_break
                                if vino_return:
                                    if err_semanticos_internos:
                                        # Vinieron errores y un return
                                        retorno = {"errores": err_semanticos_internos, "return": instancia_return}
                                        return retorno
                                    # Vino solo un return
                                    return instancia_return
                                if err_semanticos_internos:
                                    # No vino break ni return, vinieron errores
                                    return err_semanticos_internos
                                # No vino break ni return ni errores
                                break
                        else:
                            return Error("Error semántico", "La condición del Else If debe ser de tipo booleano.", self.fila, self.columna)
                    
                    # ELSE
                    if not cumplio_condicion and self.lista_instr_else is not None:
                        nuevo_entorno = Entorno(entorno)
                        for accion in self.lista_instr_else:
                            if accion is not None and not isinstance(accion, Error):
                                respuesta = accion.ejecutar(nuevo_entorno)
                                if isinstance(respuesta, Error):
                                    err_semanticos_internos.append(respuesta)
                                elif isinstance(respuesta, list):
                                    err_semanticos_internos.extend(respuesta)
                                elif isinstance(respuesta, dict):
                                    # Si viene dict, se detectaron errores semánticos y un break o un return
                                    err_semanticos_internos.extend(respuesta.get("errores"))
                                    instancia_break = respuesta.get("break")
                                    if instancia_break:
                                        vino_break = True
                                        break
                                    instancia_return = respuesta.get("return")
                                    if instancia_return:
                                        vino_return = True
                                        break
                                    print("Error que no debería de pasar, se detectó un dict en if, sin que viniera break o return")
                                elif isinstance(respuesta, Break):
                                    instancia_break = respuesta
                                    vino_break = True
                                    break
                                elif isinstance(respuesta, Return):
                                    instancia_return = respuesta
                                    vino_return = True
                                    break

                        if vino_break:    
                            if err_semanticos_internos:
                                # Vinieron errores y un break
                                retorno = {"errores": err_semanticos_internos, "break": instancia_break}
                                return retorno
                            # Vino solo un break
                            return instancia_break
                        if vino_return:
                            if err_semanticos_internos:
                                # Vinieron errores y un return
                                retorno = {"errores": err_semanticos_internos, "return": instancia_return}
                                return retorno
                            # Vino solo un return
                            return instancia_return
                        if err_semanticos_internos:
                            # No vino break ni return, vinieron errores
                            return err_semanticos_internos
                        # No vino break ni return ni errores

                elif self.lista_instr_else is not None:
                    # ELSE
                    nuevo_entorno = Entorno(entorno)
                    for accion in self.lista_instr_else:
                        if accion is not None and not isinstance(accion, Error):
                            respuesta = accion.ejecutar(nuevo_entorno)
                            if isinstance(respuesta, Error):
                                err_semanticos_internos.append(respuesta)
                            elif isinstance(respuesta, list):
                                err_semanticos_internos.extend(respuesta)
                            elif isinstance(respuesta, dict):
                                # Si viene dict, se detectaron errores semánticos y un break o un return
                                err_semanticos_internos.extend(respuesta.get("errores"))
                                instancia_break = respuesta.get("break")
                                if instancia_break:
                                    vino_break = True
                                    break
                                instancia_return = respuesta.get("return")
                                if instancia_return:
                                    vino_return = True
                                    break
                                print("Error que no debería de pasar, se detectó un dict en if, sin que viniera break o return")
                            elif isinstance(respuesta, Break):
                                instancia_break = respuesta
                                vino_break = True
                                break
                            elif isinstance(respuesta, Return):
                                instancia_return = respuesta
                                vino_return = True
                                break

                    if vino_break:
                        if err_semanticos_internos:
                            # Vinieron errores y un break
                            retorno = {"errores": err_semanticos_internos, "break": instancia_break}
                            return retorno
                        # Vino solo un break
                        return instancia_break
                    if vino_return:
                        if err_semanticos_internos:
                            # Vinieron errores y un return
                            retorno = {"errores": err_semanticos_internos, "return": instancia_return}
                            return retorno
                        # Vino solo un return
                        return instancia_return
                    if err_semanticos_internos:
                        # No vino break ni return, vinieron errores
                        return err_semanticos_internos
                    # No vino break ni return ni errores
        else:
            return Error("Error semántico", "La condición del If debe ser de tipo booleano.", self.fila, self.columna)