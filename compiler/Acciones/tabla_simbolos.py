if __name__ == "compiler.Acciones.tabla_simbolos":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Entorno.entorno import Entorno
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Entorno.entorno import Entorno

class Tabla_Simbolos(Accion):
    def __init__(self, fila, columna, expresion = None, nombre_reporte_global = ""):
        super().__init__(fila, columna)
        self.expresion = expresion
        self.nombre_reporte_global = nombre_reporte_global
    
    def ejecutar(self, entorno: Entorno):
        if self.nombre_reporte_global == "":
            valor = self.expresion.ejecutar(entorno)
            if isinstance(valor, str):
                if __name__ == "compiler.Acciones.tabla_simbolos":
                    self.reporte_html(valor, entorno)
                else:
                    print("**************" + valor + "*****************")
                    print(entorno.tabla_simbolos)
            elif isinstance(valor, Error):
                return valor                    
            else:
                return Error("Error semántico", "El parámetro de la función tablaSimbolos debe ser una cadena.", self.fila, self.columna)
        else:
            return self.reporte_html(self.nombre_reporte_global, entorno)

    def reporte_html(self, nombre, entorno: Entorno):
        html = f'''<!DOCTYPE html>
        <html lang="es">
        <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="reporte.css" type="text/css" />
        <title>{nombre}</title>
        </head>
        <body>
        <header>
            <li><span class="material-icons md-dark md-100">summarize</span></li>
            <h1>{nombre}</h1>
        </header>
        <div class="container">
            <h2 style="color: white;"><b>Variables y arreglos</b></h2>
            <div class="tabla-reporte">
                <table class="table table-striped table-hover">
                    <thead style="background-color: black; color: white;">
                        <tr>\n'''
        encabezados = ['Número', 'Tipo', 'Nombre', 'Valor', 'Línea (última modificación)']
        for title in encabezados:
            html += f'<th scope="col">{title}</th>\n'
        html += '''</tr>\n</thead>\n<tbody>\n'''
        id_fila = ""
        simbolo_agregado = 0
        try:
            for clave in entorno.tabla_simbolos:
                simbolo_agregado += 1
                id_fila = "uno" if simbolo_agregado % 2 == 1 else "dos"
                html += f'''<tr id="{id_fila}">
                <td>{simbolo_agregado}</td>
                <td>{entorno.tabla_simbolos[clave].tipo}</td>
                <td>{entorno.tabla_simbolos[clave].nombre}</td>
                <td>{entorno.tabla_simbolos[clave].valor}</td>
                <td>{entorno.tabla_simbolos[clave].linea}</td>
                </tr>\n'''
            if entorno.entorno_for:
                for clave in entorno.anterior.tabla_simbolos:
                    simbolo_agregado += 1
                    id_fila = "uno" if simbolo_agregado % 2 == 1 else "dos"
                    html += f'''<tr id="{id_fila}">
                    <td>{simbolo_agregado}</td>
                    <td>{entorno.anterior.tabla_simbolos[clave].tipo}</td>
                    <td>{entorno.anterior.tabla_simbolos[clave].nombre}</td>
                    <td>{entorno.anterior.tabla_simbolos[clave].valor}</td>
                    <td>{entorno.anterior.tabla_simbolos[clave].linea}</td>
                    </tr>\n'''
        except Exception as e:
            print(e)
            return False
        html += '''</tbody>
                </table>
            </div>
        </div>

        <div class="container">
            <h2 style="color: white;"><b>Funciones</b></h2>
            <div class="tabla-reporte">
                <table class="table table-striped table-hover">
                    <thead style="background-color: black; color: white;">
                        <tr>\n'''
        encabezados = ['Número', 'Tipo de retorno', 'Nombre', 'Parámetros que recibe', 'Línea']
        for title in encabezados:
            html += f'<th scope="col">{title}</th>\n'
        html += '''</tr>\n</thead>\n<tbody>\n'''
        id_fila = ""
        simbolo_agregado = 0
        try:
            for clave in entorno.tabla_simbolos_funciones:
                str_parametros = ""
                if entorno.tabla_simbolos_funciones[clave].parametros is not None:
                    for parametro in entorno.tabla_simbolos_funciones[clave].parametros:
                        if str_parametros == "":
                            str_parametros += f"{parametro.tipo} "
                        else:
                            str_parametros += f", {parametro.tipo} "
                        if parametro.n_dimensiones is not None:
                            for i in range(parametro.n_dimensiones):
                                str_parametros += "[]"
                        str_parametros += f" {parametro.identificador}"
                simbolo_agregado += 1
                id_fila = "uno" if simbolo_agregado % 2 == 1 else "dos"
                html += f'''<tr id="{id_fila}">
                <td>{simbolo_agregado}</td>
                <td>{entorno.tabla_simbolos_funciones[clave].tipo_retorno}</td>
                <td>{entorno.tabla_simbolos_funciones[clave].nombre}</td>
                <td>{str_parametros}</td>
                <td>{entorno.tabla_simbolos_funciones[clave].linea}</td>
                </tr>\n'''
        except Exception as e:
            print(e)
            return False
        html += '''</tbody>
                </table>
            </div>
        </div>
        
        <footer>
            <p>Elías Abraham Vasquez Soto - 201900131</p>
            <p>Proyecto - Laboratorio Organización de lenguajes y compiladores 1 N</p>        
            <img src="images/logo_usac.png" width="220" height="60"/>
        </footer>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        </body>
        </html>'''
        css = '''* {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: 'Lato', sans-serif;
        }

        html {
            min-height: 100%;
            position: relative;
        }

        body {
            background-color:rgb(236, 239, 243);
            padding-top: 20px;
            margin-bottom: 150px;
        }

        /* ===== Iconos de Google ===== */
        /* Rules for sizing the icon. */
        .material-icons.md-24 { font-size: 24px; }
        .material-icons.md-30 { font-size: 30px; }
        .material-icons.md-100 { font-size: 100px; }
        /* Rules for using icons as black on a light background. */
        .material-icons.md-dark { color: rgba(0, 0, 0, 0.54); }
        .material-icons.md-dark.md-inactive { color: rgba(0, 0, 0, 0.26); }
        /* Rules for using icons as white on a dark background. */
        .material-icons.md-light { color: rgba(255, 255, 255, 1); }
        .material-icons.md-light.md-inactive { color: rgba(255, 255, 255, 0.3); }

        header {
            display: flex;
            justify-content: center;
            position: relative;
        }
        header h1 {
            font-size: 75px;
        }

        .container {
            background-color: rgb(6, 80, 99);
            padding-top: 20px;
            padding-bottom: 20px;
            padding-left: 50px;
            margin: 30px 100px 30px 100px;
        }

        .tabla-reporte {
            padding-top: 20px;
            padding-left: 20px;
            padding-right: 40px;
            text-align: center;
            font-size: 20px;
            letter-spacing: 1px;
            overflow-x: scroll;
        }

        table {
            width: 100%;
            table-layout: fixed;
        }

        table thead{    
            color: white;
        }

        table td, th {
            width: 220px;
            vertical-align: middle;
        }

        #uno {
            background-color: rgb(221, 202, 177);
        }

        #dos {
            background-color: rgb(238, 236, 231);
        }

        footer {
            color: white;
            line-height: 10px;
            text-align: center;
            padding-top: 20px;
            padding-bottom: 5px;
            font-size: 15px;
            position: absolute;
            bottom: 0;
            width: 100%;
            background-image: url("images/footer.png");
        }'''
        try:
            reporte = open(f"Tablas de simbolos/{nombre}.html", "w",encoding="utf8")
            reporte.write(html)
            reporte.close()
            print("->HTML Tabla generado")
            css_reporte = open("Tablas de simbolos/reporte.css", "w",encoding="utf8")
            css_reporte.write(css)
            css_reporte.close()
            print("->CSS Tabla generado")
            return True
        except Exception as e:
            print(e)
            return False