if __name__ == "compiler.Acciones.inc_dec":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Entorno.entorno import Entorno
    from compiler.Entorno.simbolo import Simbolo
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Entorno.entorno import Entorno
    from Entorno.simbolo import Simbolo

class Inc_Dec(Accion):
    def __init__(self, fila, columna, identificador, operacion):
        super().__init__(fila, columna)
        self.identificador = identificador
        self.operacion = operacion
        self.tipo = None
    
    def ejecutar(self, entorno: Entorno):
        variable: Simbolo = entorno.buscar_simbolo(self.identificador)

        if variable is not None:
            if variable.tipo == 'int' or variable.tipo == 'double':
                if self.operacion == "++":
                    self.tipo = variable.tipo
                    entorno.editar_simbolo(variable.nombre, Simbolo(variable.tipo, variable.nombre, variable.valor + 1, variable.linea))
                elif self.operacion == "--":
                    self.tipo = variable.tipo
                    entorno.editar_simbolo(variable.nombre, Simbolo(variable.tipo, variable.nombre, variable.valor - 1, variable.linea))
            else:
                return Error("Error semántico", "Solo se pueden incrementar/decrementar valores enteros y double.", self.fila, self.columna)
        else:
            return Error("Error semántico", "Al incrementar/decrementar una variable, no se encontró el identificador en el entorno actual, ni en los padre.", self.fila, self.columna)