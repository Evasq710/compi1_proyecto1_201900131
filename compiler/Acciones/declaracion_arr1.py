if __name__ == "compiler.Acciones.declaracion_arr1":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Entorno.entorno import Entorno
    from compiler.Entorno.simbolo import Simbolo
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Entorno.entorno import Entorno
    from Entorno.simbolo import Simbolo

class Declaracion_Array1(Accion):
    def __init__(self, fila, columna, tipo, n_dimensiones : int, identificador, confirmacion_tipo, size_dimensiones : list):
        super().__init__(fila, columna)
        self.tipo = tipo
        self.n_dimensiones = n_dimensiones
        self.identificador = identificador
        self.confirmacion_tipo = confirmacion_tipo
        self.size_dimensiones = size_dimensiones
    
    def ejecutar(self, entorno: Entorno):
        # Lista que contendrá todos los errores semánticos encontrados, antes de crear el arreglo
        err_semanticos = []

        if self.tipo != self.confirmacion_tipo:
            err_semanticos.append(Error("Error semántico", "En la declaración de un arreglo, no coinciden los tipos indicados.", self.fila, self.columna))
        if self.n_dimensiones != len(self.size_dimensiones):
            err_semanticos.append(Error("Error semántico", "En la declaración de un arreglo, no coinciden la cantidad de dimensiones del arreglo con las dimensiones a las que se les asigna un tamaño.", self.fila, self.columna))
        
        valores_size_dimensiones = []
        for expresion in self.size_dimensiones:
            size_dimension = expresion.ejecutar(entorno)
            if isinstance(size_dimension, Error):
                err_semanticos.append(size_dimension)
            elif expresion.tipo != "int":
                err_semanticos.append(Error("Error semántico", f"En la declaración de un arreglo, se detectó una expresión de tipo {expresion.tipo} en la asignación de tamaños de las dimensiones. Solo se permiten enteros.", self.fila, self.columna))
            elif not isinstance(size_dimension, int):
                err_semanticos.append(Error("Error semántico", f"En la declaración de un arreglo, se detectó una expresión de tipo 'int', que devolvió un valor de tipo {type(size_dimension)} (error interno).", self.fila, self.columna))
                print(f"Error que no debería pasar: se detectó una expresión de tipo 'int', que devolvió un valor de tipo {type(size_dimension)} en declaración de arreglo.")
            else:
                valores_size_dimensiones.append(size_dimension)
                
        if err_semanticos:
            return err_semanticos
        
        array = self.crear_dimensiones(valores_size_dimensiones.copy())
        simbolo = Simbolo(tipo="array", nombre=self.identificador, valor=array, linea=self.fila, tipo_datos_array=self.tipo, num_dimensiones=self.n_dimensiones)
        insertado = entorno.insertar_simbolo(simbolo)

        if not insertado:
            # Ya existe el símbolo en el entorno, no se inserta
            return Error("Error semántico", "No se puede declarar el arreglo, su identificador ya existe en el entorno.", self.fila, self.columna)

    
    def crear_dimensiones(self, valores_size_dimensiones: list):
        # En la última iteración recursiva, valores_size_dimensiones estará vacío, retornando los valores correspondientes, iterando sobre el tamaño de la última dimensión
        # Esto asegura que la última dimensión tenga valores de tipo primitivo (enteros, double, etc)
        if len(valores_size_dimensiones) == 0:
            if self.tipo == "int":
                return 0
            elif self.tipo == "double":
                return 0.0
            elif self.tipo == "boolean":
                return False
            elif self.tipo == "char" or self.tipo == "string":
                return ""
            else:
                print(f"Error que no debería de pasar, en la declaración de arreglos, se declaró tipo {self.tipo}, siendo este no primitivo.")
        
        arreglo_actual = []
        new_size = valores_size_dimensiones.pop(0)
        for i in range(new_size):
            arreglo_actual.append(self.crear_dimensiones(valores_size_dimensiones.copy()))
        # Arreglo de valores cuando new_size es la última dimensión
        # Arreglo de arreglos anteriores, según los tamaños de las dimensiones
        # EJEMPLO: [3][2], el arreglo de dos posiciones, se retornará 3 veces, dentro de arreglo_actual
        return arreglo_actual
