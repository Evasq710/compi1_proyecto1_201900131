if __name__ == "compiler.Acciones.ciclo_for":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Acciones.instr_break import Break
    from compiler.Acciones.instr_return import Return
    from compiler.Entorno.entorno import Entorno
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Acciones.instr_break import Break
    from Acciones.instr_return import Return
    from Entorno.entorno import Entorno

class For(Accion):
    def __init__(self, fila, columna, expresion_inicio, condicion, expresion_inc_dec, listado_instrucciones):
        super().__init__(fila, columna)
        self.expresion_inicio = expresion_inicio
        self.condicion = condicion
        self.expresion_inc_dec = expresion_inc_dec
        self.listado_instrucciones = listado_instrucciones
    
    def ejecutar(self, entorno: Entorno):
        entorno_condicion_for = Entorno(entorno)

        inicializacion = self.expresion_inicio.ejecutar(entorno_condicion_for)
        if isinstance(inicializacion, Error):
            return inicializacion

        condicion = self.condicion.ejecutar(entorno_condicion_for)
        if isinstance(condicion, Error):
            return condicion
        
        if condicion is True or condicion is False:
            err_semanticos_internos = []
            vino_break = False
            vino_return = False
            instancia_return = None

            while condicion:
                nuevo_entorno = Entorno(entorno_condicion_for)
                nuevo_entorno.entorno_for = True
                for accion in self.listado_instrucciones:
                    if accion is not None and not isinstance(accion, Error):
                        respuesta = accion.ejecutar(nuevo_entorno)
                        if isinstance(respuesta, Error):
                            err_semanticos_internos.append(respuesta)
                        elif isinstance(respuesta, list):
                            err_semanticos_internos.extend(respuesta)
                        elif isinstance(respuesta, dict):
                            # Si viene dict, se detectaron errores semánticos y un break o un return
                            err_semanticos_internos.extend(respuesta.get("errores"))
                            if respuesta.get("break"):
                                vino_break = True
                                break
                            instancia_return = respuesta.get("return")
                            if instancia_return:
                                vino_return = True
                                break
                            print("Error que no debería de pasar, se detectó un dict en for, sin que viniera break o return")
                        elif isinstance(respuesta, Break):
                            vino_break = True
                            break
                        elif isinstance(respuesta, Return):
                            instancia_return = respuesta
                            vino_return = True
                            break

                if not vino_break or not vino_return:
                    inc_dec = self.expresion_inc_dec.ejecutar(entorno_condicion_for)
                    if isinstance(inc_dec, Error):
                        err_semanticos_internos.append(inc_dec)
                        return err_semanticos_internos

                    condicion = self.condicion.ejecutar(entorno_condicion_for)
                elif vino_return:
                    if err_semanticos_internos:
                        # Vinieron errores y un return
                        retorno = {"errores": err_semanticos_internos, "return": instancia_return}
                        return retorno
                    # Vino solo un return
                    return instancia_return
                else:
                    break

            if err_semanticos_internos:
                return err_semanticos_internos
        else:
            return Error("Error semántico", "La condición del For debe ser de tipo booleano.", self.fila, self.columna)