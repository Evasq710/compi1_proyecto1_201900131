class Parametro():
    def __init__(self, fila, columna, tipo, identificador, n_dimensiones : int = None):
        self.fila = fila
        self.columna = columna
        self.tipo = tipo
        self.identificador = identificador
        self.n_dimensiones = n_dimensiones