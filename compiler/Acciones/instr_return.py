if __name__ == "compiler.Acciones.instr_return":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Entorno.entorno import Entorno
else:
    from Acciones.Interface.Accion import Accion
    from Entorno.entorno import Entorno

class Return(Accion):
    def __init__(self, fila, columna, expresion):
        super().__init__(fila, columna)
        self.expresion = expresion
    
    def ejecutar(self, entorno: Entorno):
        self.entorno_return = entorno
        return self