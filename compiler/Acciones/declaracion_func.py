if __name__ == "compiler.Acciones.declaracion_func":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.instr_return import Return
    from compiler.Acciones.Interface.Error import Error
    from compiler.Entorno.entorno import Entorno
    from compiler.Entorno.simbolo import Simbolo
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.instr_return import Return
    from Acciones.Interface.Error import Error
    from Entorno.entorno import Entorno
    from Entorno.simbolo import Simbolo

class Declaracion_Func(Accion):
    def __init__(self, fila, columna, tipo, identificador, lista_instrucciones, lista_parametros = None):
        super().__init__(fila, columna)
        self.tipo = tipo
        self.identificador = identificador
        self.lista_instrucciones = lista_instrucciones
        self.lista_parametros = lista_parametros
    
    def ejecutar(self, entorno: Entorno):
        vino_return = False
        for instruccion in self.lista_instrucciones:
            if isinstance(instruccion, Return):
                vino_return = True
                break
        
        if self.tipo == "void" and vino_return:
            return Error("Error semántico", "En la declaración de una función, se declaró de tipo 'void', pero se encontró un return.", self.fila, self.columna)
        
        if self.tipo != "void" and not vino_return:
            return Error("Error semántico", f"En la declaración de una función, se declaró de tipo '{self.tipo}', pero no se encontró un return.", self.fila, self.columna)
        
        simbolo_funcion = Simbolo(tipo="funcion", nombre=self.identificador, valor=self.lista_instrucciones, linea=self.fila, tipo_retorno=self.tipo, parametros=self.lista_parametros)
        insertado = entorno.insertar_funcion(simbolo_funcion)
        if not insertado:
            # Ya existe el símbolo en el entorno, no se inserta
            return Error("Error semántico", f"No se puede declarar la función, esta ya existe en el entorno.", self.fila, self.columna)