if __name__ == "compiler.Acciones.declaracion_arr2":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Entorno.entorno import Entorno
    from compiler.Entorno.simbolo import Simbolo
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Entorno.entorno import Entorno
    from Entorno.simbolo import Simbolo

class Declaracion_Array2(Accion):
    def __init__(self, fila, columna, tipo, n_dimensiones : int, identificador, arreglo: list):
        super().__init__(fila, columna)
        self.tipo = tipo
        self.n_dimensiones = n_dimensiones
        self.identificador = identificador
        self.arreglo = arreglo
    
    def ejecutar(self, entorno: Entorno):
        err_semanticos = []

        dimensiones = self.comprobar_n_dimensiones(self.arreglo.copy(), 0)
        if dimensiones != self.n_dimensiones:
            err_semanticos.append(Error("Error semántico", f"En la declaración de un arreglo por lista, no coinciden la cantidad de dimensiones declaradas ({self.n_dimensiones}) con las de la lista asignada ({dimensiones}).", self.fila, self.columna))
        
        size_array = self.comprobar_size_dimensiones(self.arreglo.copy())
        if isinstance(size_array, Error):
            err_semanticos.append(size_array)
        if isinstance(size_array, list):
            for size in size_array:
                if size != size_array[0]:
                    err_semanticos.append(Error("Error semántico", "En la declaración de un arreglo por lista, se encontraron variaciones en la cantidad de elementos declarados en la última dimensión.", self.fila, self.columna))
        
        array_lleno = self.crear_arreglo(self.arreglo.copy(), entorno)
        if isinstance(array_lleno, Error):
            err_semanticos.append(array_lleno)
        
        if err_semanticos:
            return err_semanticos
        
        simbolo = Simbolo(tipo="array", nombre=self.identificador, valor=array_lleno, linea=self.fila, tipo_datos_array=self.tipo, num_dimensiones=self.n_dimensiones)
        insertado = entorno.insertar_simbolo(simbolo)

        if not insertado:
            # Ya existe el símbolo en el entorno, no se inserta
            return Error("Error semántico", "No se puede declarar el arreglo por lista, su identificador ya existe en el entorno.", self.fila, self.columna)


    def comprobar_n_dimensiones(self, arreglo, dimensiones):
        dimensiones += 1
        if isinstance(arreglo[0], list):
            return self.comprobar_n_dimensiones(arreglo[0], dimensiones)
        return dimensiones


    def comprobar_size_dimensiones(self, arreglo):
        size_array = []
        if isinstance(arreglo[0], list):
            for dimension in arreglo:
                # Comprobación de concordancia entre tamaños de arreglos
                if len(dimension) != len(arreglo[0]):
                    return Error("Error semántico", "En la declaración de un arreglo por lista, difieren las longitudes de los arreglos en la lista.", self.fila, self.columna)
                
                size = self.comprobar_size_dimensiones(dimension)
                if isinstance(size, Error):
                    return size
                elif isinstance(size, list):
                    size_array.extend(size)
                else:
                    size_array.append(size)

            return size_array

        # En este punto, el arreglo del parámetro es un arreglo de expresiones, retorno de la cantidad de expresiones encontradas
        return len(arreglo)
    

    def crear_arreglo(self, arreglo, entorno):
        array = []
        if isinstance(arreglo[0], list):
            for dimension in arreglo:
                if not isinstance(dimension, list):
                    # Error de cruce entre listas y expresiones en las posiciones del arreglo
                    return Error("Error semántico", "En la declaración de un arreglo por lista, el tipo de los elementos debe ser el mismo para todas las posiciones correspondientes (tipo lista o expresión).", self.fila, self.columna)
                respuesta = self.crear_arreglo(dimension, entorno)
                if isinstance(respuesta, Error):
                    return respuesta

                array.append(respuesta)
                    
            return array

        # En este punto, el arreglo del parámetro es un arreglo de expresiones
        array_valores = []
        for expresion in arreglo:
            if isinstance(expresion, list):
                # Error de cruce entre listas y expresiones en las posiciones del arreglo
                return Error("Error semántico", "En la declaración de un arreglo por lista, el tipo de los elementos debe ser el mismo para todas las posiciones correspondientes (tipo lista o expresión).", self.fila, self.columna)
            
            valor = expresion.ejecutar(entorno)
            if isinstance(valor, Error):
                # Error semántico en la ejecución de la expresión
                return valor
            
            if expresion.tipo != self.tipo:
                # Error semántico en los tipos
                return Error("Error semántico", f"En la declaración de un arreglo por lista, se encontraron valores de tipo diferente al {self.tipo}, declarado en el arreglo.", self.fila, self.columna)
            
            array_valores.append(valor)
        
        return array_valores
        