if __name__ == "compiler.Acciones.asignacion":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Entorno.entorno import Entorno
    from compiler.Entorno.simbolo import Simbolo
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Entorno.entorno import Entorno
    from Entorno.simbolo import Simbolo

class Asignacion(Accion):
    def __init__(self, fila, columna, identificador, expresion):
        super().__init__(fila, columna)
        self.identificador = identificador
        self.expresion = expresion
    
    def ejecutar(self, entorno: Entorno):
        valor = self.expresion.ejecutar(entorno)
        if isinstance(valor, Error):
            return valor
            
        variable: Simbolo = entorno.buscar_simbolo(str(self.identificador).lower())
        if variable is not None:
            if self.expresion.tipo == 'null':
                # Reasignación del valor de la variabe a null
                entorno.editar_simbolo(str(self.identificador).lower(), Simbolo(variable.tipo, str(self.identificador).lower(), 'null', self.fila))
            elif variable.tipo == 'int':
                if self.expresion.tipo == 'int':
                    entorno.editar_simbolo(str(self.identificador).lower(), Simbolo(variable.tipo, str(self.identificador).lower(), valor, self.fila))
                else:
                    return Error("Error semántico", "La asignación de la variable de tipo 'int' no concuerda con el tipo de la expresión dada.", self.fila, self.columna)
            elif variable.tipo == 'double':
                if self.expresion.tipo == 'double':
                    entorno.editar_simbolo(str(self.identificador).lower(), Simbolo(variable.tipo, str(self.identificador).lower(), valor, self.fila))
                else:
                    return Error("Error semántico", "La asignación de la variable de tipo 'double' no concuerda con el tipo de la expresión dada.", self.fila, self.columna)
            elif variable.tipo == 'boolean':
                if self.expresion.tipo == 'boolean':
                    entorno.editar_simbolo(str(self.identificador).lower(), Simbolo(variable.tipo, str(self.identificador).lower(), valor, self.fila))
                elif self.expresion.tipo == 'int' and (valor == 0 or valor == 1):
                    valor = bool(valor)
                    entorno.editar_simbolo(str(self.identificador).lower(), Simbolo(variable.tipo, str(self.identificador).lower(), valor, self.fila))
                else:
                    return Error("Error semántico", "La asignación de la variable de tipo 'boolean' no concuerda con el tipo de la expresión dada.", self.fila, self.columna)
            elif variable.tipo == 'char':
                if self.expresion.tipo == 'char':
                    entorno.editar_simbolo(str(self.identificador).lower(), Simbolo(variable.tipo, str(self.identificador).lower(), valor, self.fila))
                else:
                    return Error("Error semántico", "La asignación de la variable de tipo 'char' no concuerda con el tipo de la expresión dada.", self.fila, self.columna)
            elif variable.tipo == 'string':
                if self.expresion.tipo == 'string':
                    entorno.editar_simbolo(str(self.identificador).lower(), Simbolo(variable.tipo, str(self.identificador).lower(), valor, self.fila))
                else:
                    return Error("Error semántico", "La asignación de la variable de tipo 'string' no concuerda con el tipo de la expresión dada.", self.fila, self.columna)
        else:
            return Error("Error semántico", "Al asignar un valor a una variable, no se encontró el identificador en el entorno actual, ni en los padre.", self.fila, self.columna)