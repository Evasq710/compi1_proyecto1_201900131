if __name__ == "compiler.Acciones.imprimir":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Entorno.entorno import Entorno
    from tkinter import END
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Entorno.entorno import Entorno

class Imprimir(Accion):
    def __init__(self, fila, columna, expresion, txtbox_console):
        super().__init__(fila, columna)
        self.expresion = expresion
        self.txtbox_console = txtbox_console
    
    def ejecutar(self, entorno: Entorno):
        valor = self.expresion.ejecutar(entorno)
        if isinstance(valor, Error):
            return valor

        if valor is not True or valor is not False:
            if __name__ == "compiler.Acciones.imprimir":
                self.txtbox_console.insert(END, f"> {valor}\n")
            else:
                print(valor)
        else:
            if __name__ == "compiler.Acciones.imprimir":
                self.txtbox_console.insert(END, f"> {str(valor).lower()}\n")
            else:
                print(str(valor).lower())
