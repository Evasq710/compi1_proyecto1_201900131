if __name__ == "compiler.Acciones.switch":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Acciones.instr_break import Break
    from compiler.Acciones.instr_return import Return
    from compiler.Entorno.entorno import Entorno
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Acciones.instr_break import Break
    from Acciones.instr_return import Return
    from Entorno.entorno import Entorno

class Switch(Accion):
    def __init__(self, fila, columna, expr_switch, lista_cases = None, default = None):
        super().__init__(fila, columna)
        self.expr_switch = expr_switch
        # Pos 0 -> expr del case, pos 1 -> lista instrucciones del case
        self.lista_cases = lista_cases
        # lista de instrucciones
        self.default = default
    
    def ejecutar(self, entorno: Entorno):
        valor_switch = self.expr_switch.ejecutar(entorno)
        if isinstance(valor_switch, Error):
            return valor_switch
        
        if self.expr_switch.tipo == 'int' or self.expr_switch.tipo == 'double' or self.expr_switch.tipo == 'boolean' or self.expr_switch.tipo == 'char' or self.expr_switch.tipo == 'string':
            
            if self.lista_cases is not None:

                case_ejecutado = False
                vino_break = False
                vino_return = False
                instancia_return = None
                err_semanticos_internos = []

                for case in self.lista_cases:

                    if not case_ejecutado:
                        valor_case = case[0].ejecutar(entorno)
                        if isinstance(valor_case, Error):
                            return valor_case

                        if self.expr_switch.tipo == 'int':
                            if case[0].tipo == 'int' or case[0].tipo == 'double':
                                if valor_switch == valor_case:
                                    # Coinciden valores de case y switch
                                    case_ejecutado = True
                                    nuevo_entorno = Entorno(entorno)
                                    for accion in case[1]:
                                        if accion is not None and not isinstance(accion, Error):
                                            respuesta = accion.ejecutar(nuevo_entorno)
                                            if isinstance(respuesta, Error):
                                                err_semanticos_internos.append(respuesta)
                                            elif isinstance(respuesta, list):
                                                err_semanticos_internos.extend(respuesta)
                                            elif isinstance(respuesta, dict):
                                                # Si viene dict, se detectaron errores semánticos y un break o un return
                                                err_semanticos_internos.extend(respuesta.get("errores"))
                                                if respuesta.get("break"):
                                                    vino_break = True
                                                    break
                                                instancia_return = respuesta.get("return")
                                                if instancia_return:
                                                    vino_return = True
                                                    break
                                                print("Error que no debería de pasar, se detectó un dict en swich, sin que viniera break o return")
                                            elif isinstance(respuesta, Break):
                                                vino_break = True
                                                break
                                            elif isinstance(respuesta, Return):
                                                instancia_return = respuesta
                                                vino_return = True
                                                break
                            elif case[0].tipo == 'string':
                                if str(valor_switch) == valor_case:
                                    # Coinciden valores de case y switch
                                    case_ejecutado = True
                                    nuevo_entorno = Entorno(entorno)
                                    for accion in case[1]:
                                        if accion is not None and not isinstance(accion, Error):
                                            respuesta = accion.ejecutar(nuevo_entorno)
                                            if isinstance(respuesta, Error):
                                                err_semanticos_internos.append(respuesta)
                                            elif isinstance(respuesta, list):
                                                err_semanticos_internos.extend(respuesta)
                                            elif isinstance(respuesta, dict):
                                                # Si viene dict, se detectaron errores semánticos y un break o un return
                                                err_semanticos_internos.extend(respuesta.get("errores"))
                                                if respuesta.get("break"):
                                                    vino_break = True
                                                    break
                                                instancia_return = respuesta.get("return")
                                                if instancia_return:
                                                    vino_return = True
                                                    break
                                                print("Error que no debería de pasar, se detectó un dict en switch, sin que viniera break o return")
                                            elif isinstance(respuesta, Break):
                                                vino_break = True
                                                break
                                            elif isinstance(respuesta, Return):
                                                instancia_return = respuesta
                                                vino_return = True
                                                break
                            else:
                                return Error("Error semántico", "Error de comparación entre switch y case, solo se pueden comparar enteros con enteros, double o string.", self.fila, self.columna)
                        elif self.expr_switch.tipo == 'double':
                            if case[0].tipo == 'int' or case[0].tipo == 'double':
                                if valor_switch == valor_case:
                                    # Coinciden valores de case y switch
                                    case_ejecutado = True
                                    nuevo_entorno = Entorno(entorno)
                                    for accion in case[1]:
                                        if accion is not None and not isinstance(accion, Error):
                                            respuesta = accion.ejecutar(nuevo_entorno)
                                            if isinstance(respuesta, Error):
                                                err_semanticos_internos.append(respuesta)
                                            elif isinstance(respuesta, list):
                                                err_semanticos_internos.extend(respuesta)
                                            elif isinstance(respuesta, dict):
                                                # Si viene dict, se detectaron errores semánticos y un break o un return
                                                err_semanticos_internos.extend(respuesta.get("errores"))
                                                if respuesta.get("break"):
                                                    vino_break = True
                                                    break
                                                instancia_return = respuesta.get("return")
                                                if instancia_return:
                                                    vino_return = True
                                                    break
                                                print("Error que no debería de pasar, se detectó un dict en switch, sin que viniera break o return")
                                            elif isinstance(respuesta, Break):
                                                vino_break = True
                                                break
                                            elif isinstance(respuesta, Return):
                                                instancia_return = respuesta
                                                vino_return = True
                                                break
                            elif case[0].tipo == 'string':
                                if str(valor_switch) == valor_case:
                                    # Coinciden valores de case y switch
                                    case_ejecutado = True
                                    nuevo_entorno = Entorno(entorno)
                                    for accion in case[1]:
                                        if accion is not None and not isinstance(accion, Error):
                                            respuesta = accion.ejecutar(nuevo_entorno)
                                            if isinstance(respuesta, Error):
                                                err_semanticos_internos.append(respuesta)
                                            elif isinstance(respuesta, list):
                                                err_semanticos_internos.extend(respuesta)
                                            elif isinstance(respuesta, dict):
                                                # Si viene dict, se detectaron errores semánticos y un break o un return
                                                err_semanticos_internos.extend(respuesta.get("errores"))
                                                if respuesta.get("break"):
                                                    vino_break = True
                                                    break
                                                instancia_return = respuesta.get("return")
                                                if instancia_return:
                                                    vino_return = True
                                                    break
                                                print("Error que no debería de pasar, se detectó un dict en switch, sin que viniera break o return")
                                            elif isinstance(respuesta, Break):
                                                vino_break = True
                                                break
                                            elif isinstance(respuesta, Return):
                                                instancia_return = respuesta
                                                vino_return = True
                                                break
                            else:
                                return Error("Error semántico", "Error de comparación entre switch y case, solo se pueden comparar double con enteros, double o string.", self.fila, self.columna)
                        elif self.expr_switch.tipo == 'boolean':
                            if case[0].tipo == 'boolean':
                                if valor_switch == valor_case:
                                    # Coinciden valores de case y switch
                                    case_ejecutado = True
                                    nuevo_entorno = Entorno(entorno)
                                    for accion in case[1]:
                                        if accion is not None and not isinstance(accion, Error):
                                            respuesta = accion.ejecutar(nuevo_entorno)
                                            if isinstance(respuesta, Error):
                                                err_semanticos_internos.append(respuesta)
                                            elif isinstance(respuesta, list):
                                                err_semanticos_internos.extend(respuesta)
                                            elif isinstance(respuesta, dict):
                                                # Si viene dict, se detectaron errores semánticos y un break o un return
                                                err_semanticos_internos.extend(respuesta.get("errores"))
                                                if respuesta.get("break"):
                                                    vino_break = True
                                                    break
                                                instancia_return = respuesta.get("return")
                                                if instancia_return:
                                                    vino_return = True
                                                    break
                                                print("Error que no debería de pasar, se detectó un dict en switch, sin que viniera break o return")
                                            elif isinstance(respuesta, Break):
                                                vino_break = True
                                                break
                                            elif isinstance(respuesta, Return):
                                                instancia_return = respuesta
                                                vino_return = True
                                                break
                            elif case[0].tipo == 'string':
                                if str(valor_switch).lower() == valor_case:
                                    # Coinciden valores de case y switch
                                    case_ejecutado = True
                                    nuevo_entorno = Entorno(entorno)
                                    for accion in case[1]:
                                        if accion is not None and not isinstance(accion, Error):
                                            respuesta = accion.ejecutar(nuevo_entorno)
                                            if isinstance(respuesta, Error):
                                                err_semanticos_internos.append(respuesta)
                                            elif isinstance(respuesta, list):
                                                err_semanticos_internos.extend(respuesta)
                                            elif isinstance(respuesta, dict):
                                                # Si viene dict, se detectaron errores semánticos y un break o un return
                                                err_semanticos_internos.extend(respuesta.get("errores"))
                                                if respuesta.get("break"):
                                                    vino_break = True
                                                    break
                                                instancia_return = respuesta.get("return")
                                                if instancia_return:
                                                    vino_return = True
                                                    break
                                                print("Error que no debería de pasar, se detectó un dict en switch, sin que viniera break o return")
                                            elif isinstance(respuesta, Break):
                                                vino_break = True
                                                break
                                            elif isinstance(respuesta, Return):
                                                instancia_return = respuesta
                                                vino_return = True
                                                break
                            else:
                                return Error("Error semántico", "Error de comparación entre switch y case, solo se pueden comparar booleanos con booleanos o string.", self.fila, self.columna)
                        elif self.expr_switch.tipo == 'char':
                            if case[0].tipo == 'char':
                                if valor_switch == valor_case:
                                    # Coinciden valores de case y switch
                                    case_ejecutado = True
                                    nuevo_entorno = Entorno(entorno)
                                    for accion in case[1]:
                                        if accion is not None and not isinstance(accion, Error):
                                            respuesta = accion.ejecutar(nuevo_entorno)
                                            if isinstance(respuesta, Error):
                                                err_semanticos_internos.append(respuesta)
                                            elif isinstance(respuesta, list):
                                                err_semanticos_internos.extend(respuesta)
                                            elif isinstance(respuesta, dict):
                                                # Si viene dict, se detectaron errores semánticos y un break o un return
                                                err_semanticos_internos.extend(respuesta.get("errores"))
                                                if respuesta.get("break"):
                                                    vino_break = True
                                                    break
                                                instancia_return = respuesta.get("return")
                                                if instancia_return:
                                                    vino_return = True
                                                    break
                                                print("Error que no debería de pasar, se detectó un dict en switch, sin que viniera break o return")
                                            elif isinstance(respuesta, Break):
                                                vino_break = True
                                                break
                                            elif isinstance(respuesta, Return):
                                                instancia_return = respuesta
                                                vino_return = True
                                                break
                            else:
                                return Error("Error semántico", "Error de comparación entre switch y case, solo se puede comparar char con char.", self.fila, self.columna)
                        elif self.expr_switch.tipo == 'string':
                            if case[0].tipo == 'int' or case[0].tipo == 'double':
                                if valor_switch == str(valor_case):
                                    # Coinciden valores de case y switch
                                    case_ejecutado = True
                                    nuevo_entorno = Entorno(entorno)
                                    for accion in case[1]:
                                        if accion is not None and not isinstance(accion, Error):
                                            respuesta = accion.ejecutar(nuevo_entorno)
                                            if isinstance(respuesta, Error):
                                                err_semanticos_internos.append(respuesta)
                                            elif isinstance(respuesta, list):
                                                err_semanticos_internos.extend(respuesta)
                                            elif isinstance(respuesta, dict):
                                                # Si viene dict, se detectaron errores semánticos y un break o un return
                                                err_semanticos_internos.extend(respuesta.get("errores"))
                                                if respuesta.get("break"):
                                                    vino_break = True
                                                    break
                                                instancia_return = respuesta.get("return")
                                                if instancia_return:
                                                    vino_return = True
                                                    break
                                                print("Error que no debería de pasar, se detectó un dict en switch, sin que viniera break o return")
                                            elif isinstance(respuesta, Break):
                                                vino_break = True
                                                break
                                            elif isinstance(respuesta, Return):
                                                instancia_return = respuesta
                                                vino_return = True
                                                break
                            elif case[0].tipo == 'boolean':
                                if valor_switch == str(valor_case).lower():
                                    # Coinciden valores de case y switch
                                    case_ejecutado = True
                                    nuevo_entorno = Entorno(entorno)
                                    for accion in case[1]:
                                        if accion is not None and not isinstance(accion, Error):
                                            respuesta = accion.ejecutar(nuevo_entorno)
                                            if isinstance(respuesta, Error):
                                                err_semanticos_internos.append(respuesta)
                                            elif isinstance(respuesta, list):
                                                err_semanticos_internos.extend(respuesta)
                                            elif isinstance(respuesta, dict):
                                                # Si viene dict, se detectaron errores semánticos y un break o un return
                                                err_semanticos_internos.extend(respuesta.get("errores"))
                                                if respuesta.get("break"):
                                                    vino_break = True
                                                    break
                                                instancia_return = respuesta.get("return")
                                                if instancia_return:
                                                    vino_return = True
                                                    break
                                                print("Error que no debería de pasar, se detectó un dict en switch, sin que viniera break o return")
                                            elif isinstance(respuesta, Break):
                                                vino_break = True
                                                break
                                            elif isinstance(respuesta, Return):
                                                instancia_return = respuesta
                                                vino_return = True
                                                break
                            elif case[0].tipo == 'string':
                                if valor_switch == valor_case:
                                    # Coinciden valores de case y switch
                                    case_ejecutado = True
                                    nuevo_entorno = Entorno(entorno)
                                    for accion in case[1]:
                                        if accion is not None and not isinstance(accion, Error):
                                            respuesta = accion.ejecutar(nuevo_entorno)
                                            if isinstance(respuesta, Error):
                                                err_semanticos_internos.append(respuesta)
                                            elif isinstance(respuesta, list):
                                                err_semanticos_internos.extend(respuesta)
                                            elif isinstance(respuesta, dict):
                                                # Si viene dict, se detectaron errores semánticos y un break o un return
                                                err_semanticos_internos.extend(respuesta.get("errores"))
                                                if respuesta.get("break"):
                                                    vino_break = True
                                                    break
                                                instancia_return = respuesta.get("return")
                                                if instancia_return:
                                                    vino_return = True
                                                    break
                                                print("Error que no debería de pasar, se detectó un dict en switch, sin que viniera break o return")
                                            elif isinstance(respuesta, Break):
                                                vino_break = True
                                                break
                                            elif isinstance(respuesta, Return):
                                                instancia_return = respuesta
                                                vino_return = True
                                                break
                            else:
                                return Error("Error semántico", "Error de comparación entre switch y case, no se pueden comparar string con char o null.", self.fila, self.columna)
                        else:
                            return Error("Error semántico", "Error de comparación entre switch y case, no se puede comparar un valor nulo.", self.fila, self.columna)
                    
                    elif not vino_break and not vino_return:
                        # Se ejecutó un case, y no vino su break, se ejecutan las acciones hasta encontrar un break
                        nuevo_entorno = Entorno(entorno)
                        for accion in case[1]:
                            if accion is not None and not isinstance(accion, Error):
                                respuesta = accion.ejecutar(nuevo_entorno)
                                if isinstance(respuesta, Error):
                                    err_semanticos_internos.append(respuesta)
                                elif isinstance(respuesta, list):
                                    err_semanticos_internos.extend(respuesta)
                                elif isinstance(respuesta, dict):
                                    # Si viene dict, se detectaron errores semánticos y un break o un return
                                    err_semanticos_internos.extend(respuesta.get("errores"))
                                    if respuesta.get("break"):
                                        vino_break = True
                                        break
                                    instancia_return = respuesta.get("return")
                                    if instancia_return:
                                        vino_return = True
                                        break
                                    print("Error que no debería de pasar, se detectó un dict en switch, sin que viniera break o return")
                                elif isinstance(respuesta, Break):
                                    vino_break = True
                                    break
                                elif isinstance(respuesta, Return):
                                    instancia_return = respuesta
                                    vino_return = True
                                    break
                    
                    elif vino_return:
                        # Se ejecutó mínimo un case, y vino return, dejar de evaluar los case
                        if err_semanticos_internos:
                            # Vinieron errores y un return
                            retorno = {"errores": err_semanticos_internos, "return": instancia_return}
                            return retorno
                        # Vino solo un return
                        return instancia_return
                    else:
                        # Se ejecutó mínimo un case, y vino break, dejar de evaluar los case
                        break

                # DEFAULT LUEGO DE CASES
                if self.default is not None:
                    if not case_ejecutado or (case_ejecutado and not vino_break and not vino_return):
                        nuevo_entorno = Entorno(entorno)
                        for accion in self.default:
                            if accion is not None and not isinstance(accion, Error):
                                respuesta = accion.ejecutar(nuevo_entorno)
                                if isinstance(respuesta, Error):
                                    err_semanticos_internos.append(respuesta)
                                elif isinstance(respuesta, list):
                                    err_semanticos_internos.extend(respuesta)
                                elif isinstance(respuesta, dict):
                                    # Si viene dict, se detectaron errores semánticos y un break o un return
                                    err_semanticos_internos.extend(respuesta.get("errores"))
                                    if respuesta.get("break"):
                                        vino_break = True
                                        break
                                    instancia_return = respuesta.get("return")
                                    if instancia_return:
                                        vino_return = True
                                        break
                                    print("Error que no debería de pasar, se detectó un dict en switch, sin que viniera break o return")
                                elif isinstance(respuesta, Break):
                                    vino_break = True
                                    break
                                elif isinstance(respuesta, Return):
                                    instancia_return = respuesta
                                    vino_return = True
                                    break
                    elif case_ejecutado and vino_return:
                        if err_semanticos_internos:
                            # Vinieron errores y un return
                            retorno = {"errores": err_semanticos_internos, "return": instancia_return}
                            return retorno
                        # Vino solo un return
                        return instancia_return

            elif self.default is not None:
                # DEFAULT
                # TODO Break en Default
                vino_break = False
                err_semanticos_internos = []
                
                nuevo_entorno = Entorno(entorno)
                for accion in self.default:
                    if accion is not None and not isinstance(accion, Error):
                        respuesta = accion.ejecutar(nuevo_entorno)
                        if isinstance(respuesta, Error):
                            err_semanticos_internos.append(respuesta)
                        elif isinstance(respuesta, list):
                            err_semanticos_internos.extend(respuesta)
                        elif isinstance(respuesta, dict):
                            # Si viene dict, se detectaron errores semánticos y un break o un return
                            err_semanticos_internos.extend(respuesta.get("errores"))
                            if respuesta.get("break"):
                                vino_break = True
                                break
                            instancia_return = respuesta.get("return")
                            if instancia_return:
                                vino_return = True
                                break
                            print("Error que no debería de pasar, se detectó un dict en switch, sin que viniera break o return")
                        elif isinstance(respuesta, Break):
                            vino_break = True
                            break
                        elif isinstance(respuesta, Return):
                            instancia_return = respuesta
                            vino_return = True
                            break
            else:
                return Error("Error semántico", "Se detectó un switch sin listado de cases, ni instrucción default.", self.fila, self.columna)
        
            if vino_return:
                if err_semanticos_internos:
                    # Vinieron errores y un return
                    retorno = {"errores": err_semanticos_internos, "return": instancia_return}
                    return retorno
                # Vino solo un return
                return instancia_return
            if err_semanticos_internos:
                return err_semanticos_internos

        else:
            return Error("Error semántico", f"Únicamente se admiten valores de tipo primitivo en la expresión del switch, {self.expr_switch.tipo} fue encontrado.", self.fila, self.columna)