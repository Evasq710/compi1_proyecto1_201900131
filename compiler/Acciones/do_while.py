if __name__ == "compiler.Acciones.do_while":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Acciones.instr_break import Break
    from compiler.Acciones.instr_return import Return
    from compiler.Entorno.entorno import Entorno
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Acciones.instr_break import Break
    from Acciones.instr_return import Return
    from Entorno.entorno import Entorno

class Do_While(Accion):
    def __init__(self, fila, columna, listado_instrucciones, condicion):
        super().__init__(fila, columna)
        self.listado_instrucciones = listado_instrucciones
        self.condicion = condicion
    
    def ejecutar(self, entorno: Entorno):
        err_semanticos_internos = []
        nuevo_entorno = Entorno(entorno)
        vino_break = False
        vino_return = False
        instancia_return = None

        for accion in self.listado_instrucciones:
            if accion is not None and not isinstance(accion, Error):
                respuesta = accion.ejecutar(nuevo_entorno)
                if isinstance(respuesta, Error):
                    err_semanticos_internos.append(respuesta)
                elif isinstance(respuesta, list):
                    err_semanticos_internos.extend(respuesta)
                elif isinstance(respuesta, dict):
                    # Si viene dict, se detectaron errores semánticos y un break o un return
                    err_semanticos_internos.extend(respuesta.get("errores"))
                    if respuesta.get("break"):
                        vino_break = True
                        break
                    instancia_return = respuesta.get("return")
                    if instancia_return:
                        vino_return = True
                        break
                    print("Error que no debería de pasar, se detectó un dict en do while, sin que viniera break o return")
                elif isinstance(respuesta, Break):
                    vino_break = True
                    break
                elif isinstance(respuesta, Return):
                    instancia_return = respuesta
                    vino_return = True
                    break
        
        if not vino_break or not vino_return:
            condicion = self.condicion.ejecutar(entorno)
            if isinstance(condicion, Error):
                err_semanticos_internos.append(condicion)
                return err_semanticos_internos
            
            if condicion is True or condicion is False:
                iteraciones = 0
                while condicion:
                    nuevo_entorno = Entorno(entorno)
                    for accion in self.listado_instrucciones:
                        if accion is not None and not isinstance(accion, Error):
                            respuesta = accion.ejecutar(nuevo_entorno)
                            if isinstance(respuesta, Error):
                                err_semanticos_internos.append(respuesta)
                            elif isinstance(respuesta, list):
                                err_semanticos_internos.extend(respuesta)
                            elif isinstance(respuesta, dict):
                                # Si viene dict, se detectaron errores semánticos y un break o un return
                                err_semanticos_internos.extend(respuesta.get("errores"))
                                if respuesta.get("break"):
                                    vino_break = True
                                    break
                                instancia_return = respuesta.get("return")
                                if instancia_return:
                                    vino_return = True
                                    break
                                print("Error que no debería de pasar, se detectó un dict en do while, sin que viniera break o return")
                            elif isinstance(respuesta, Break):
                                vino_break = True
                                break
                            elif isinstance(respuesta, Return):
                                instancia_return = respuesta
                                vino_return = True
                                break

                    if not vino_break or not vino_return:
                        iteraciones += 1
                        condicion = self.condicion.ejecutar(entorno)
                    elif vino_return:
                        if err_semanticos_internos:
                            # Vinieron errores y un return
                            retorno = {"errores": err_semanticos_internos, "return": instancia_return}
                            return retorno
                        # Vino solo un return
                        return instancia_return
                    else:
                        break
                    
                if err_semanticos_internos:
                    return err_semanticos_internos
            else:
                err_semanticos_internos.append(Error("Error semántico", "La condición del Do-While debe ser de tipo booleano.", self.fila, self.columna))
                return err_semanticos_internos
        elif vino_return:
            if err_semanticos_internos:
                # Vinieron errores y un return
                retorno = {"errores": err_semanticos_internos, "return": instancia_return}
                return retorno
            # Vino solo un return
            return instancia_return
        elif err_semanticos_internos:
            return err_semanticos_internos