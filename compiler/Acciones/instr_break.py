if __name__ == "compiler.Acciones.instr_break":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Entorno.entorno import Entorno
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Entorno.entorno import Entorno

class Break(Accion):
    def __init__(self, fila, columna):
        super().__init__(fila, columna)
    
    def ejecutar(self, entorno):
        return self