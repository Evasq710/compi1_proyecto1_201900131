if __name__ == "compiler.Acciones.asignacion_arr":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Entorno.entorno import Entorno
    from compiler.Entorno.simbolo import Simbolo
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Entorno.entorno import Entorno
    from Entorno.simbolo import Simbolo

class Asignacion_Arr(Accion):
    def __init__(self, fila, columna, identificador, expresiones_posicion : list, expresion):
        super().__init__(fila, columna)
        self.identificador = identificador
        self.expresiones_posicion = expresiones_posicion
        self.expresion = expresion
    
    def ejecutar(self, entorno: Entorno):           
        variable: Simbolo = entorno.buscar_simbolo(str(self.identificador).lower())
        if variable is None:
            return Error("Error semántico", "Al asignar un valor a un arreglo, no se encontró el identificador en el entorno actual, ni en los padre.", self.fila, self.columna)
        
        if variable.tipo != "array":
            self.tipo = 'ERROR_SEMANTICO'
            return Error("Error semántico", f"El identificador se llamó utilizando la sintaxis de un arreglo, cuando es de tipo {variable.tipo}.", self.fila, self.columna)
        
        posiciones = []
        for expresion_posicion in self.expresiones_posicion:
            posicion = expresion_posicion.ejecutar(entorno)
            if isinstance(posicion, Error):
                self.tipo = 'ERROR_SEMANTICO'
                return posicion
            if expresion_posicion.tipo != "int":
                self.tipo = 'ERROR_SEMANTICO'
                return Error("Error semántico", f"En la llamada de arreglos, el índice debe ser de tipo int, {expresion_posicion.tipo} fue encontrado.", self.fila, self.columna)
            posiciones.append(posicion)

        valor = self.expresion.ejecutar(entorno)
        if isinstance(valor, Error):
            return valor
        
        if variable.tipo_datos_array != self.expresion.tipo:
            return Error("Error semántico", f"En la asignación de valor a un arreglo, se asignó un valor de tipo {self.expresion.tipo} a un arreglo de tipo {variable.tipo_datos_array}.", self.fila, self.columna)

        respuesta = self.modificar_valor(variable.valor, posiciones, valor)
        if isinstance(respuesta, Error):
            return respuesta

        # Editando el símbolo modificado
        simbolo_editado = Simbolo(tipo="array", nombre=str(self.identificador).lower(), valor=variable.valor, linea=self.fila, tipo_datos_array=variable.tipo_datos_array)
        entorno.editar_simbolo(str(self.identificador).lower(), simbolo_editado)
    

    def modificar_valor(self, variable, arreglo_posiciones: list, valor):
        if len(arreglo_posiciones) == 1:
            if not isinstance(variable, list):
                return Error("Error semántico", "En la asignación de valor a un arreglo, se intentó acceder a una dimensión que no existe.", self.fila, self.columna)
            try:
                index_final = arreglo_posiciones.pop()
                if isinstance(variable[index_final], list):
                    return Error("Error semántico", "En la asignación de un valor a un arreglo, el lenguaje solo admite que se le asignen valores a la última dimensión del arreglo (interno).", self.fila, self.columna)
                # En este punto se hace la asignación del valor en la posición indicada
                variable[index_final] = valor
                return True
            except:
                return Error("Error semántico", "Se hizo referencia al index de un arreglo fuera de los límites de este.", self.fila, self.columna)
            
        if isinstance(variable, list):
            nuevo_index = arreglo_posiciones.pop(0)
            try:
                return self.modificar_valor(variable[nuevo_index], arreglo_posiciones, valor)
            except:
                return Error("Error semántico", "Se hizo referencia al index de un arreglo fuera de los límites de este.", self.fila, self.columna)
        return Error("Error semántico", "En la asignación de valor a un arreglo, se intentó acceder a una dimensión que no existe", self.fila, self.columna)
        
        # if self.expresion.tipo == 'null':
        #     # Reasignación del valor de la variabe a null
        #     entorno.editar_simbolo(str(self.identificador).lower(), Simbolo(variable.tipo, str(self.identificador).lower(), 'null', self.fila))
        # elif variable.tipo == 'int':
        #     if self.expresion.tipo == 'int':
        #         entorno.editar_simbolo(str(self.identificador).lower(), Simbolo(variable.tipo, str(self.identificador).lower(), valor, self.fila))
        #     else:
        #         return Error("Error semántico", "La asignación de la variable de tipo 'int' no concuerda con el tipo de la expresión dada.", self.fila, self.columna)
        # elif variable.tipo == 'double':
        #     if self.expresion.tipo == 'double':
        #         entorno.editar_simbolo(str(self.identificador).lower(), Simbolo(variable.tipo, str(self.identificador).lower(), valor, self.fila))
        #     else:
        #         return Error("Error semántico", "La asignación de la variable de tipo 'double' no concuerda con el tipo de la expresión dada.", self.fila, self.columna)
        # elif variable.tipo == 'boolean':
        #     if self.expresion.tipo == 'boolean':
        #         entorno.editar_simbolo(str(self.identificador).lower(), Simbolo(variable.tipo, str(self.identificador).lower(), valor, self.fila))
        #     elif self.expresion.tipo == 'int' and (valor == 0 or valor == 1):
        #         valor = bool(valor)
        #         entorno.editar_simbolo(str(self.identificador).lower(), Simbolo(variable.tipo, str(self.identificador).lower(), valor, self.fila))
        #     else:
        #         return Error("Error semántico", "La asignación de la variable de tipo 'boolean' no concuerda con el tipo de la expresión dada.", self.fila, self.columna)
        # elif variable.tipo == 'char':
        #     if self.expresion.tipo == 'char':
        #         entorno.editar_simbolo(str(self.identificador).lower(), Simbolo(variable.tipo, str(self.identificador).lower(), valor, self.fila))
        #     else:
        #         return Error("Error semántico", "La asignación de la variable de tipo 'char' no concuerda con el tipo de la expresión dada.", self.fila, self.columna)
        # elif variable.tipo == 'string':
        #     if self.expresion.tipo == 'string':
        #         entorno.editar_simbolo(str(self.identificador).lower(), Simbolo(variable.tipo, str(self.identificador).lower(), valor, self.fila))
        #     else:
        #         return Error("Error semántico", "La asignación de la variable de tipo 'string' no concuerda con el tipo de la expresión dada.", self.fila, self.columna)