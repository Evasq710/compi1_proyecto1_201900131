class Error:
    def __init__(self, tipo, descripcion, fila, columna):
        self.tipo = tipo
        self.descripcion = descripcion
        self.fila = fila
        self.columna = columna

    def reporte_str_error(self):
        reporte = "Tipo error: " + self.tipo + " - " + self.descripcion + " - Fila: " + str(self.fila) + " Columna: " + str(self.columna)
        return reporte