if __name__ == "compiler.Acciones.declaracion":
    from compiler.Acciones.Interface.Accion import Accion
    from compiler.Acciones.Interface.Error import Error
    from compiler.Entorno.entorno import Entorno
    from compiler.Entorno.simbolo import Simbolo
    from compiler.Expresiones.primitivo import Primitivo
else:
    from Acciones.Interface.Accion import Accion
    from Acciones.Interface.Error import Error
    from Entorno.entorno import Entorno
    from Entorno.simbolo import Simbolo
    from Expresiones.primitivo import Primitivo

class Declaracion(Accion):
    def __init__(self, fila, columna, identificador, tipo, expresion = None):
        super().__init__(fila, columna)
        self.identificador = identificador
        self.tipo = tipo
        self.expresion = expresion
    
    def ejecutar(self, entorno: Entorno):
        if self.expresion is None:
            simbolo = Simbolo(self.tipo, str(self.identificador).lower(), 'null', self.fila)
        else:
            valor = self.expresion.ejecutar(entorno)
            if not isinstance(valor, Error):
                if self.expresion.tipo == 'null':
                    # Declaración sin asignación de valor
                    simbolo = Simbolo(self.tipo, str(self.identificador).lower(), 'null', self.fila)
                elif self.tipo == 'int':
                    if self.expresion.tipo == 'int':
                        simbolo = Simbolo(self.tipo, str(self.identificador).lower(), valor, self.fila)
                    else:
                        return Error("Error semántico", "La declaración de la variable de tipo 'int' no concuerda con el tipo de la expresión.", self.fila, self.columna)
                elif self.tipo == 'double':
                    if self.expresion.tipo == 'double':
                        simbolo = Simbolo(self.tipo, str(self.identificador).lower(), valor, self.fila)
                    else:
                        return Error("Error semántico", "La declaración de la variable de tipo 'double' no concuerda con el tipo de la expresión.", self.fila, self.columna)
                elif self.tipo == 'boolean':
                    if self.expresion.tipo == 'boolean':
                        simbolo = Simbolo(self.tipo, str(self.identificador).lower(), valor, self.fila)
                    elif self.expresion.tipo == 'int' and (valor == 0 or valor == 1):
                        valor = bool(valor)
                        simbolo = Simbolo(self.tipo, str(self.identificador).lower(), valor, self.fila)
                    else:
                        return Error("Error semántico", "La declaración de la variable de tipo 'boolean' no concuerda con el tipo de la expresión.", self.fila, self.columna)
                elif self.tipo == 'char':
                    if self.expresion.tipo == 'char':
                        simbolo = Simbolo(self.tipo, str(self.identificador).lower(), valor, self.fila)
                    else:
                        return Error("Error semántico", "La declaración de la variable de tipo 'char' no concuerda con el tipo de la expresión.", self.fila, self.columna)
                elif self.tipo == 'string':
                    if self.expresion.tipo == 'string':
                        simbolo = Simbolo(self.tipo, str(self.identificador).lower(), valor, self.fila)
                    else:
                        return Error("Error semántico", "La declaración de la variable de tipo 'string' no concuerda con el tipo de la expresión.", self.fila, self.columna)
            else:
                # Retornando el error semántico de la expresión sin agregar el símbolo
                return valor

        if simbolo:
            insertado = entorno.insertar_simbolo(simbolo)

            if not insertado:
                # Ya existe el símbolo en el entorno, no se inserta
                return Error("Error semántico", "No se puede declarar la variable, esta ya existe en el entorno.", self.fila, self.columna)
            