
class Dato:
    def __init__(self, id_node, label, inicio = False, hoja_lexema = False, id_nodo_padre = None, hoja_error = False):
        self.id_node = id_node
        self.label = label
        self.inicio = inicio
        self.hoja_lexema = hoja_lexema
        self.id_nodo_padre = id_nodo_padre
        self.hoja_error = hoja_error

class Nodo:
    def __init__(self, dato: Dato):
        self.dato = dato
        self.nodos_hijo = []
        self.str_nodos_graphviz = ''
    
    def insertar_hijo(self, nodo_hijo):
        self.nodos_hijo.append(nodo_hijo)

    def obtener_nodo(self, id_node):
        if self.dato.id_node == id_node:
            return self
        for nodo_hijo in self.nodos_hijo:
            nodo = nodo_hijo.obtener_nodo(id_node)
            if nodo is not None:
                return nodo
        return None
    
    def insertar_hijo_en(self, nodo_hijo, id_node_padre):
        nodo_padre = self.obtener_nodo(id_node_padre)
        if nodo_padre is not None:
            nodo_padre.insertar_hijo(nodo_hijo)
            return True
        print(f"No se encontró al nodo interior con id {id_node_padre}")
        return False    

    def imprimir_arbol(self):
        print(f"Mi id: {self.dato.id_node}, mi label: {self.dato.label}, mi padre: {self.dato.id_nodo_padre}")
        if self.nodos_hijo:
            print(">> Soy No Terminal, y mis hijos son:")
            for nodo_hijo in self.nodos_hijo:
                nodo_hijo.imprimir_arbol()
        else:
            print(">> Soy Terminal, por lo que no tengo hijos")
    
    def crear_nodos_graphviz(self):
        if self.dato.inicio:
            self.str_nodos_graphviz += f'node{self.dato.id_node}[label="{self.dato.label}", fillcolor="cyan3"];\n'
        elif self.dato.hoja_lexema:
            self.str_nodos_graphviz += f'node{self.dato.id_node}[label="{self.dato.label}", fillcolor="white", shape="underline"];\n'
            self.str_nodos_graphviz += f'node{self.dato.id_nodo_padre} -> node{self.dato.id_node};\n'
        elif self.dato.hoja_error:
            self.str_nodos_graphviz += f'node{self.dato.id_node}[label="{self.dato.label}", fillcolor="firebrick1"];\n'
            self.str_nodos_graphviz += f'node{self.dato.id_nodo_padre} -> node{self.dato.id_node};\n'
        else:
            self.str_nodos_graphviz += f'node{self.dato.id_node}[label="{self.dato.label}"];\n'
            self.str_nodos_graphviz += f'node{self.dato.id_nodo_padre} -> node{self.dato.id_node};\n'
        if self.nodos_hijo:
            for nodo_hijo in self.nodos_hijo:
                self.str_nodos_graphviz += nodo_hijo.crear_nodos_graphviz()
        return self.str_nodos_graphviz

class Arbol_Graphviz:
    def __init__(self, str_nodos):
        self.str_graphviz = '''digraph G {
        graph[label="CST (Concrete Syntax Tree)"]
        node[style="filled", fillcolor="palegreen"]\n'''
        self.str_graphviz += str_nodos
        self.str_graphviz += '}'

    # def __init__(self):
    #     self.arbol_CST = Digraph(comment='CST (Concrete Syntax Tree) generado de Little Code', node_attr={'style': 'filled', 'fillcolor': 'palegreen'},
    #                             edge_attr={'arrowsize': '0.7'}, graph_attr={'label': 'CST (Concrete Syntax Tree)'})